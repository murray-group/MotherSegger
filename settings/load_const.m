function [CONST] = load_const(params)
    CONST = loadConstants(params,0);
    CONST.seg.OPTI_FLAG = 0;
    CONST.parallel.PARALLEL_FLAG = false;
    CONST.trackOpti.MIN_AREA_NO_NEIGH = 50;
    CONST.trackLoci.numSpots = [5,5,5,5,5,5]; % Max number of foci to fit in each fluorescence channel (default = [0 0]) % fit up to 5 foci in each cell
    CONST.parallel.verbose = 0;% not verbose state
end