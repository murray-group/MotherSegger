%% combine field of views which where analyzed by PartI (+ PartII)
base = "/media/koehlerr/970EVOPlus/Data/Microfluid/20211008_ParB/";
base = "/media/koehlerr/970EVOPlus/Data/Microfluid/20211126_CFX/";
combine_these = ["Day1/","Day2/"];
output_folder_name = "combined/";
path_first = strcat(base,combine_these(1));
channel_paths = get_channel_paths(path_first,false);

combined_folder = strcat(base,output_folder_name);
if ~exist(combined_folder, 'dir')
   mkdir(combined_folder)
end
disp('combining channels')
for channel_it = 1:numel(channel_paths)
    channel_path = channel_paths(channel_it);
    if ~mod(channel_it,10)
        disp(strcat(string(channel_it)," out of ",string(numel(channel_paths))))
    end
    try
        foci_list = dir(channel_path);
        names = string({foci_list.name});
        names_tif = names(endsWith(names,'tif'));
        for name = names_tif
            concat_this = cell(1,numel(combine_these));
            for it_com = 1:numel(combine_these)
                path = strrep(channel_path,combine_these(1),combine_these(it_com));
                path = strcat(path,name);
                concat_this{it_com} = loadTiff(char(path));
            end
            lengths = cellfun(@(x) size(x,3),concat_this);
            
            %fixing length issues
            y_min = 100000000000;
            x_min = 100000000000;
            for it_con = 1:numel(concat_this)
                tmp = concat_this{it_con};
                shape = size(tmp);
                if shape(1)<y_min,y_min=shape(1);end
                if shape(2)<x_min,x_min=shape(2);end
            end
            %%{
            for it_con = 1:numel(concat_this)
                tmp = concat_this{it_con};
                shape = size(tmp);
                if shape(1)>y_min||shape(2)>x_min
                    tmp = tmp(1:y_min,1:x_min,:);
                    concat_this{it_con} = tmp;
                    path = strrep(channel_path,combine_these(1),combine_these(it_con));
                    path = strcat(path,name);
                    disp(strcat("changed shape of ",path))
                end
            end
            %}
            
            
            concat_this = cat(3,concat_this{:});
            path = strrep(path,combine_these(end),output_folder_name);

            foci_list = strsplit(path,'/');
            foci_list = strjoin(foci_list(1:end-2),'/');
            combined_folder = strcat(foci_list,'/');
            if ~exist(combined_folder, 'dir')
                mkdir(combined_folder);
            end

            foci_list = strsplit(path,'/');
            foci_list = strjoin(foci_list(1:end-1),'/');
            combined_folder = strcat(foci_list,'/');
            if ~exist(combined_folder, 'dir')
                mkdir(combined_folder);
            end


            writeTiff(char(path),concat_this);
        end
        names_foci = names(startsWith(names,'supersegger_foci'));
        if numel(names_foci) == 1
            name_foci = names_foci;
            concat_this = cell(1,numel(name_foci));
            for it_com = 1:numel(combine_these)
                path = strrep(channel_path,combine_these(1),combine_these(it_com));
                path = strcat(path,name_foci);
                concat_this{it_com} = load(path);
            end

            foci_list = concat_this{1}.foci_list;
            for it_com = 2:numel(combine_these)
                concat_this{it_com}.foci_list(:,1) = concat_this{it_com}.foci_list(:,1)+sum(lengths(1:it_com-1));
                tmp = concat_this{it_com}.foci_list;
                foci_list = cat(1,foci_list,tmp);
            end

            path = strrep(path,combine_these(end),output_folder_name);
            %path
            %foci_list = foci_data.foci_list;
            save(path,'foci_list');
        end
    catch ME
        disp(strcat("failed: ",channel_path))
        disp(ME.message)
    end
end
