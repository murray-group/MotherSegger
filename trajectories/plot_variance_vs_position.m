function plot_variance_vs_position(trajectory_list)

    l_max = max(cat(1,trajectory_list.lengths));
    n_foci = size(trajectory_list(1).positions,2)

    positions = cell(1,l_max);
    for i=1:l_max,positions{i}=zeros(n_foci,0);end
    
    for tra_it = 1:numel(trajectory_list)
        tra = trajectory_list(tra_it);
        pos = tra.positions(:,:,1);
        for j = 1:numel(tra.lengths)
            l = tra.lengths(j);
            tmp = positions{l};
            tmp(:,size(tmp,2)+1) = sort(pos(j,:));
            positions{l} = tmp;
        end
    end
    %
    stds = zeros(n_foci,l_max);
    means = zeros(n_foci,l_max);
    for i = 1:l_max,stds(:,i) = std(positions{i},[],2);end
    for i = 1:l_max,means(:,i) = mean(positions{i},2);end
    stds
    counts = cellfun(@(x) size(x,2),positions);
    ptmr = 0.0668;
    figure(9)
    yyaxis left;
    plot((1:l_max)*ptmr,stds'*ptmr);
    ylabel('std')
    ylim([0,1])
    yyaxis right;
    plot((1:l_max)*ptmr,counts);
    ylabel('count')
    xlabel('length (\mum)')
    title('1 Foci')
    xlim([1.5,5])
    ax=gca ; 
    ax.FontSize = 7;
end