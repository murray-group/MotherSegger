
function plot_with_err(x,y,err,color,alpha)
    %bring all vectors into the same form (1,n)
    if(size(x,1)>1),x=x';end
    if(size(y,1)>1),y=y';end
    if(size(err,1)>1),err=err';end
    %find all positions where there is a nan
    indices = find(any(isnan(cat(1,x,y,err))));
    start = 1;
    %for each segment that does not contain a nan value
    %hold off
    plot(nan,nan)
    for i = 1:numel(indices)+1
        if i == numel(indices)+1
            stop = numel(x);
        else
            stop = indices(i)-1;
        end
        if i ~= 1
            hold on
        end
        plot_with_err_segment(x(start:stop),y(start:stop),err(start:stop),color,alpha)
        start = stop + 2;
    end
    hold off
end


function plot_with_err_segment(x,y,err,color,alpha)
    %since fill fails with nan values data to plot must be broken down into
    %fragments not containing any nan
    %plot(x,y,'-o','Color',color,'MarkerFaceColor', color ,'MarkerSize',2)
    plot(x,y,'-','Color',color)
    hold on
    y1 = y+err;
    y2 = y-err;
    h = fill([x, fliplr(x)], [y1,fliplr(y2)],color,'LineStyle','none','facealpha',alpha);
    hold off
end