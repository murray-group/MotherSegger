%returns D, a=1/tau, b=f/kT plus their uncert.
function [D,D_prop_uncert,one_over_tau,oov_prop_uncert,f_over_kT,fokT_prop_uncert,fob,range] = get_D(x,dt,dx_mean,dx_var,counts,percent_of_data_points)
    disp_info = true;
    %range = counts>mean(counts)/3; %only use regions where there is enough coverage
    range = cut_off(counts,percent_of_data_points);
    %range = ~isnan(dx_mean);
    [fob,gof]=fit(x(range)',dx_mean(range),'poly1','Weights',1./counts(range));%1./dx_SEM(range).^2); %here we find m
    [fob2,gof2]=fit(x(range)',dx_var(range),'poly1','Weights',1./counts(range));%here we find vx

    one_over_tau=-log(fob.p1*dt+1)/dt %one over tau -> oot
    f_over_kT=(1-(fob.p1*dt+1)^2)/(dt*dt*fob2.p2); %f over kT -> fokT
    %range
    CI=confint(fob,0.95);%confinterval of m
    p1uncert=fob.p1-CI(1,1);
    oov_prop_uncert=abs(p1uncert/(fob.p1*dt+1));%propagated uncertainty in s-1
    if disp_info,disp(['v=x*(exp(-(fD/kT))-1)/dt implies (fD/kT)=',num2str(one_over_tau),' +/- ',num2str(oov_prop_uncert),' s^{-1}']);end

    CI=confint(fob2,0.95);
    p2uncert=fob2.p2-CI(1,2);
    fokT_prop_uncert=f_over_kT*sqrt(    (  2*(fob.p1*dt+1)*p1uncert*dt/( 1-(fob.p1*dt+1)^2  )   )^2+(p2uncert/fob2.p2)^2);%propagated uncertainty in min-1
    if disp_info,disp(['f/kT= ', num2str(f_over_kT),' +/- ',num2str(fokT_prop_uncert),' mum^-2']);end

    D_prop_uncert=sqrt(  (dt*dt*fob2.p2/(1-exp(-2*dt*one_over_tau))   *(1-2*one_over_tau*dt*exp(-2*one_over_tau*dt)/(1-exp(-2*dt*one_over_tau)))  )^2*oov_prop_uncert^2  +  (one_over_tau*dt*dt*p2uncert/(1-exp(-2*dt*one_over_tau)))^2    );


    if disp_info,disp(['D= ', num2str(one_over_tau/f_over_kT),' +/- ',num2str(D_prop_uncert),' mum^2 s^-1']);end
    D = one_over_tau/f_over_kT;
end



%function D = get_D(dt,sigma_vel,m)
%    D = sigma_vel.^2.*log(dt.*m+1)./(dt.*m.^2+2.*m);
%end

%function m = get_slope(x,means,counts)
%    mdl = fitlm(x,means,'Weights',(counts/sum(counts)));
%    m = mdl.Coefficients{2,1};
%end