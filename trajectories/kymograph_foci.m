function [min_len,max_len,canvas] = kymograph_foci(trajectory_list)
    lengths = cat(1,trajectory_list.lengths);
    min_len = min(lengths);
    max_len = max(lengths);
    range = min_len:max_len;
    n_buckets = numel(range);
    buckets = cell(1,n_buckets);
    c_buckets = ones(n_buckets);
    for buck_it = 1:n_buckets
        l = range(buck_it);
        n = sum(lengths==l);
        buckets{buck_it} = zeros(1,n);
    end

    for tra_it = 1:numel(trajectory_list)
        tra = trajectory_list(tra_it);
        pos = tra.positions(:,:,1);
        for i = 1:size(pos,1)
            for j = 1:size(pos,2)
                l = tra.lengths(i);
                buck_index = l-min_len+1;
                tmp = buckets{buck_index};
                tmp(c_buckets(buck_index)) = pos(i,j);
                buckets{buck_index} = tmp;
                c_buckets(buck_index) = c_buckets(buck_index)+1;
            end
        end
    end
    %
    edges = -floor(max_len/2):ceil(max_len/2);
    middle = (edges(1:end-1)+edges(2:end))/2;
    canvas = nan(numel(edges)-1,n_buckets);
    for buck_it = 1:n_buckets
        l = range(buck_it);
        tmp = buckets{buck_it};
        res = histcounts(tmp,edges);
        canvas(:,buck_it) = res;
        canvas(round(max_len/2+l/2):end,buck_it) = nan;
        canvas(1:round(max_len/2-l/2),buck_it) = nan;
    end
    canvas = canvas./sum(canvas,1,'omitnan');
    q = quantile(canvas(:),.99);
    canvas(canvas>q) = q;
    imagesc(canvas)
    imAlpha=ones(size(canvas));
    imAlpha(isnan(canvas))=0;
    imagesc(canvas,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]);

    x_range = 1:20:numel(range);% locations for x/y ticks
    y_range = 1:20:numel(middle);
    % figure out x/y ticks 
    mdl_x = fitlm(x_range,range(x_range)*0.0668);
    inter_x = mdl_x.Coefficients{1,1};
    coef_x = mdl_x.Coefficients{2,1};
    mdl_y = fitlm(y_range,middle(y_range)*0.0668);
    inter_y = mdl_y.Coefficients{1,1};
    coef_y = mdl_y.Coefficients{2,1};
    tickFun = @(y,a,b) (y-b)/a;
    xTickLabels = 0:0.25:10;
    xTicks = tickFun(xTickLabels,coef_x,inter_x);
    xTickLabels = string(xTickLabels);
    xTickLabels(2:2:end) = "";
    yTickLabels = -5:.25:5;
    yTicks = tickFun(yTickLabels,coef_y,inter_y);
    yTickLabels = string(yTickLabels);
    yTickLabels(2:2:end) = "";
    %set ticks
    set(gca, 'XTick', xTicks, 'XTickLabel', xTickLabels);% set x and y ticks
    set(gca, 'YTick', yTicks, 'YTickLabel', yTickLabels);
    xlabel('length (\mum)')
    ylabel('position (\mum)')
end