
% rp = 1, un = 0, os = -1
function [rp_list,os_list] = split_trajectories(trajectory_list,treshold)

    rp_count = 1;
    os_count = 1;
    for i = 1:numel(trajectory_list)
        trajectory = trajectory_list(i);


        ann = trajectory.annotation;
        ann = ann(1:size(trajectory.positions,1));%dirty fix
        ann = ann>treshold;
        transitions = abs(diff([~ann(1);ann';~ann(end)]));
        start = find(transitions == 1);
        stop = start(2:end)-1;
        start = start(1:end-1);

        for j = 1:numel(start)
            trajectory_struct = struct();
            trajectory_struct.cycle_id = trajectory.cycle_id;
            %trajectory_struct.score = trajectory.score;
            trajectory_struct.start = trajectory.start+start(j)-1;
            trajectory_struct.stop = trajectory.start+(stop(j)-start(j))+start(j)-1;
            %trajectory_struct.positions_raw = trajectory.positions_raw(start(j):stop(j),:,:);
            %length(ann)
            %[start(j),stop(j)]
            %size(trajectory.positions)
            trajectory_struct.positions = trajectory.positions(start(j):stop(j),:,:);
            %trajectory_struct.positions_normalized = trajectory.positions_normalized(start(j):stop(j),:,:);
            %trajectory_struct.positions_sim = trajectory.positions_sim(start(j):stop(j),:,:);
            trajectory_struct.relative_cell_age = trajectory.relative_cell_age(start(j):stop(j));
            trajectory_struct.widths = trajectory.widths(start(j):stop(j));
            trajectory_struct.lengths = trajectory.lengths(start(j):stop(j));
            trajectory_struct.annotation = trajectory.annotation(start(j):stop(j));
            if ann(start(j))
                os_list(os_count) = trajectory_struct;
                os_count = os_count + 1;
            else
                rp_list(rp_count) = trajectory_struct;
                rp_count = rp_count + 1;
            end
        end
    end
end