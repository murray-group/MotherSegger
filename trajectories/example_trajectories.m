function [trajectory_list] = example_trajectories(D,dt,k,n_points,n_trajectories)

    %D = 0.002;
    %dt = .5;
    %sigma_pos = 0.01;
    %tau = sigma_pos^2/D;
    %m = get_m(dt,tau);
    
    tra_empty = struct('positions',zeros(1),'lengths',zeros(1),'widths',zeros(1),'realtive_cell_age',zeros(1),'parameters',struct());
    trajectory_list = repmat(tra_empty,[1,numel(n_trajectories)]);
    
    for i = 1:n_trajectories
        [pos] = sim_traj(dt,D,k,1,n_points);
        tra = tra_empty;
        tra.lengths = ones(size(pos,1),1);
        tra.widths = ones(size(pos,1),1);
        tra.realtive_cell_age = (1:size(pos,1))'/size(pos,1);
        tra.parameters.D = D;
        tra.parameters.dt = dt;
        tra.parameters.k = k;
        tra.parameters.c_e = 1;
        tra.parameters.sigma_x = sqrt(1/k);
        tra.parameters.sigma_y = sqrt(1/k);
        tra.positions = repmat(pos,[1,1,2]);%to keep the position format constant
        trajectory_list(i) = tra;
    end
end


function [pos] = sim_traj(dt,D,k,n_springs,l)
    K = D*k*n_springs;
    S = 1-exp(-2*dt*K);
    expx = exp(-dt*K);
    sd = sqrt(S/k);
    pos = zeros(l,1);
    normrands = normrnd(0,1,1,l);
    for i = 2:l
        x0 = pos(i-1);
        %x = x0*expx+sd*normrnd(0,1,1);
        x = x0*expx+sd*normrands(i);
        pos(i) = x;
    end
end

%{


D = 0.0005;
sigma_pos = 0.1;
it = 100;
n =1;
%range_dt = linspace(1,600,it);
%range_dt = exp(linspace(log(0.001),log(600),100));
range_dt = exp(linspace(log(0.001),log(100),100));
range_D = linspace(0.0001,0.01,100);
range_buckets = linspace(11,1001,100);
%n = 1;
%%

now = 190;
results = zeros(it,now,5);
expected = zeros(it,5);
quantile_vel = 0.2;
for i = 1:numel(range_dt)
    i
    dt = range_dt(i);
    %dt = 10.01;
    %D = range_D(i);
    %D = 0.003;
    %n_buckets = range_buckets(i);
    n_buckets = 101;
    
    tau = sigma_pos^2/D/n;
    m = get_m(dt,tau);
    sigma_vel = get_sigma_vel(D,dt,tau);
    D_verfiy = get_D(dt,sigma_vel,m);
    
    expected(i,:) = [D_verfiy,tau,m,sigma_vel,sigma_pos/sqrt(n)];
    
    tmp = zeros(now,5);
    parfor j = 1:now
        [pos,sigma_pos_hat] = sim_traj(dt,D,sigma_pos,n,10000);
        [D_hat,m_hat,sigma_vel_hat] = predict_parameters_from_pos(dt,pos,quantile_vel,n_buckets);
        tau_hat = get_tau(dt,m_hat);
        tmp(j,:) = [D_hat,tau_hat,m_hat,sigma_vel_hat,sigma_pos_hat];
    end
    results(i,:,:) = tmp;
end
%%
a = mean(real(results(:,:,3)'))-expected(:,3)'
mean(a(round(end/2):end))
%%
range = range_dt;
m_hat = mean(real(results(:,:,3)'));
m_expected = expected(:,3)';
sigma_vel_hat = mean(real(results(:,:,4)'));
sigma_vel_expected = expected(:,4)';
figure(1)
subplot(2,2,1)
plot(range,m_hat)
hold on
plot(range,m_expected)
hold off 
title('m')
subplot(2,2,2)
plot(range,sigma_vel_hat)
hold on
plot(range,sigma_vel_expected)
hold off 
title('sigma vel')

subplot(2,2,3)
D_expected = get_D(range_dt,sigma_vel_expected,m_expected);
plot(range,D_expected)
hold on
D_1 = get_D(range_dt,sigma_vel_hat,m_hat);
plot(range,D_1)
D_2 = get_D(range_dt,sigma_vel_hat,m_expected);
plot(range,D_2)
D_3 = get_D(range_dt,sigma_vel_expected,m_hat);
plot(range,D_3)
hold off
legend('D:m expected, sig expected','D:m hat, sig hat','D:m expected, sig hat','D:m hat, sig expected')
title('D')

%%
range = range_dt;
%range = range_D;
%range = range_buckets;
figure(2)
subplot(3,3,1)
plot_with_area(range,mean(real(results(:,:,1)')),std(real(results(:,:,1)')),[0,0,1],0.5)
%plot(range_dt,mean(real(results(:,:,1)),2))
hold on 
plot(range,expected(:,1))
hold off
legend('prediction mean','prediction std','expected')
title('D')
subplot(3,3,2)
%plot(range_dt,mean(real(results(:,:,2)),2))
plot_with_area(range,mean(real(results(:,:,2)')),std(real(results(:,:,2)')),[0,0,1],0.5)
hold on 
plot(range,expected(:,2))
hold off
title('tau')
subplot(3,3,3)
%plot(range_dt,mean(real(results(:,:,3)),2))
plot_with_area(range,mean(real(results(:,:,3)')),std(real(results(:,:,3)')),[0,0,1],0.5)
hold on 
plot(range,expected(:,3))
hold off
title('m')
subplot(3,3,4)
%plot(range_dt,mean(real(results(:,:,4)),2))
plot_with_area(range,mean(real(results(:,:,4)')),std(real(results(:,:,4)')),[0,0,1],0.5)
hold on 
plot(range,expected(:,4))
hold off
title('sigma vel')
subplot(3,3,5)
%plot(range_dt,mean(real(results(:,:,5)),2))
plot_with_area(range,mean(real(results(:,:,5)')),std(real(results(:,:,5)')),[0,0,1],0.5)
hold on 
plot(range,expected(:,5))
hold off
title('sigma pos')
subplot(3,3,6)
plot(range,mean(real(results(:,:,3)),2).*range(:)+1)
hold on 
plot(range,expected(:,3).*range(:)+1)
hold off
title('m*dt+1')
subplot(3,3,7)
%sum(imag(results(:,:,1))~=0,2)
plot(range,sum(imag(results(:,:,1))~=0,2)/now)
title('relative number of values with imaginary part D')
%sum(imag(results(:,:,1))~=0,2)
subplot(3,3,8)
plot(range,sum(imag(results(:,:,2))~=0,2)/now)
title('relative number of values with imaginary part tau')
subplot(3,3,9)
plot(range,2*range(:)./expected(:,2))
%hold on
%plot(range_dt,1)
%hold off
title('2*dt/tau')
%% semi log
results_tmp = results;
results_filter = repmat(any(imag(results_tmp)~=0,3),[1,1,size(results_tmp,3)]);
results_tmp(results_filter) = nan;
figure(2)
subplot(3,3,1)
semilogx(range_dt,nanmean(real(results_tmp(:,:,1)),2),range_dt,expected(:,1))
grid on
legend('prediction','expected','Location','southwest')
title('D')
subplot(3,3,2)
%plot(range_dt,mean(real(results(:,:,2)),2))
semilogx(range_dt,nanmean(real(results_tmp(:,:,2)),2),range_dt,expected(:,2))
grid on
title('tau')
subplot(3,3,3)
%plot(range_dt,mean(real(results(:,:,3)),2))
semilogx(range_dt,nanmean(real(results_tmp(:,:,3)),2),range_dt,expected(:,3))
grid on
title('m')
subplot(3,3,4)
%plot(range_dt,mean(real(results(:,:,4)),2))
semilogx(range_dt,nanmean(real(results_tmp(:,:,4)),2),range_dt,expected(:,4))
grid on
title('sigma vel')
subplot(3,3,5)
%plot(range_dt,mean(real(results(:,:,5)),2))
semilogx(range_dt,nanmean(real(results_tmp(:,:,5)),2),range_dt,expected(:,5))
grid on
title('sigma pos')
subplot(3,3,6)
semilogx(range_dt,nanmean(real(results_tmp(:,:,3)),2).*range_dt(:)+1)
grid on
title('m*dt+1')
subplot(3,3,7)
semilogx(range_dt,sum(imag(results_tmp(:,:,1))~=0,2)/now)
grid on
title('relative number of values with imaginary part D')
%sum(imag(results(:,:,1))~=0,2)
subplot(3,3,8)
%semilogx(range_dt,nanmean(real(results_tmp(:,:,1)),2),range_dt,expected(:,1))
%grid on
plot(1,1)
%plot(range_dt,sum(imag(results(:,:,2))~=0,2)/now)
%title('relative number of values with imaginary part tau')
subplot(3,3,9)
semilogx(range_dt,2*range_dt(:)./expected(:,2))
grid on
title('2*dt/tau')

%% noise sigma

D = 0.003;
sigma_pos = 0.1;
it = 100;
n = 1;
range_dt = linspace(0.001,100,100);
range_D = linspace(0.0001,0.01,100);
range_buckets = linspace(11,1001,100);

expected = zeros(it,5);
for i = 1:numel(range_dt)
    dt = range_dt(i);
    %dt = 10.01;
    %D = range_D(i);
    %D = 0.003;
    %n_buckets = range_buckets(i);
    n_buckets = 10001;
    
    tau = sigma_pos^2/D/n;
    m = get_m(dt,tau);
    sigma_vel = get_sigma_vel(D,dt,tau);
    sigma_vel1 = get_sigma_vel(D,dt,tau)-0.001;
    sigma_vel2 = get_sigma_vel(D,dt,tau)+0.001;
    D_verfiy = get_D(dt,sigma_vel,m);
    D_verfiy1 = get_D(dt,sigma_vel1,m);
    D_verfiy2 = get_D(dt,sigma_vel2,m);
    
    expected(i,:) = [D_verfiy,D_verfiy1,D_verfiy2,sigma_vel,sigma_vel2];
end
range = range_dt;
figure(4)
plot(range,expected(:,1))
hold on
plot(range,expected(:,2))
plot(range,expected(:,3))
legend('D','D from sigma_{vel}-0.001','D from sigma_{vel}+0.001');
title('influence of noise on D')
hold off


%% noise m

D = 0.003;
sigma_pos = 0.1;
it = 100;
n = 1;
range_dt = linspace(0.001,100,100);
range_D = linspace(0.0001,0.01,100);
range_buckets = linspace(11,1001,100);

expected = zeros(it,5);
for i = 1:numel(range_dt)
    dt = range_dt(i);
    %dt = 10.01;
    %D = range_D(i);
    %D = 0.003;
    %n_buckets = range_buckets(i);
    n_buckets = 10001;
    
    tau = sigma_pos^2/D/n;
    m = get_m(dt,tau);
    sigma_vel = get_sigma_vel(D,dt,get_tau(dt,m));
    noise = -1.3932e-04;
    sigma_vel1 = get_sigma_vel(D,dt,get_tau(dt,m-noise));
    sigma_vel2 = get_sigma_vel(D,dt,get_tau(dt,m+noise));
    D_verfiy = get_D(dt,sigma_vel,m);
    D_verfiy1 = get_D(dt,sigma_vel1,m);
    D_verfiy2 = get_D(dt,sigma_vel2,m);
    
    expected(i,:) = [D_verfiy,D_verfiy1,D_verfiy2,sigma_vel,sigma_vel2];
end
range = range_dt;
figure(4)
plot(range,expected(:,1))
hold on
plot(range,expected(:,2))
plot(range,expected(:,3))
find(any(imag(expected)~=0,2),1,'first')
legend('D','D from m-0.001','D from m+0.001');
title('influence of noise on D')
hold off



%}
