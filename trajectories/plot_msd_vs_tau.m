function plot_msd_vs_tau(cycle_list,cycle_ids,foci_selector,absolute_position,offsets,dt,ptmr)
    %dt = 1;
    %ptmr = 0.0668;
    %offsets = 1:60;
    %absolute_position = false;
    %cycle_ids = find([cycle_list.duration]==60);%for short timescale data
    %cycle_ids = complete_cycle_ids;
    %foci_collector = [2];%select all trajectories with these many foci to calculate the MSD
    trajectory_all = cell(1,numel(foci_selector));
    for foci_it = 1:numel(foci_selector)
        fluor_selector = 1;
        fs = foci_selector(foci_it);
        trajectory_min_length = 12;%12;
        interpolate_points_in_a_row = 0;
        set_mean_to_0 = false;
        flip_by_pole = false;
        seperate = true;
        remove_bb = false; %only for old data sets
        trajectory_list = get_trajectories(cycle_ids,cycle_list,fs,trajectory_min_length,fluor_selector,interpolate_points_in_a_row,set_mean_to_0,flip_by_pole,seperate,remove_bb);
        trajectory_all{foci_it} = trajectory_list;
    end

    trajectory_all = cat(2,trajectory_all{:});
    longest_trajectory = max(cellfun(@(x) numel(x)/2,{trajectory_all.positions}));
    position_matrix = nan(numel(trajectory_all),longest_trajectory);
    length_matrix = nan(numel(trajectory_all),longest_trajectory);
    for i = 1:numel(trajectory_all)
        tra = trajectory_all(i);
        pos = tra.positions(:,1);
        %pos = pos-mean(pos);
        if absolute_position
            length_matrix(i,1:(tra.stop-tra.start+1)) = tra.lengths;
        else
            bbs = cycle_list(tra.cycle_id).bbs(tra.start:tra.stop,:,:);
            pos = pos+tra.lengths/2+bbs(:,1,1);
        end
        position_matrix(i,1:(tra.stop-tra.start+1)) = pos;
    end

    %
    msd_all = nan(1,numel(offsets));
    mean_squared_displacements = nan(numel(offsets),numel(trajectory_all));
    for offset_it = 1:numel(offsets)
        offset = offsets(offset_it);
        if absolute_position
            factor = length_matrix(:,(offset+1):end)./length_matrix(:,1:end-offset);
        else
            factor = 1;
        end
        displacement = position_matrix(:,(offset+1):end)-position_matrix(:,1:end-offset).*factor;
        displacement = displacement * ptmr;
        squared_displacement = displacement.^2;
        mean_squared_displacement = mean(squared_displacement,2,'omitnan');
        mean_squared_displacements(offset_it,:) = mean_squared_displacement;
        msd_all(offset_it) = median(mean_squared_displacement,'omitnan');
    end

    %
    tau_all = offsets*dt;
    %ylimit = [0,0.15];
    %ylimit = [0,0.6];
    msd_mean = mean(mean_squared_displacements,2,'omitnan');
    msd_std = std(mean_squared_displacements,[],2,'omitnan');
    figure(1)
    subplot(1,2,1)
    scatter(tau_all,msd_mean,5,'filled','MarkerFaceColor','r')
    hold on
    scatter(repmat(tau_all,[1,numel(trajectory_all)]),mean_squared_displacements(:),'filled','MarkerFaceColor','b','MarkerEdgeColor','b',...
        'MarkerFaceAlpha',.02,'MarkerEdgeAlpha',0)
    scatter(tau_all,msd_mean,7,'filled','MarkerFaceColor','r')
    hold off
    title('MSD')
    legend('mean','all')
    %ylim(ylimit)
    xlim([tau_all(1),tau_all(end)])
    xlabel('\tau (s)')
    ylabel('MSD (\mum^2)')

    subplot(1,2,2)
    plot_with_err(tau_all,msd_mean,msd_std,[0,0,1],0.2)
    mdl = fitlm(tau_all,msd_mean,'weights',1./var(mean_squared_displacements,[],2,'omitnan'))
    hold on
    plot(tau_all,tau_all*mdl.Coefficients{2,1}+mdl.Coefficients{1,1})
    hold off
    title('Fit')
    %ylim(ylimit)
    xlim([tau_all(1),tau_all(end)])
    xlabel('\tau (s)')
    ylabel('MSD (\mum^2)')
    l3 = strcat("fit:",string(newline),"D=",string(mdl.Coefficients{2,1}/2),string(newline),"inter=",string(mdl.Coefficients{1,1}))
    legend({'mean','std',l3})
end