%takes a list of weights/counts (x) and returns an logical vector of the lowest 1-c_o of it. E.g
%[1,1,0,1,1] and a c_o of 0.8 would [0,0,1,0,0]
function [filter_indices] = cut_off(x,c_o)
    if ~(size(x,1)==1),x=x';end
    total = sum(x);
    indices = 1:numel(x);
    sort_this = cat(1,x,indices);
    sort_this = sortrows(sort_this',1,'descend');
    smaller_than_cut_off = cumsum(sort_this(:,1))/total<c_o;
    filter_indices = ismember(indices,sort_this(smaller_than_cut_off,2));
end