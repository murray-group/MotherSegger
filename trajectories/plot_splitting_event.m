function [canvas] = plot_splitting_event(cycle_list,cycle_ids,fluor_selector,before,after)
    %fluor_selector = 1;
    trajectory_min_length = 6;
    interpolate_points_in_a_row = 1;
    set_mean_to_0 = false;
    flip_by_pole = true;
    seperate = false;
    remove_bb = false; %only for old data sets
    %cycle_ids = complete_cycle_ids;
    trajectory_list1 = get_trajectories(cycle_ids,cycle_list,1,trajectory_min_length,fluor_selector,interpolate_points_in_a_row,set_mean_to_0,flip_by_pole,seperate,remove_bb);
    trajectory_list2 = get_trajectories(cycle_ids,cycle_list,2,trajectory_min_length,fluor_selector,interpolate_points_in_a_row,set_mean_to_0,flip_by_pole,seperate,remove_bb);
    %trajectory_list_border = get_border_cell_trajectories(cycle_ids,cycle_list,foci_selector,trajectory_min_length,fluor_selector,interpolate_points_in_a_row,set_mean_to_0,flip_by_pole,seperate,remove_bb);
    tra_li_cy2 = [trajectory_list2.cycle_id];
    %
    candidates1 = zeros(0,1);
    candidates2 = zeros(0,1);
    for i = 1:numel(trajectory_list1)
        tra = trajectory_list1(i);
        indices = find(tra_li_cy2 == tra.cycle_id);

        for j = 1:numel(indices)
            tra2 = trajectory_list2(indices(j));
            if tra.stop+1==tra2.start
                candidates1(numel(candidates1)+1) = i;
                candidates2(numel(candidates2)+1) = indices(j);
            end
        end
    end
    %
    tra_l1 = trajectory_list1(candidates1);
    tra_l2 = trajectory_list2(candidates2);
    l1 = max([tra_l1.stop]-[tra_l1.start]+1,[],'all');
    l2 = max([tra_l2.stop]-[tra_l2.start]+1,[],'all');
    n_buckets = 20;
    canvas = zeros(n_buckets,l1+l2);
    for i = 1:numel(tra_l1)
        tra = tra_l1(i);
        pos = tra.positions(:,:,1);
        for k = size(pos,2)
            p = pos(:,k);
            buckets = round((p./tra.lengths+0.5)*(n_buckets-1))+1;
            for j = 1:numel(buckets)
                canvas(buckets(end-j+1),l1-j+1)=canvas(buckets(end-j+1),l1-j+1)+1;
            end
        end
        %break
    end
    for i = 1:numel(tra_l2)
        tra = tra_l2(i);
        pos = tra.positions(:,:,1);
        for k = 1:size(pos,2)
            p = pos(:,k);
            buckets = round((p./tra.lengths+0.5)*(n_buckets-1))+1;
            buckets(buckets<1) = 1;
            buckets(buckets>n_buckets)=n_buckets;
            for j = 1:numel(buckets)
                canvas(buckets(j),l1+j)=canvas(buckets(j),l1+j)+1;
            end
        end
        %break
    end
    canvas = canvas./sum(canvas,1);
    imagesc(canvas(:,l1-before+1:l1+after))
    m = mod(before,5);
    d1 = floor(before/5);
    d2 = floor(after/5);
    xtl = (d1*-5):5:(d2*5);
    xt = (0:5:((d1+d2+1)*5))+m+0.5;
    %xt = 3:5:40
    xticks(xt)
    xticklabels(xtl)
    hold on
    plot([l1,l1]+0.5,[1,n_buckets],'--w')
    hold off
    xlabel('time')
    ylabel('relative position')
end