

function show_annotated_trajectory(trajectory,treshold)

    pos = squeeze(trajectory.positions(:,1,1));
    x = 1:numel(pos);
    xlimit = [1,numel(x)];
    cell_len = trajectory.lengths';
    y1 = cell_len/2;
    y2 = -cell_len/2;
    h = fill([x, fliplr(x)], [y1,fliplr(y2)], [187, 188, 214]/255,'LineStyle','none','facealpha',.5);
    hold on
    plot(-pos,'b')
    ann = trajectory.annotation;
    treshold >= 0
    if treshold >= 0
        'hi'
        ann = ann>treshold;
        colormap(gca,winter(2));
        ticks = [.25,1-.25];
    else
        colormap(gca,winter(200))
        ticks = [.03,1-.03];
    end
    scatter(x,-pos,[],ann,'filled')
    cb = colorbar;
    cb.Ticks = ticks;
    cb.TickLabels = {'rp','os'};
    plot(cat(1,cell_len,-cell_len)'/2,'Color',[149, 125, 173]/500)
    xlim(xlimit)
    hold off
    
    title('trajectory')
    ylabel('position (pixel)')
    xlabel('time (min)')
    ylimit = max(trajectory.lengths)/2*1.2;
    ylim([-ylimit,ylimit])
    legend({'cell','trajectory'})
end