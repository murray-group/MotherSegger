function histogram_trajectories(trajectory_list,cycle_list,binranges,normalized,relative,use_pole,color,alpha)
    %
    x = zeros(0,1);
    for i = 1:numel(trajectory_list)
        trajectory = trajectory_list(i);
        cycle = cycle_list(trajectory.cycle_id);
        
        if use_pole
            pole = cycle.pole;
        else
            pole = 1;
        end
        for j = 1:size(trajectory.positions,2)
            if normalized
                pos = squeeze(trajectory.positions_normalized(:,j,1))*pole;
            else
                pos = squeeze(trajectory.positions(:,j,1))*pole;
            end
            x = cat(1,x,pos);
        end
        %break
    end
    x
    [bincounts] = histc(x,binranges);
    binranges = (binranges(1:end-1)+binranges(2:end))/2;
    bincounts = bincounts(1:end-1);
    %numel(bincounts)
    %numel(binranges)
    if relative
        bincounts = bincounts/sum(bincounts);
    end
    a = area(binranges,bincounts,'LineStyle','none');
    a.FaceAlpha = alpha;
    a(1).FaceColor = color;
    a(1).EdgeColor = color/2;
    hold on 
    plot(binranges,bincounts,'-o','Color',color,'MarkerFaceColor', color)
    hold off
end
