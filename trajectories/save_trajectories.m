function save_trajectories(trajectory_list,name,format)
    if strcmp(format, 'txt')
        save_txt(trajectory_list,strcat(name,'.txt'));
    elseif strcmp(format,'sim')
        save_sim(trajectory_list,name);
    end
end

function save_txt(trajectory_list,path_out)
    fid=fopen(path_out,'W');
    for i = 1:numel(trajectory_list)
        trajectory = trajectory_list(i);
        if size(trajectory.positions,2) == 1
            positions = trajectory.positions(:,1,1);
            for j=1:numel(positions)
                fprintf(fid, string(positions(j)));
                fprintf(fid, ' ');
            end
            fprintf(fid,'\n');
            positions = trajectory.relative_cell_age;
            for j=1:numel(positions)
                fprintf(fid, string(positions(j)));
                fprintf(fid, ' ');
            end
            fprintf(fid,'\n');
        end
    end
    fclose(fid);
end

function save_sim(trajectory_list,base_name)
    if ~exist(base_name, 'dir')
       mkdir(base_name)
    end
    for trajectory_index = 1:numel(trajectory_list)
        trajectory = trajectory_list(trajectory_index);
        parameters = struct();
        parameters.simulation_name = strcat('trajectory_',num2str(trajectory_index,'%06.f'));
        parameters.cycle_id = trajectory.cycle_id;
        parameters.dt = 60;
        parameters.t_fin = parameters.dt*size(trajectory.positions,1);
        parameters.c_e = 1;
        parameters.l0 = 1;
        parameters.w0 = 2;
        parameters.tn_pc = 1;
        parameters.k_off = -1;
        parameters.k_a = -1;
        parameters.k_d = -1;
        parameters.k_off = -1;
        tmp = 25;
        parameters.run = mod(trajectory_index-1,tmp)+1;
        parameters.sim_run = ceil(trajectory_index/tmp);

        output = struct();
        output.p_pc = trajectory.positions_sim;
        output.ct = 60;
        output.time_elapsed = parameters.t_fin;
        output.lengths = trajectory.lengths;
        output.widths = trajectory.widths;
        output.times = trajectory.start:trajectory.stop;
        output.cycle_duration = numel(output.times);
        %figure(2)
        %output.visualization = display_cycle_with_range(cycle,frame_list,cell_list,1,start(j):stop(j));
        result = struct();
        result.parameters = parameters;
        result.output = output;
        result.output.p_pc
        name = strcat(parameters.simulation_name,'__run-',sprintf( '%04d', parameters.run ));
        save_simulation_results(base_name,name,result);
    end
end
