function plot_relative_kymograph(trajectory_list,bin_x,bin_y)

    %bin_x = 100;
    %bin_y = 40;
    canvas = zeros(bin_y,bin_x);
    for tra_it = 1:numel(trajectory_list)
        tra = trajectory_list(tra_it);
        r = tra.relative_cell_age;
        p = tra.positions(:,1,1)./tra.lengths+0.5;
        x = round(r*(bin_x-1))+1;
        p(p<0) = nan;
        p(p>1) = nan;
        y = round(p*(bin_y-1))+1;
        indices = sub2ind([bin_y,bin_x],y',x);
        for index = indices
            if ~isnan(index)
                canvas(index) = canvas(index)+1;
            end
        end
    end
    figure(1)
    canvas = canvas./sum(canvas,1);
    q = quantile(canvas(:),0.95);
    canvas(canvas>q) = q;
    imagesc(canvas)
    xt = 20:20:100;
    xticks(xt)
    xticklabels(xt/100)
    yt = 0:5:bin_y;
    yt = yt(1:end-1);
    yticks(yt)
    yticklabels(yt/bin_y-0.5)
    xlabel('relative cell age')
    ylabel('relative position')
    colorbar()
end