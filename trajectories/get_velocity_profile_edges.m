function [x,means,vars,counts] = get_velocity_profile_edges(trajectory_list,edges,relative)
    %create a list to store all velocities based on their position
    n_bins = numel(edges)-1;
    bin_list = cell(n_bins,1);
    for i = 1:n_bins
        bin_list{i} = zeros(0,1);
    end
    
    %put each velocity into its acording bin
    for i = 1:numel(trajectory_list)
        tra = trajectory_list(i);
        pos = tra.positions;
        for j = 1:size(pos,2)
            %pos2 = pos(:,j,1);
            if relative
                pos_buck = pos(:,j,1)./tra.lengths*2;
            else
                pos_buck = pos(:,j,1);
            end
            %vel = abs(diff(pos2)); for speed
            vel = diff(pos(:,j,1));
            %pos2
            pos_buck = pos_buck(1:end-1);
            buckets = edges<pos_buck;
            [~,buckets]=min(buckets,[],2);
            buckets = buckets -1;
            buckets(buckets==0) = nan;
            
            for bin_it = 1:numel(buckets) 
                if ~isnan(buckets(bin_it)) && ~isnan(vel(bin_it))
                    tmp = bin_list{buckets(bin_it)};
                    tmp(numel(tmp)+1) = vel(bin_it);
                    bin_list{buckets(bin_it)} = tmp;
                end
            end
            %end
        end
    end
    
    center = (edges(1:end-1)+edges(2:end))/2; %half the width of a bucket
    x = center;
    means = cellfun(@mean,bin_list);
    counts = cellfun(@numel,bin_list);
    vars = cellfun(@var,bin_list);
    
end