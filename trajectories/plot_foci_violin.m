function plot_foci_violin(cycle_list,cycle_ids,fluor_name,ptmr)
    lengths = cat(1,cycle_list(cycle_ids).lengths);
    foci_counts = zeros(size(lengths));
    foci_pointer = 1;
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        foci_count = sum(~isnan(cycle.(fluor_name).intensity),2);
        if numel(foci_count)
            foci_count(foci_count>5) = 5;% test
            foci_counts(foci_pointer:foci_pointer+cycle.duration-1) = foci_count;
        end
        foci_pointer = foci_pointer+cycle.duration;
    end
    max_foci = max(foci_counts);
    foci_unique = unique(foci_counts);
    %foci_unique = foci_unique(2:end);
    n_points = zeros(1,max_foci);
    for foci_it = 1:numel(foci_unique)
        n_points(foci_it) = sum(foci_counts==foci_unique(foci_it));
    end
    max_points = max(n_points);
    res = nan(max_points,numel(foci_unique));
    numel(foci_unique)
    for foci_it = 1:numel(foci_unique)
        l = foci_counts==foci_unique(foci_it);
        p = lengths(l);
        res(1:numel(p),foci_it) = p;%/foci_unique(foci_it);
    end
    colors = get_default_color();
    clf
    res;
    violin2(res*ptmr,'facecolor',colors,'facealpha',1);
    xticklabels(string(foci_unique'))
    xlabel('number of foci')
    ylabel('length')
    title('Foci Distributions')
    
    
    ax=gca ; 
    ax.FontSize = 7 ;
    yt = 0.5:0.5:10;
    yticks(yt)
    yt = string(yt);
    yt(1:2:end) = '';
    yticklabels(yt);
    
end