function [x,means,vars,counts] = get_velocity_profile(trajectory_list,n_bins,relative_position)
    %find min and max pos (long-axis) in trajectory list
    if relative_position
        min_pos = -.5;
        max_pos = .5;
    else
        min_pos = 0;
        max_pos = 0;
        for i = 1:numel(trajectory_list)
            tra = trajectory_list(i);
            pos = tra.positions(:,:,1);
            %check for new min or max
            min_pos2 = min(pos(:));
            max_pos2 = max(pos(:));
            if min_pos > min_pos2,min_pos = min_pos2;end
            if max_pos < max_pos2,max_pos = max_pos2;end
        end
    end
    %create a list to store all velocities based on their position
    bin_list = cell(n_bins,1);
    for i = 1:n_bins
        bin_list{i} = zeros(0,1);
    end
    
    %put each velocity into its acording bin
    for i = 1:numel(trajectory_list)
        tra = trajectory_list(i);
        pos = tra.positions;
        for j = 1:size(pos,2)
            pos2 = pos(:,j,1);
            %vel = abs(diff(pos2)); for speed
            vel = diff(pos2);
            
            if relative_position,pos2 = pos2./tra.lengths;end
            
            pos2 = pos2(1:end-1);
            buckets = round((pos2-min_pos)/(max_pos-min_pos)*n_bins+1);
            buckets(buckets>n_bins) = n_bins;
            buckets(buckets<1) = 1;
            for bin_it = 1:numel(buckets) 
                if ~isnan(buckets(bin_it)) && ~isnan(vel(bin_it))
                    tmp = bin_list{buckets(bin_it)};
                    tmp(numel(tmp)+1) = vel(bin_it);
                    bin_list{buckets(bin_it)} = tmp;
                end
            end
            %end
        end
    end
    
    center = (max_pos-min_pos)/n_bins/2; %half the width of a bucket
    x = linspace(min_pos+center,max_pos-center,n_bins);
    means = cellfun(@mean,bin_list);
    counts = cellfun(@numel,bin_list);
    vars = cellfun(@var,bin_list);
    
end