
%return the trajectories of a given set of cycles with the given cycle_ids
% - foci_selector: only return trajectories with this given number of foci
% - trajectory_length_filter: only return trajectories with a length
%                             greather or eaul to trajectory_length_filter
% - trajectory_score_filter: only return trajectories with a score greater
%                            or equal to trajectory_score_filter
% - foci_index: only return foci from the fluorescent signal with the index
%               foci_index
function [trajectory_list] = get_trajectories(cycle_ids,cycle_list,foci_selector,trajectory_length_filter,foci_index,interpolate,set_mean_to_0,flip_by_pole,seperate,remove_bb)%,ptmr)
    
    ffn = strcat('foci',string(foci_index)); %foci field name
    n_trajectory = 0; 
    for i = 1:numel(cycle_ids)
        cycle_id = cycle_ids(i);
        cycle = cycle_list(cycle_id);
        foci_positions = cycle.(ffn).position;
        %------------------------interploation-----------------------------------
        if interpolate > 0
            n_nans = sum(~isnan(foci_positions(:,:,1)),2);
            max_foci_till_index = zeros(size(n_nans,1),1);
            max_focus = 0;
            for j = 1:numel(max_foci_till_index)
                if n_nans(j)>max_focus
                    max_focus = n_nans(j);
                end
                max_foci_till_index(j) = max_focus;
            end

            b = false;
            start = -1;
            for j = 2:numel(max_foci_till_index)-1 
                if max_foci_till_index(j)>1
                    break
                end
                if n_nans(j) == 0
                    if ~b
                        if n_nans(j-1) == 1
                            b = true;
                            start = j;
                        end
                    end

                    if b
                        if n_nans(j+1) == 1 && j-start+1 <= interpolate
                            before = foci_positions(start-1,:,:);
                            %foci_positions(start:j,:,:)
                            after = foci_positions(j+1,:,:);
                            siwtch_row1 = find(~isnan(before(:,:,1)));
                            siwtch_row2 = find(~isnan(after(:,:,1)));
                            x_interpolation = linspace(before(1,siwtch_row1,2),after(1,siwtch_row2,2),j-start+3);
                            y_interpolation = linspace(before(1,siwtch_row1,1),after(1,siwtch_row2,1),j-start+3);
                            if siwtch_row1 ~= siwtch_row2
                                tmp_row = foci_positions(1:start-1,siwtch_row1,:);
                                foci_positions(1:start-1,siwtch_row1,:) = foci_positions(1:start-1,siwtch_row2,:);
                                foci_positions(1:start-1,siwtch_row2,:) = tmp_row;
                            end

                            foci_positions(start-1:j+1,siwtch_row2,1) = y_interpolation;
                            foci_positions(start-1:j+1,siwtch_row2,2) = x_interpolation;
                        end
                    end
                else
                    b = false;
                end
            end
        end
        
        %find start and end index of individual foci trajectores
        foci_tmp = diff([0;sum(~isnan(foci_positions(:,:,1)),2) == foci_selector;0]);
        start = find(foci_tmp == 1);
        stop = find(foci_tmp == -1)-1;
        for tra_it = 1:numel(start)
            trajectory = foci_positions(start(tra_it):stop(tra_it),:,:);
            trajectory = trajectory(:,sum(isnan(trajectory(:,:,1)),1) == 0,:);
            
            tmp = trajectory(:,:,1);
            tmp = tmp-mean(tmp,2);
            flip_it = (sum(tmp>0,'all')/numel(tmp))<0.5;
                
            if seperate
                sep = size(trajectory,2);
            else
                sep = 1;
            end
            for sep_it = 1:sep%size(trajectory,2)
                if ~seperate
                    sep_it = 1:size(trajectory,2);
                end
                if size(trajectory,1) > trajectory_length_filter

                    if remove_bb
                        tmp = cat(3,cycle.bbs(start(tra_it):stop(tra_it),1,1),cycle.bbs(start(tra_it):stop(tra_it),1,2));
                        tmp = repmat(tmp,[1,size(trajectory,2),1]);
                        trajectory = trajectory - tmp;
                    end
                    ts = struct(); %trajectory struct
                    ts.cycle_id = cycle_id;
                    ts.start = start(tra_it);
                    ts.stop = stop(tra_it);
                    %ts.positions_raw = trajectory;
                    ts.positions = trajectory(:,sep_it,:);
                    ts.positions(:,:,1) = ts.positions(:,:,1)-cycle.lengths(start(tra_it):stop(tra_it))/2;
                    if(flip_by_pole)
                        if foci_selector ~= 3
                            ts.positions(:,:,1) = ts.positions(:,:,1)*cycle_list(cycle_id).pole;
                        else
                            ts.positions(:,:,1) = ts.positions(:,:,1)*(flip_it-0.5)*2;
                        end
                    
                    end
                    if(set_mean_to_0),ts.positions(:,:,1) = ts.positions(:,:,1)-mean(ts.positions(:,:,1));end
                    ts.positions(:,:,2) = ts.positions(:,:,2)-cycle.widths(start(tra_it):stop(tra_it))/2;

                    ts.lengths = cycle.lengths(start(tra_it):stop(tra_it));
                    ts.widths = cycle.widths(start(tra_it):stop(tra_it));
                    ts.relative_cell_age = ((start(tra_it):stop(tra_it))-1)/(cycle.duration-1);

                    if (size(ts.positions,2) == 1 && seperate) || (size(ts.positions,2) == foci_selector && ~seperate)
                        trajectory_list(n_trajectory+1) = ts;
                        n_trajectory = n_trajectory + 1;
                    end
                end
            end
        end
        %{
        for j = 1:numel(start)
            trajectory = foci_positions(start(j):stop(j),:,:);
            trajectory = trajectory(:,sum(isnan(trajectory(:,:,1)),1) == 0,:);
            
            
            if size(trajectory,1) > trajectory_length_filter
                
                if remove_bb
                    tmp = cat(3,cycle.bbs(start(j):stop(j),1,1),cycle.bbs(start(j):stop(j),1,2));
                    tmp = repmat(tmp,[1,size(trajectory,2),1]);
                    trajectory = trajectory - tmp;
                end
                ts = struct(); %trajectory struct
                ts.cycle_id = cycle_id;
                ts.start = start(j);
                ts.stop = stop(j);
                
                ts.positions = trajectory;
                ts.positions(:,:,1) = ts.positions(:,:,1)-cycle.lengths(start(j):stop(j))/2;
                if(flip_by_pole),ts.positions(:,:,1) = ts.positions(:,:,1)*cycle_list(cycle_id).pole;end
                if(set_mean_to_0),ts.positions(:,:,1) = ts.positions(:,:,1)-mean(ts.positions(:,:,1));end
                ts.positions(:,:,2) = ts.positions(:,:,2)-cycle.widths(start(j):stop(j))/2;
                
                ts.lengths = cycle.lengths(start(j):stop(j));
                ts.widths = cycle.widths(start(j):stop(j));
                ts.relative_cell_age = ((start(j):stop(j))-1)/(cycle.duration-1);
                
                if size(ts.positions,2) == foci_selector
                    trajectory_list(n_trajectory+1) = ts;
                    n_trajectory = n_trajectory + 1;
                end
            end
        end
        %}
        
        
        
    end
    n_points = 0;
    for tra_it = 1:n_trajectory
        n_points = n_points + sum(~isnan(trajectory_list(tra_it).positions(:,:,1)),'all');
    end
    disp(strcat("trajectories: ",string(n_trajectory)," | data points: ",string(n_points)))
    trajectory_list = sort_trajectory_by_length(trajectory_list);
end



function [trajectory_list] = sort_trajectory_by_length(trajectory_list)
    n = numel(trajectory_list);
    sort_this = nan(n,2);
    for i = 1:n
        traj = trajectory_list(i);
        sort_this(i,:) = [i,traj.stop - traj.start];
    end
    sort_this = sortrows(sort_this,2,'descend');
    trajectory_list = trajectory_list(sort_this(:,1));
end











