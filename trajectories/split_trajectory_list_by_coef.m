
function [rp_list,os_list] = split_trajectory_list_by_coef(trajectory_list,lag,sw,min_length)
    tra = trajectory_list(1);
    tra.coefs = [0,0,0,0,0,0,0];%add aditional field
    os_list = tra;
    os_counter = 0;
    rp_list = tra;
    rp_counter = 0;
    for i = 1:numel(trajectory_list)
        id = i;%11
        tra = trajectory_list(id);
        [coefs,~,~,indices] = lag_tra(tra,sw,lag);
        if(numel(coefs))%ensures that the trajectory fits at least on sliding window
            %into segments
            behave = coefs>0;
            regime_change = diff(behave)~=0;
            rcp = [1,find(regime_change)+1,numel(coefs)+1];%regime change positions
            rcsas = [rcp(1:end-1);rcp(2:end)-1];%regime change start and stop
            %if(rcsas(2,1)~=0)% to avoid trajectories with too little points
            for j = 1:size(rcsas,2)
                start = indices(rcsas(1,j));
                stop = indices(rcsas(2,j));
                if stop-start+1>=min_length%make an individual trajectory for each fragment
                    tra_frag = tra; %trajectory fragment
                    tra_frag.start = tra.start+start-1;
                    tra_frag.stop = tra.start+stop-1;
                    tra_frag.positions = tra.positions(start:stop,:,:);
                    tra_frag.lengths = tra.lengths(start:stop);
                    tra_frag.widths = tra.widths(start:stop);
                    tra_frag.relative_cell_age = tra.relative_cell_age(start:stop);
                    tra_frag.coefs = coefs(rcsas(1,j):rcsas(2,j));
                    behaviour = behave(rcsas(1,j));
                    if behaviour
                        os_counter = os_counter+1;
                        os_list(os_counter) = tra_frag;
                    else
                        rp_counter = rp_counter+1;
                        rp_list(rp_counter) = tra_frag;
                    end
                end
            end
        end
    end
end