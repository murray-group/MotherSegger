
function [x,means,vars,counts] = plot_velocity_vs_position(trajectory_list,cycle_list,n_buckets,normalize,color,alpha)
    min_pos = 0;
    max_pos = 0;
    bucket_list = cell(n_buckets,1);
    for i = 1:n_buckets
        bucket_list{i} = zeros(0,1);
    end

    for i = 1:numel(trajectory_list)
        trajectory = trajectory_list(i);
        cycle = cycle_list(trajectory.cycle_id);

        for j = 1:size(trajectory.positions,2)
            if normalize
                pos = trajectory.positions_normalized(:,j,1)*cycle.pole;
            else
                pos = trajectory.positions(:,j,1)*cycle.pole;
            end
            if min(pos)<min_pos,min_pos = min(pos);end
            if max(pos)>max_pos,max_pos = max(pos);end
        end
    end

    for i = 1:numel(trajectory_list)
        trajectory = trajectory_list(i);

        for k = 1:size(trajectory.positions,1)
            trajectory.positions(k,:,1) = sort(trajectory.positions(k,:,1));
        end
        cycle = cycle_list(trajectory.cycle_id);

        for j = 1:size(trajectory.positions,2)
            if normalize
                pos = trajectory.positions_normalized(:,j,1)*cycle.pole;
            else
                pos = trajectory.positions(:,j,1)*cycle.pole;
            end
            vel = diff(pos);
            if numel(vel)>0 && ~any(isnan(vel))
                pos = pos(1:end-1);
                buckets = round((pos-min_pos)/(max_pos-min_pos)*n_buckets+1);
                buckets(buckets>n_buckets) = n_buckets;
                for buck_it = 1:numel(buckets)
                    tmp = bucket_list{buckets(buck_it)};
                    tmp(numel(tmp)+1) = vel(buck_it);
                    bucket_list{buckets(buck_it)} = tmp;
                end
            end
        end
    end
    x = linspace(min_pos,max_pos,n_buckets);
    means = cellfun(@mean,bucket_list);
    counts = cellfun(@numel,bucket_list);
    vars = cellfun(@var,bucket_list);
    
    
    empty_buckets = counts == 0;
    middle = round(numel(empty_buckets)/2);
    ebf = find(empty_buckets(1:middle));
    if numel(ebf) > 0
        ebf = ebf(end);
    else
        ebf = 1;
    end
    ebl = find(empty_buckets(middle:end));
    if numel(ebl) > 0
        ebl = ebl(1)+middle-1;
    else
        ebl = numel(empty_buckets);
    end
    keeper = false(numel(empty_buckets),1);
    keeper(ebf:ebl) = true;
    means(~keeper) = nan;
    counts(~keeper) = 0;
    vars(~keeper) = nan;
    
    xlimits = max(abs([min_pos,max_pos]));
    xlimits = [-xlimits,xlimits];
    
    ylimits = [min(means)-max(vars),max(means)+max(vars)];
    
    t = 2;
    plot([xlimits(1)*t,xlimits(2)*t],[0,0],'--k','Color',[0.5,0.5,0.5,0.5])
    hold on
    plot([0,0],[ylimits(1)*t,ylimits(2)*t],'--k','Color',[0.5,0.5,0.5,0.5])
    plot(x,means,'-o','Color',color,'MarkerFaceColor', color ,'MarkerSize',2)
    %errorbar(x,means,stds,'r')
    
    y1 = means+vars;
    y2 = means-vars;
    y1 = y1(~isnan(means))';
    y2 = y2(~isnan(means))';
    x2 = x(~isnan(means));
    h = fill([x2, fliplr(x2)], [y1,fliplr(y2)],color,'LineStyle','none','facealpha',alpha);
    hold off
    xlim(xlimits*1.2)
    ylim(ylimits)
    xlabel('long axis')
    ylabel('velocity')
end
