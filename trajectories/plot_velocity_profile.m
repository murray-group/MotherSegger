
%plot a nice velocity progile
function plot_velocity_profile(trajectory_list,n_bins,c_o,color)
    alpha = 0.5;
    relative_position = false;
    [x,means,vars,counts] = get_velocity_profile(trajectory_list,n_bins,relative_position); %velocity profile
    plot_with_err(x,means,sqrt(vars),color,alpha);%plot part of the velocity profile
    co_filter = cut_off(counts,c_o);%plot these bins in red bins which contain the lowest 0.2 counts
    x(co_filter) = nan;
    means(co_filter) = nan;
    vars(co_filter) = nan;
    hold on
    plot_with_err(x,means,sqrt(vars),[1,0,0],alpha);%plot areas with not enough points
    hold off
end