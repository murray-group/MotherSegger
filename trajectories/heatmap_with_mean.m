function [x,mean_y] = heatmap_with_mean(values_x,values_y,edges_x,edges_y,label_x,label_y,plot_title)
    if(size(values_x,1)==1),values_x=values_x';end
    if(size(values_y,1)==1),values_y=values_y';end
    % middle of bins
    y = (edges_y(1:end-1)+edges_y(2:end))/2;
    x = (edges_x(1:end-1)+edges_x(2:end))/2;
    % get heatmp and normalize along y to 1
    results = histcounts2(values_y,values_x,edges_y,edges_x);
    results_final = results./sum(results,1);
    % modify results for better display
    q = quantile(results_final(:),0.98);
    results_final(results_final==0) = nan;
    mean_y = sum(results_final.*(1:size(results,1))',1,'omitnan');% get mean for each bin
    results_final(results_final>q) = q;% reduce the intensity of high tail so visulization looks better
    % display heatmap
    imagesc(results_final)
    set(gca,'YDir','normal')% invert y axis so it is normal
    colorbar()
    x_range = 1:4:numel(x);% locations for x/y ticks
    y_range = 1:5:numel(y);
    set(gca, 'XTick', x_range, 'XTickLabel', x(x_range));% set x and y ticks
    set(gca, 'YTick', y_range, 'YTickLabel', y(y_range));
    hold on 
    scatter(1:numel(mean_y),mean_y,'r','filled')% plot mean along the x
    hold off
    %labels
    title(plot_title)
    ylabel(label_y)
    xlabel(label_x)
    
    mean_y(mean_y==0) = nan;
    
    mean_y(~isnan(mean_y)) = y(round(mean_y(~isnan(mean_y))));
end