
function [pos,vel]= plot_pos_and_delta_pos(trajectory_list,cycle_list,relative_position)
    pos = zeros(0,1);
    pos_p1 = zeros(0,1);
    pos_p2 = zeros(0,1);
    vel = zeros(0,1);
    for tra_it = 1:numel(trajectory_list)
        tra = trajectory_list(tra_it);
        cycle = cycle_list(tra.cycle_id);
        p = tra.positions(:,:,1);
        if relative_position,p = p./repmat(tra.lengths,[1,size(p,2)])*2;end
        for j = 1:size(p,2)
            pos = cat(1,pos,p);
            vel = cat(1,vel,diff(p));
            if cycle.pole == 1
                pos_p1 = cat(1,pos_p1,p);
            else
                pos_p2 = cat(1,pos_p2,p);
            end
        end
    end
    %%{
    clf
    subplot(1,2,1)
    histogram(pos)
    title(strcat("pos, mean: ",string(mean(pos))," | std: ",string(std(pos))))
    xlabel('position (pixel)')
    ylabel('absolute abundance')
    subplot(1,2,2)
    histogram(vel)
    title(strcat("vel, mean: ",string(mean(vel))," | std: ",string(std(pos))))
    xlabel('velocity (pixel/sampling time)')
    ylabel('absolute abundance')
    %{
    subplot(1,3,3)
    histogram(pos_p1)
    hold on 
    histogram(pos_p2)
    hold off
    title(strcat("vel, mean: ",string(mean(vel))," | std: ",string(std(pos))))
    xlabel('velocity (pixel/sampling time)')
    ylabel('absolute abundance')
    %}
end