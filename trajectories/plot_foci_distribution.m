function plot_foci_distribution(cycle_list,cycle_ids,fluor_name,n_buckets)
    % vs rca
    edges = 0:1/n_buckets:1;
    n_points = 0;
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        n_points = n_points + cycle.duration;
    end
    foci_data = nan(2,n_points);
    foci_data_pointer = 1;
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        duration = cycle.duration;
        foci_count = sum(~isnan(cycle.(fluor_name).intensity),2);
        times = (0:duration-1)/(duration-1);
        buckets = round(times*(n_buckets-1)+1);
        if numel(foci_count)
            foci_data(1,foci_data_pointer:foci_data_pointer+duration-1) = foci_count;
            foci_data(2,foci_data_pointer:foci_data_pointer+duration-1) = buckets;
        end
        foci_data_pointer = foci_data_pointer + duration;
    end
    n_foci = max(foci_data(1,:));
    results = zeros(n_buckets,n_foci+1);
    results_norm = zeros(n_buckets,n_foci+1);
    for buck_it = 1:n_buckets
        res = foci_data(1,foci_data(2,:)==buck_it);
        for foci_it = 0:n_foci
            results(buck_it,foci_it+1) = sum(res==foci_it);
            results_norm(buck_it,foci_it+1) = sum(res==foci_it)/numel(res);
        end
    end
    range = repmat(0:1/(n_buckets-1):1,[n_foci+1,1])';
    subplot(2,2,1)
    area(range,results_norm)
    lgnd = legend('0 Foci','1 Foci','2 Foci','3 Foci','4 Foci','5 Foci');
    set(lgnd,'color','none');
    title("Foci Distribution vs RCA (rel.)")
    xlabel('relative cell age')
    ylabel('relative foci abundance')
    xlim([0,1])
    ylim([0,1])
    subplot(2,2,2)
    area(range,results)
    lgnd = legend('0 Foci','1 Foci','2 Foci','3 Foci','4 Foci','5 Foci');
    set(lgnd,'color','none');
    title("Foci Distribution vs RCA (abs.)")
    xlabel('relative cell age')
    ylabel('absolute foci abundance')
    xlim([0,1])
    % vs length
    n_points = 0;
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        n_points = n_points + cycle.duration;
    end
    foci_data = nan(2,n_points);
    foci_data_pointer = 1;
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        duration = cycle.duration;
        foci_count = sum(~isnan(cycle.(fluor_name).intensity),2);
        if numel(foci_count)
            foci_data(1,foci_data_pointer:foci_data_pointer+duration-1) = foci_count;
            foci_count(foci_count==0) = 1;
            foci_count = 1; %disable this to se length scale
            foci_data(2,foci_data_pointer:foci_data_pointer+duration-1) = round(cycle.lengths./foci_count);
        end
        foci_data_pointer = foci_data_pointer + duration;
    end
    n_foci = max(foci_data(1,:));
    min_length = min(foci_data(2,:));
    max_length = max(foci_data(2,:));
    results_norm = zeros(n_buckets,n_foci+1);
    for buck_it = min_length:max_length
        res = foci_data(1,foci_data(2,:)==buck_it);
        for foci_it = 0:n_foci
            results(buck_it,foci_it+1) = sum(res==foci_it);%/numel(res);
            results_norm(buck_it,foci_it+1) = sum(res==foci_it)/numel(res);
        end
    end
    results_norm(isnan(results_norm)) = 0;
    ptmr = 0.0688;
    subplot(2,2,3)
    len_range = (min_length:max_length)*ptmr;
    range = repmat(len_range,[n_foci+1,1])';
    area(range,results_norm(min_length:max_length,:))
    lgnd = legend('0 Foci','1 Foci','2 Foci','3 Foci','4 Foci','5 Foci');
    set(lgnd,'color','none');
    title("Foci Distribution vs Length (rel.)")
    xlabel('cell length')
    ylabel('relative foci abundance')
    xlim([len_range(1),len_range(end)])
    ylim([0,1])
    subplot(2,2,4)
    len_range = (min_length:max_length)*ptmr;
    range = repmat(len_range,[n_foci+1,1])';
    area(range,results(min_length:max_length,:))
    lgnd = legend('0 Foci','1 Foci','2 Foci','3 Foci','4 Foci','5 Foci');
    set(lgnd,'color','none');
    title("Foci Distribution vs Length (abs.)")
    xlabel('cell length')
    ylabel('absolute foci abundance')
    xlim([len_range(1),len_range(end)])
    %ylim([0,1])
end