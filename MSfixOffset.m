%% load Experiment
base = "D:\ismath\zapB_new_dataset\";
channel_paths = get_channel_paths(base,true);
[channel_list,cycle_list] = load_mothersegger_data(channel_paths,true);
cycle_list = add_lineprofiles(channel_list,cycle_list,1,[1,2]);
complete_cycle_ids = get_complete_cycle_ids(cycle_list);

%% to only check cells which are not the mother cell
mother_cycle_ids = get_mother_cycle_ids(channel_list,cycle_list);
cycle_ids = complete_cycle_ids(~ismember(complete_cycle_ids,mother_cycle_ids));
%% find offset
cycle_ids = complete_cycle_ids;
%lengths = 30:45;%check offset for cells with these lengths
lengths = 40:80;%check offset for cells with these lengths
fluor = 1;%check offsets for these fluorescent channels
n_channels = numel(channel_list);
cycle_lists = cell(1,n_channels);
for channel_it = 1:n_channels, cycle_lists{channel_it} = cycle_list([cycle_list.channel_id] == channel_list(channel_it).channel_id);end

results = zeros(numel(lengths),n_channels);
counts = zeros(numel(lengths),n_channels);
%
disp('determining offset')
parfor channel_it = 1:numel(channel_list)
    if ~mod(channel_it,10)
        disp(strcat(string(channel_it)," out of ",string(numel(channel_list))))
    end
    res = zeros(numel(lengths),1);
    count = zeros(numel(lengths),1);
    tmp = cycle_lists{channel_it};
    %ignore mother cells
    ct = [tmp.cycle_id];
    tmp = tmp(ismember(ct,complete_cycle_ids));
    for j = 1:numel(lengths)
        [o,c] = determine_offset(tmp,1:numel(tmp),lengths(j),fluor,1);
        res(j) = o;
        count(j) = c;
    end
    results(:,channel_it) = -res;
    counts(:,channel_it) = count;
end
%
edges = (min(results(:))-0.25):0.5:(max(results(:))+0.25);
figure(2)
subplot(2,1,1)
histogram(results(:),edges)
xlabel('offset (pixel)')
ylabel('absolute count')
subplot(2,1,2)
bar([channel_list.channel_id],median(results,1,'omitnan'))
xlabel('channel id')
ylabel('offset (pixel)')
ylim(ylim()*1.1)
%%
median(median(results(:,1:217),1))
median(median(results(:,218:end),1))
%% fix offset
channel_paths = get_channel_paths(base,true);
n_channels = numel(channel_paths);
n_fluors = 2;
offsets = zeros(n_channels,n_fluors,2);
%offsets(1:90,1,1) = 3;
offsets(1:122,1,1) = mean(median(results(:,1:122),1),'omitnan');
offsets(123:end,1,1) = mean(median(results(:,123:end),1),'omitnan');
offsets(1:217,2,1) = 0;%mean(median(results(:,1:90),1));
offsets(218:end,2,1) = 0%mean(median(results(:,91:210),1));
%%
%offsets = [-2,0;0,0;0,0];
%offsets = [0,0;0,0;0,0];
disp('fixing offsets')
for channel_it = 1:n_channels
    if ~mod(channel_it,10)
        disp(strcat(string(channel_it)," out of ",string(numel(channel_list))))
    end
    channel_path = channel_paths{channel_it};
    batch_data = load(strcat(channel_path,'mothersegger_data.mat'));
    
    names = {dir(channel_path).name};
    max_foci_index = 0;
    for name_it = 1:numel(names)
        name = names{name_it};
        % offset/shift all fluor images
        if startsWith(name,'fluor')
            fluor_index = str2num(name(numel('fluor')+1));
            if fluor_index > max_foci_index, max_foci_index = fluor_index; end
            fluor = loadTiff(strcat(channel_path,name));
            shape = size(fluor);
            [yl,xl] = get_logical_indices(round(offsets(channel_it,fluor_index,:)),shape(1:2));
            fluor_shifted = zeros(shape,'uint16');
            fluor_shifted(flip(yl),flip(xl),:) = fluor(yl,xl,:);
            writeTiff(strcat(channel_path,'',name),fluor_shifted);
        end
    end
    % offset/shift all foci positions
    %batch_data = load(strcat(channel_path,'mothersegger_data.mat'));
    cycle_list = batch_data.cycle_list;
    for cycle_id = 1:numel(cycle_list)
        cycle = cycle_list(cycle_id);
        fields = fieldnames(cycle);
        foci_fields = fields(startsWith(fields,'foci'));
        for foci_it = numel(foci_fields)
            foci_field = foci_fields{foci_it};
            fluor_index = str2num(foci_field(numel('foci')+1));
            cycle.(foci_field).position(:,:,1) = cycle.(foci_field).position(:,:,1)-offsets(channel_it,fluor_index,1);
            cycle.(foci_field).position(:,:,2) = cycle.(foci_field).position(:,:,2)-offsets(channel_it,fluor_index,2);
        end
        cycle_list(cycle_id) = cycle;
    end
    overwrite_mothersegger_data(cycle_list,batch_data.channel_list)
end
disp('done')

%% functions
[o,c] = determine_offset(tmp,1:numel(tmp),lengths(j),fluor,1);
function [yl,xl] = get_logical_indices(offset,shape)
    yl = (1:shape(1))-offset(1);
    yl = yl > 0 & yl <= shape(1);
    xl = (1:shape(2))-offset(2);
    xl = xl > 0 & xl <= shape(2);
end


function [offset,count] = determine_offset(cycle_list,cycle_ids,len_selector,fluor_index,dim)
    fn = strcat("fluor",string(fluor_index),"_lp");
    lps = zeros(len_selector,2);
    n_lp = [0,0];
    for cycle_it = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(cycle_it));
        if dim == 2
            cell_length_filter = cycle.widths == len_selector;
        elseif dim == 1
            cell_length_filter = cycle.lengths == len_selector;
        end
        if any(cell_length_filter)
            cell_length_filter = find(cell_length_filter)';
            for cell_it = cell_length_filter
                lp = cycle.(fn){cell_it};
                if cycle.pole == 1
                    n_lp(1) = n_lp(1) + 1;
                    lps(:,1) = lps(:,1) + lp;
                else
                    n_lp(2) = n_lp(2) + 1;
                    lps(:,2) = lps(:,2) + lp;
                end
            end
        end
    end
    lps = [lps(:,1)/n_lp(1),lps(:,2)/n_lp(2)];
    lps(:,1) = flipud(lps(:,1));
    %[c,lags] = xcorr(lps(:,1),lps(:,2));
    lps(:,1)=smooth(lps(:,1));
    lps(:,2)=smooth(lps(:,2));
    xc1 = diff(lps(:,1));
    xc2 = diff(lps(:,2));
    [corr,lags] = xcorr(xc1,xc2); %works better with derivative
    [~,cell_it] = max(corr);
    if all(n_lp)
        offset = -lags(cell_it)/2;
    else
        offset = nan;
    end
    count = n_lp(1)*n_lp(2);
    %{
    offset
    figure(13)
    range = 1:len_selector;
    plot(range, lps(:,1))
    hold on
    plot(range+offset, lps(:,2))
    hold off
    if offset > 20
        aa
    end
    aa
    %}
end
