
function [canvas] = scatter_to_heatmap(x,y,n_buckets)

    %n_buckets = [50,60];
    %x_tmp = x_max;
    %y_tmp = y_max;
    maxi = [max(y),max(x)];
    mini = [min(y),min(x)];
    canvas = zeros(n_buckets(1),n_buckets(2));
    for i = 1:numel(x)
        iy = round((y(i)-mini(1))/(maxi(1)-mini(1))*n_buckets(1)+1);
        if iy>n_buckets(1), iy=n_buckets(1);end
        ix = round((x(i)-mini(2))/(maxi(2)-mini(2))*n_buckets(2)+1);
        if ix>n_buckets(2), ix=n_buckets(2);end
        if ~any(isnan([ix,iy]))
            canvas(iy,ix) = canvas(iy,ix) + 1;
        end
    end
end
