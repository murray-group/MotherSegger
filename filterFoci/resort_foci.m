%resort all foci in a given foci list
function [cycle_list] = resort_foci(cycle_list)
    for cycle_id = 1:numel(cycle_list)
        cycle = cycle_list(cycle_id);
        for fluor_index = 1:cycle.number_of_fluors
            foci_field_name = strcat('foci',string(fluor_index));
            if(~isempty(cycle.(foci_field_name).intensity))
                foci_characteristics_list = cat(3,cycle.(foci_field_name).intensity,cycle.(foci_field_name).sigma);
                foci_list = cycle.(foci_field_name).position;
                [sorted_foci_list,sorted_foci_characteristics_list] = sort_foci(foci_list,foci_characteristics_list);
                empty_rows = ~all(isnan(sorted_foci_list(:,:,1)),1);
                cycle.(foci_field_name).position = sorted_foci_list(:,empty_rows,:);
                cycle.(foci_field_name).intensity = sorted_foci_characteristics_list(:,empty_rows,1);
                cycle.(foci_field_name).sigma = sorted_foci_characteristics_list(:,empty_rows,2);
            end
        end
        cycle_list(cycle_id) = cycle;
    end
end
