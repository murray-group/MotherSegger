

function [cycle_list] = filter_foci(cycle_list,cycle_ids,fluor_index,a,b,c,d,e,f)
    %a = -1.2/10;
    %b = 1.2;
    %c = 0;%4.5;
    %d = 0;%0.65;
    filter_fun = @(x,a,b) a*x+b;

    %x = zeros(0,1);
    %y = zeros(0,1);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        fn = strcat('foci',string(fluor_index));
        foci = cycle.(fn);
        l = filter_fun(foci.intensity,a,b)<foci.sigma & foci.sigma > d & foci.intensity > c & filter_fun(foci.intensity,e,f)>foci.sigma;


        cycle.(fn).intensity(~l) = nan;
        cycle.(fn).sigma(~l) = nan;
        cycle.(fn).position(repmat(~l,[1,1,2])) = nan;

        %size(x)
        %if(size(foci.intensity(l(:)))
        %x = cat(1,x,foci.intensity(l(:)));
        %y = cat(1,y,foci.sigma(l(:)));
        cycle_list(cycle_ids(i)) = cycle;
    end
end