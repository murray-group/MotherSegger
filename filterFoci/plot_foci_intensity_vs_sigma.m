

function [x_all,y_all,x_min,y_min,x_max,y_max] = plot_foci_intensity_vs_sigma(cycle_list,cycle_ids,fluor_index)
    x_all = zeros(0,1);
    y_all = zeros(0,1);
    x_max = zeros(0,1);
    y_max = zeros(0,1);
    x_min = zeros(0,1);
    y_min = zeros(0,1);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        fn = strcat('foci',string(fluor_index));
        foci = cycle.(fn);

        [~,indices] = max(cycle.(fn).intensity,[],2);
        if(numel(indices))
            c = sub2ind(size(cycle.(fn).intensity),1:numel(indices),indices');
            c = (ismember(1:numel(cycle.(fn).intensity),c));

            if(size(foci.intensity,2)==1)
                x_max = cat(1,x_max,foci.intensity(c));
                y_max = cat(1,y_max,foci.sigma(c));
            else
                x_max = cat(1,x_max,foci.intensity(c)');
                y_max = cat(1,y_max,foci.sigma(c)');
            end

            [~,indices] = min(cycle.(fn).intensity,[],2);
            c = sub2ind(size(cycle.(fn).intensity),1:numel(indices),indices');
            c = (ismember(1:numel(cycle.(fn).intensity),c));

            if(size(foci.intensity,2)==1)
                x_min = cat(1,x_min,foci.intensity(c));
                y_min = cat(1,y_min,foci.sigma(c));
            else
                x_min = cat(1,x_min,foci.intensity(c)');
                y_min = cat(1,y_min,foci.sigma(c)');
            end

            x_all = cat(1,x_all,foci.intensity(~isnan(foci.intensity)));
            y_all = cat(1,y_all,foci.sigma(~isnan(foci.sigma)));
        end
    end
    figure(4)
    scatter(x_all,y_all,0.8,'filled','g')
    hold on
    scatter(x_min,y_min,0.8,'filled','b')
    scatter(x_max,y_max,0.8,'filled','r')
    hold off
    title('intensity vs sigma')
    xlabel('intensity')
    ylabel('sigma')
    legend({'rest','min','max'})
end