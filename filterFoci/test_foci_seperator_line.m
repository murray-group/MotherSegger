
function test_foci_seperator_line(x_all,y_all,x_min,y_min,x_max,y_max,a,b,c,d,e,f)
    filter_fun = @(x,a,b) a*x+b;
    %c = 0.65;
    %d = 4.5;

    figure(4)
    scatter(x_all,y_all,0.8,'filled','g')
    hold on
    scatter(x_min,y_min,0.8,'filled','b')
    scatter(x_max,y_max,0.8,'filled','r')
    ylimits = ylim;
    xlimits = xlim;
    plot([c,c],[ylimits(1),ylimits(2)],'k')
    plot([xlimits(1),xlimits(2)],[d,d],'k')
    plot([xlimits(1),xlimits(2)],filter_fun([xlimits(1),xlimits(2)],a,b),'k')
    plot([xlimits(1),xlimits(2)],filter_fun([xlimits(1),xlimits(2)],e,f),'k')
    hold off
    xlim(xlimits)
    ylim(ylimits)
    title('intensity vs sigma')
    xlabel('intensity')
    ylabel('sigma')
    legend({'rest','min','max'})
end