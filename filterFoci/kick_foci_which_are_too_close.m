
function [cycle_list] = kick_foci_which_are_too_close(cycle_list,cycle_ids,dis_thresh,fluor_index)
    tmp = zeros(0,1);
    %dis_thresh = 7;
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        fn = strcat('foci',string(fluor_index));
        foci = cycle.(fn);
        %c = ~(filter_fun(foci.intensity)>foci.sigma) & foci.sigma > 0.65 & foci.intensity > 4.5;

        p = foci.position;
        for j = 1:size(p,1)
            pr = p(j,:,:); %positions from one row
            pi = find(~isnan(pr(:,:,1)))';
            if numel(pi)>1
                pi = cat(2,pi,squeeze(foci.intensity(j,pi))');
                sortrows(pi,-2);
                for k = 1:size(pi,1)-1
                    p1 = pr(1,pi(k,1),:);
                    for l = k+1:size(pi,1)
                        p2 = pr(1,pi(l,1),:);
                        dis = vecnorm(p1-p2);
                        if dis < dis_thresh

                            foci.position(j,pi(l,1),:) = nan;
                            foci.intensity(j,pi(l,1)) = nan;
                            foci.sigma(j,pi(l,1)) = nan;
                            %pi(l,:) = nan;
                        end
                        tmp(numel(tmp)+1) = dis;
                    end
                end
            end
        end
        cycle.(fn) = foci;
        cycle_list(cycle_ids(i)) = cycle;
    end
end
