% this one was changed at some point
function [bincounts] = hist_transparent(y,edges,color,alpha)
    [bincounts] = histcounts(y,edges);
    middle = (edges(1:end-1)+edges(2:end))/2;
    hist_transparent_counts(bincounts,middle,true,color,alpha);
end