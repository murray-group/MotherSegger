%dispay_channel shows a overview over a single channel.
% - background_name: which image should be shown in the background (the
%                    given name must a tif-file located at the base folder
%                    of the channl, or 'black' for a black background)
% - signal: foci of which fluorescent signal should be shown
% - show_every: only show every nth frame
function [canvas] = display_channel_with_range(channel,cycle_list,background_name,fluor_index,range,smoothen)

n_frames = channel.n_frames;
if strcmp('black',background_name)
    stack = zeros(channel.shape(1),channel.shape(2),n_frames,'uint16');
else
    stack = loadTiff(convertStringsToChars(strcat(channel.path,background_name,'.tif')));
end

name_foci = strcat('foci',string(fluor_index));
use_contour = true;
iterator = numel(range);

%construct background image first
for it = 1:iterator
    frame_index = range(it);
    tmp = stack(:,:,frame_index);
    
    mini = min(tmp,[],'all');
    maxi = max(tmp,[],'all');
    %tmp = uint16((tmp-mini)/(maxi-mini)*2^16);
    canvas_frame = tmp;
    if it == 1
        canvas = canvas_frame;
    else
        canvas = cat(2,[canvas,canvas_frame]);
    end
    %break
end


figure
canvas = double(canvas);
canvas = canvas/max(canvas,[],'all');
%canvas = cat(3,canvas*0,canvas,canvas*0);
imshow(canvas)%,[mini,maxi])
hold on;

edge_x=0;

%then overlay all the cell contours
for it = 1:iterator
    frame_index = range(it);
    canvas_frame = stack(:,:,frame_index);
    
    %number_of_cells = sum(~isnan(channel.cycle_ids(frame_index,:)));
    for cell_it = 1:size(channel.cycle_ids,2)%number_of_cells
        cycle_id = channel.cycle_ids(frame_index,cell_it);
        if ~isnan(cycle_id)
            %if ismember(cycle_id,complete_cell_cycles)
            cycle_index = channel.cycle_indices(frame_index,cell_it);
            cycle = cycle_list(channel.cycle_ids(frame_index,cell_it));
            bb = squeeze(cycle.bbs(cycle_index,:,:));
            mask = cycle.masks{cycle_index};
            %color = cycle.color*(1-(0.7*~cycle.flags.complete));
            if use_contour
                %mask = mask_to_contour(mask,1);
                %poly=mask2poly(mask);
                 tmp_mask = false(size(mask,1)+2,size(mask,2)+2);
                tmp_mask(2:end-1,2:end-1) = mask;
                mask=imopen(tmp_mask,strel('square',3));
                mask=imclose(mask,strel('square',3));
                [ii,jj]=find(bwperim(mask),1,'first');
                if isempty(ii), continue; end
                poly=bwtraceboundary(mask,[ii,jj],'n',4,inf,'counterclockwise');
                poly=poly(:,[2 1])-1;
                
                if ~exist('smoothen','var'), smoothen=15; end
        
                if smoothen
                    fourierTracedPoints = frdescp(poly);
                    %takes the inverse fourier transform of fourierTracedPoints to get real values. 
                    poly = ifdescp(fourierTracedPoints,smoothen);
                    poly = poly([1:end ,1],:);
                end
                plot(poly(:,1)+edge_x+bb(3)-1,poly(:,2)+bb(1)-1,'Color',cycle.color*(0.7+cycle.flags.complete*0.3))
            end
            
            if fluor_index > 0 && ~isempty(cycle.(name_foci).position)
                positions = squeeze(cycle.(name_foci).position(cycle_index,:,:));
                if(size(positions,2)==1),positions=positions';end
                if ~isempty(positions)
                    for focus_index = 1:size(positions,1)
                        position = positions(focus_index,:);
                        if ~isnan(position(1))
                        %canvas_frame = draw_circle(canvas_frame,position(1),position(2),5,color,30);
                        plot(position(2)+edge_x+bb(3)-1,position(1)+bb(1)-1,'r.')
                        end
                    end
                end
            end
        end
    end
    edge_x=edge_x+size(canvas_frame,2);
end

end
