function channel_quality(channel_list,cycle_list)
    max_frames = 0;
    n_channels = numel(channel_list);
    for channel_id = 1:n_channels%find max frames of all channels
        channel = channel_list(channel_id);
        if channel.n_frames > max_frames,max_frames = channel.n_frames;end
    end
    results = nan(max_frames,n_channels,3);
    for channel_id = 1:n_channels
        channel = channel_list(channel_id);
        for frame_it = 1:channel.n_frames
            good_mass = 0;
            bad_mass = 0;
            idk_mass = 0;
            for cycle_it = 1:size(channel.cycle_ids,2)
                cycle_id = channel.cycle_ids(frame_it,cycle_it);
                if ~isnan(cycle_id)
                    cycle = cycle_list(cycle_id);
                    cell_area = cycle.areas(channel.cycle_indices(frame_it,cycle_it));
                    if cycle.flags.complete
                        good_mass = cell_area;
                    elseif cycle.flags.bad && ~cycle.flags.border_cell
                        bad_mass = cell_area;
                    else
                        idk_mass = cell_area;
                    end
                end
            end
            results(frame_it,channel_id,:) = [good_mass,bad_mass,idk_mass];
        end
    end
    results_norm = results./(sum(results,3));
    subplot(1,3,1)
    bar(permute(mean(results_norm(:,:,:),1,'omitnan'),[2,3,1]),'stacked')
    xlabel('channel')
    ylabel('fraction')
    legend('good','bad','border')
    title('Quality vs Channels')
    ylim([0,1])
    subplot(1,3,2)
    bar(permute(mean(results_norm(:,:,:),2,'omitnan'),[1,3,2]),'stacked')
    xlabel('frame')
    ylabel('fraction')
    legend('good','bad','border')
    title('Quality vs Time')
    ylim([0,1])
    subplot(1,3,3)
    imagesc(results_norm(:,:,1))
    xlabel('channel')
    ylabel('frame')
    title('Channel vs Time')
    colorbar()
end