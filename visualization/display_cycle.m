function [canvas] = display_cycle(cycle,channel_list,padding,background,signal,smoothen)
    range= 1:cycle.duration;
    canvas = display_cycle_with_range(cycle,channel_list,padding,background,signal,range,smoothen);
end