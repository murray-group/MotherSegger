%dispay_channel shows a overview over a single channel.
% - background_name: which image should be shown in the background (the
%                    given name must a tif-file located at the base folder
%                    of the channl, or 'black' for a black background)
% - signal: foci of which fluorescent signal should be shown
% - show_every: only show every nth frame
function [canvas] = display_channel_with_range(channel,cycle_list,background_name,fluor_index,range)

n_frames = channel.n_frames;

stack1 = loadTiff(convertStringsToChars(strcat(channel.path,'phase','.tif')));
stack2 = loadTiff(convertStringsToChars(strcat(channel.path,'fluor1_subtracted','.tif')));

name_foci = strcat('foci',string(fluor_index));
use_contour = true;
iterator = numel(range);

%construct background image first
for it = 1:iterator
    frame_index = range(it);
    
    tmp1 = double(stack1(:,:,frame_index));
    mini1 = min(tmp1,[],'all');
    maxi1 = max(tmp1,[],'all');
    tmp1 = uint16((tmp1-mini1)/(maxi1-mini1)*2^16);
    canvas_frame1 = tmp1;
    
    tmp2 = double(stack2(:,:,frame_index));
    mini2 = min(tmp2,[],'all');
    maxi2 = max(tmp2,[],'all');
    tmp2 = uint16((tmp2-mini2)/(maxi2-mini2)*2^16);
    canvas_frame2 = tmp2;
    if it == 1
        canvas1 = canvas_frame1;
        canvas2 = canvas_frame2;
    else
        canvas1 = cat(2,[canvas1,canvas_frame1]);
        canvas2 = cat(2,[canvas2,canvas_frame2]);
    end
    %break
end
canvas = repmat(canvas1,[1,1,3]);
canvas(:,:,1) = max(cat(3,canvas1,canvas2*3),[],3);
canvas(:,:,2) = max(cat(3,canvas1,canvas2*3),[],3);
image(canvas)
hold on;

edge_x=0;

%then overlay all the cell contours
for it = 1:iterator
    frame_index = range(it);
    canvas_frame = stack1(:,:,frame_index);
    
    %number_of_cells = sum(~isnan(channel.cycle_ids(frame_index,:)));
    for cell_it = 1:size(channel.cycle_ids,2)%number_of_cells
        cycle_id = channel.cycle_ids(frame_index,cell_it);
        if ~isnan(cycle_id)
            %if ismember(cycle_id,complete_cell_cycles)
            cycle_index = channel.cycle_indices(frame_index,cell_it);
            cycle = cycle_list(channel.cycle_ids(frame_index,cell_it));
            bb = squeeze(cycle.bbs(cycle_index,:,:));
            mask = cycle.masks{cycle_index};
            %color = cycle.color*(1-(0.7*~cycle.flags.complete));
            if use_contour
                %mask = mask_to_contour(mask,1);
                poly=mask2poly(mask);
                plot(poly(:,1)+edge_x+bb(3)-1,poly(:,2)+bb(1)-1,'Color',cycle.color)
            end
            
            if fluor_index > 0 
                positions = squeeze(cycle.(name_foci).position(cycle_index,:,:));
                if(size(positions,2)==1),positions=positions';end
                if ~isempty(positions)
                    for focus_index = 1:size(positions,1)
                        position = positions(focus_index,:);
                        if ~isnan(position(1))
                        %canvas_frame = draw_circle(canvas_frame,position(1),position(2),5,color,30);
                        %plot(position(2)+edge_x+bb(3)-1,position(1)+bb(1)-1,'r.')
                        end
                    end
                end
            end
        end
    end
    edge_x=edge_x+size(canvas_frame,2);
end

end
