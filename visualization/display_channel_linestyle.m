function canvas = display_channel_linestyle(channel,cycle_list,signal,range)

    if ~exist('show_ids','var'),show_ids = false;end
    
    range = range(range<=channel.n_frames);
    canvas = zeros(channel.shape(1),numel(range),1);
    frame_iterator = 0;
    ffn = strcat('foci',string(signal)); %foci field name
    
    cycle_ids = channel.cycle_ids(range,:);
    cycle_ids = sort(unique(cycle_ids(~isnan(cycle_ids))));

    %make a color map such that each id has its own color
    colors = reshape([cycle_list(cycle_ids).color],[3,numel(cycle_ids)])';
    colors = double(colors)./2^16;
    complete = arrayfun(@(x) x.flags.complete,cycle_list(cycle_ids));
    colors = colors.*(complete'+1)/2;
    if signal>0
        colors(end+1,:)=1;
        white = cycle_ids(end)+1;%if
    end

    for frame_index = range
        frame_iterator = frame_iterator + 1;
        
        for cell_it = 1:size(channel.cycle_ids,2)%number_of_cells
            cycle_id = channel.cycle_ids(frame_index,cell_it);
            if ~isnan(cycle_id)
                cycle_index = channel.cycle_indices(frame_index,cell_it);
                cycle = cycle_list(channel.cycle_ids(frame_index,cell_it));
                bb = cycle.bbs(cycle_index,:,:);

                canvas(bb(1):bb(2),frame_iterator) = cycle.cycle_id;
                
                if signal > 0
                    positions = cycle.(ffn).position;
                    if numel(positions)
                        for focus_index = 1:size(positions,2)
                            position = positions(cycle_index,focus_index);
                            if ~isnan(position(1))
                                canvas(bb(1)+round(position)-1,frame_iterator) = white;
                            end
                        end
                    end
                end
            end
        end
        
    end

    imagesc(canvas,'AlphaData',canvas~=0);
    set(gca,'color',[0 0 0]);
    colormap(colors)
    clim([min(canvas(canvas~=0),[],'all','omitnan') , max(canvas,[],'all','omitnan')])
end