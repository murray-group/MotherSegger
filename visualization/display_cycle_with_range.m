%shows a cycle with the given range
function [canvas] = display_cycle_with_range(cycle,channel_list,padding,background_name,signal,range,smoothen,tracking)
    if isa(background_name,'char') %if there is only one background name given display a black and white background
        [canvas,~,bb_c] = get_canvas(cycle,channel_list,padding,background_name,range);
    else %if there are multiple background names (up to 3) display an rgb image
        canvas = cell(1,numel(background_name));
        for f_it = 1:numel(background_name)
            b_name = background_name{f_it};
            [canvas_tmp,~,bb_c] = get_canvas(cycle,channel_list,padding,b_name,range);
            canvas_tmp = canvas_tmp./max(canvas_tmp,[],'all');
            canvas{f_it} = canvas_tmp;
        end
        if numel(background_name)==2
            canvas{3}=zeros(size(canvas_tmp));
        end
        canvas = cat(3,canvas{:});
    end
    
    %find smallest and biggest number in the image and display it in that
    %range
    mini = nanmin(canvas,[],'all');
    maxi = nanmax(canvas,[],'all');
    %show the background
    
    figure;

    imshow(canvas,[mini,maxi]);
    hold on;
    
    
    %overlay contours and foci
    for r_it = 1:numel(range)
        %get the mask
        mask = cycle.masks{range(r_it)};
        %poly=mask2poly(mask);
        
                tmp_mask = false(size(mask,1)+2,size(mask,2)+2);
                tmp_mask(2:end-1,2:end-1) = mask;
                mask=imopen(tmp_mask,strel('square',3));
                mask=imclose(mask,strel('square',3));
                [ii,jj]=find(bwperim(mask),1,'first');
                if isempty(ii), continue; end
                poly=bwtraceboundary(mask,[ii,jj],'n',4,inf,'counterclockwise');
                poly=poly(:,[2 1])-1;
        
        if ~exist('smoothen','var'), smoothen=15; end
        
        if smoothen
            fourierTracedPoints = frdescp(poly);
            %takes the inverse fourier transform of fourierTracedPoints to get real values. 
            poly = ifdescp(fourierTracedPoints,smoothen);
            poly = poly([1:end ,1],:);
        end

        %overlay mask for one cell
        plot(poly(:,1)+bb_c(r_it,3)-1,poly(:,2)+bb_c(r_it,1)-1,'Color',cycle.color)
    end
    


    color = colororder*1.2;
    color(color>1)=1;
    for signal_it = 1:numel(signal)
        
        s = signal(signal_it);
        if(s~=-1)
            pos = cycle.(strcat('foci',string(s))).position;
            pos = pos(range,:,:);

            pos(:,:,2) = pos(:,:,2)+bb_c(:,3)-1;
            pos(:,:,1) = pos(:,:,1)+bb_c(:,1)-1;
            
            if ~exist('tracking','var') || ~tracking
                plot(pos(:,:,2),pos(:,:,1),'.','Color',color(signal_it,:),'MarkerSize',12)
            else
                for trajectory_it = 1:size(pos,2)
                    x = pos(:,trajectory_it,2);
                    y = pos(:,trajectory_it,1);
                    f = ~isnan(x) & ~isnan(y);
                    x = x(f);
                    y = y(f);
                    for c = 1:numel(x)-1
                        xt = [x(c),x(c+1)];
                        yt = [y(c),y(c+1)];
                        [xt,yt] = reduce_connection_length(xt,yt,2);
                        plot(xt,yt,'Color',color(signal_it,:))
                    end
                end
            end
        end
    end
    hold off;
end

%reduce the line between the given points by (reduce) on both sides
function [x,y] = reduce_connection_length(x,y,reduce)
    dx = x(2)-x(1);
    dy = y(2)-y(1);
    length_total = sqrt(dx^2+dy^2);
    percent_removed = reduce/length_total;

    x(1) = x(1)+dx*percent_removed;
    x(2) = x(2)-dx*percent_removed;

    y(1) = y(1)+dy*percent_removed;
    y(2) = y(2)-dy*percent_removed;
end


function [canvas,stack,bb_new] = get_canvas(cycle,channel_list,padding,background_name,range)
    %duration = numel(range);%used to interate over range
    channel = channel_list(cycle.channel_id);
    n_frames = channel.n_frames;
    
    if strcmp('black',background_name) || strcmp('',background_name)%use a black background if specified or if no name is given
        stack =zeros(channel.shape(1),channel.shape(2),n_frames,'uint16');
    else %otherwise load the appropriate tiff file
        path_background = convertStringsToChars(strcat(channel.path,background_name,'.tif'));
        stack = loadTiff(path_background);
    end
    
    %get the cell from each frame specified in range
    canvas = cell(numel(range),1);
    bb_new = nan(numel(range),4);
    shape = size(stack);
    for r_it = 1:numel(range)
        cycle_index = range(r_it);
        canvas_frame = stack(:,:,cycle.times(cycle_index));
        bb = cycle.bbs(cycle_index,:,:);

        padding_left = padding;
        padding_right = padding;
        

        if bb(3)-padding_left<1, padding_left = bb(3)-1; end
        if bb(4)+padding_right>shape(2), padding_right = shape(2)-bb(4); end
        
        bb_tmp = [bb(1),bb(2),bb(3)-padding_left,bb(4)+padding_right];
        bb_new(r_it,:) = [1,1 + bb(2)-bb(1),1+padding_left,1+padding_left + bb(4)-bb(3)];
        %widths(r_it)=bb(4)-bb(3)+1;
        canvas{r_it} = canvas_frame(bb_tmp(1):bb_tmp(2),bb_tmp(3):bb_tmp(4));
    end
    
    %find the longest cell on the current frame
    max_length = max(cellfun(@(x) size(x,1),canvas));
    
    for r_it = 1:numel(range)
        cell_im = canvas{r_it};
        cell_padded = nan(max_length,size(cell_im,2));
        height_correction = round((max_length-size(cell_im,1))/2);
        cell_padded(1+height_correction:size(cell_im,1)+height_correction,:) = cell_im;
        %normalize output
        mini = min(cell_padded(cell_padded~=0),[],'all');
        if numel(mini)
            cell_padded = cell_padded-mini;
            cell_padded(cell_padded<0)=0;
        end
        maxi = max(cell_padded,[],'all');
        if maxi ~= 0
            cell_padded = cell_padded./maxi;
        end
        
        bb_new(r_it,1)=bb_new(r_it,1)+height_correction;
        bb_new(r_it,2)=bb_new(r_it,2)+height_correction;

        %add conavas tmp
        canvas{r_it} = cell_padded;
    end

    offset_x = 0;
    for r_it = 2:numel(range)
        offset_x = offset_x + size(canvas{r_it-1},2);
        bb_new(r_it,3)=bb_new(r_it,3)+offset_x;
        bb_new(r_it,4)=bb_new(r_it,4)+offset_x;
    end

    %concatinate individual images into one
    canvas = cat(2,canvas{:});
end