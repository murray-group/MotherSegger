function kymograph_single_cell(cycle,start,stop)
    lengths = cellfun(@numel, cycle.fluor1_lp)
    lengths = lengths(start:stop);
    max_length = max(lengths);
    lps = cycle.fluor1_lp(start:stop);
    canvas = nan(max_length,numel(lps));
    for i = 1:numel(lps)
        lp = lps{i};
        lp = lp/max(lp);
        s_c = floor((max_length-lengths(i))/2)+1;
        canvas(s_c:s_c+lengths(i)-1,i)=lp;
    end
    imagesc(canvas)

    imAlpha=ones(size(canvas));
    imAlpha(isnan(canvas))=0;
    imagesc(canvas,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]);
end