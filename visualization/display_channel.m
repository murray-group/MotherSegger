function [canvas] = display_channel(channel,cycle_list,background_name,signal,show_every,smoothen)

    n_frames = channel.n_frames;    
    range = 1:show_every:n_frames;
    [canvas] = display_channel_with_range(channel,cycle_list,background_name,signal,range,smoothen);
end