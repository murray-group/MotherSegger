function p = mask2poly(mask)
        tmp_mask = false(size(mask,1)+2,size(mask,2)+2);
        tmp_mask(2:end-1,2:end-1) = mask;
        %p=contourc(1:size(tmp_mask,2),1:size(tmp_mask,1),double(tmp_mask),[0.5,0.5])';
        p=contourc(double(imopen(tmp_mask,strel('square',3))),[0.5 0.5])'; 
        p=p(2:end,:);%for some reason the first point is weird
        p=p-1;%correct for the expansion
end 