function [mother_cycle_ids] = get_mother_cycle_ids(channel_list,cycle_list)
    mother_cycle_ids = [];
    for channel_it = 1:numel(channel_list)
        mother_cycle_ids_tmp = [];
        channel = channel_list(channel_it);
        for frame_it = 1:channel.n_frames
            max_level = 0;
            max_id = nan;
            for cycle_it = 1:size(channel.cycle_ids,2)
                cycle_id = channel.cycle_ids(frame_it,cycle_it);
                if ~isnan(cycle_id)
                    cycle = cycle_list(cycle_id);
                    bb = cycle.bbs(channel.cycle_indices(frame_it,cycle_it),:,:);
                    if bb(1) > max_level
                        max_level = bb(1);
                        max_id = cycle_id;
                    end
                end
            end
            if ~isnan(max_id) && cycle_list(max_id).flags.complete && ~ismember(max_id, mother_cycle_ids_tmp)
                mother_cycle_ids_tmp(numel(mother_cycle_ids_tmp)+1) = max_id;
            end
        end
        mother_cycle_ids = cat(2,mother_cycle_ids,mother_cycle_ids_tmp);
    end
end