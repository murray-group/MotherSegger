function hist_transparent_counts(bincounts,middle,density,color,alpha)
    if density
        bincounts = bincounts/trapz(middle,bincounts);
    end
    a = area(middle,bincounts,'LineStyle','none');
    a.FaceAlpha = alpha;
    a(1).FaceColor = color;
    a(1).EdgeColor = color/2;
    hold on 
    plot(middle,bincounts,'-','Color',color,'MarkerFaceColor', color)
    hold off
end