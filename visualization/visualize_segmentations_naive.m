

function visualize_segmentations_naive(segmentations)
    t2 = ceil(sqrt(numel(segmentations)));
    if(numel(segmentations)<=t2*(t2-1))
        t1 = t2-1;
    else
        t1 = t2;
    end
    r = cell(t2,t1);
    figure(101)
    for it = 1:numel(segmentations)
        y = mod(it-1,t1)+1;
        x = (it-y)/t1+1;
        r{y,x} = visualize_segmentation_naive(segmentations{it});
        subplot(t1,t2,it)
        imagesc(r{y,x})
    end
end


function [canvas] = visualize_segmentation_naive(segmentation)
    s = squeeze(segmentation(:,round(size(segmentation,2)/2),:));
    canvas = uint16(zeros(size(s,1),size(s,2),3));
    s(s>0)=1;
    s = flip(s,1);
    for x = 1:size(s,2)
        y = 1;
        c = 0;
        while(y<=size(s,1))
            while(y<=size(s,1)&&s(y,x)==0)
                y = y + 1;
            end
            c = c + 1;
            while(y<=size(s,1)&&s(y,x)==1)
                canvas(y,x,:)=get_color(c);
                y = y + 1;
            end
        end
    end
    canvas = flip(canvas,1);
    %figure(101)
    %imagesc(canvas)
end


function [color] = get_color(index)
    colors = [27072   59497   49475
   41008   55851   65085
   30951   35094   32380
   42086   36226   65060
   64987   17802   58449
   25687   52256   60084
   53225   39644   12206
   38532   32706   54965
   49554   52401   65238
   16274   37248   60240
   59994   53199   46614
   58188   54591   49242
   26040   20996   50064
   64327   14444   31526
   60198   38238   48013
   63215   54042   51047
    7417   35781   10882
   51096   52880   57985
   47444   40464   46316
   49684   40854   35911];
    if index > 20
        index = 20;
    end
    color = colors(index,:);
end