function [contour] = mask_to_contour(mask,width)
        tmp_mask = false(size(mask,1)+2,size(mask,2)+2);
        tmp_mask(2:end-1,2:end-1) = mask;
        contour = xor(tmp_mask,imerode(tmp_mask,strel('square',width*2+1)));
        contour = contour(2:end-1,2:end-1);
end