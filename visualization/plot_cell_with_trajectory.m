function plot_cell_with_trajectory(cycle,fluor_name,color,linewidth,alpha,offset)
    %color = [1,0,0];
    ptmr = 0.0688;
    x = (1:cycle.duration)+offset;
    foci_count = sum(~isnan(cycle.(fluor_name).intensity),2);
    borders = cycle.lengths'/2*ptmr;
    plot(x,borders,'Color',color,'LineWidth',linewidth)
    hold on
    plot(x,-borders,'Color',color,'LineWidth',linewidth)
    h = fill([x, fliplr(x)], [borders,fliplr(-borders)],color,'LineStyle','none','facealpha',alpha);
    seq_indices = find(diff(foci_count)>0);
    seq_indices = [0;seq_indices;cycle.duration];
    seqs_last_n = nan;
    seqs_last = nan;
    for seg_it = 1:numel(seq_indices)-1
        xs = seq_indices(seg_it)+1:seq_indices(seg_it+1);
        seqs = cycle.(fluor_name).position(xs,:,1)*ptmr;
        seqs = seqs(:,~any(isnan(seqs),1),:);
        seqs_n = size(seqs,2);
        seqs = seqs - repmat(borders(xs)',[1,seqs_n]);
        plot(x(repmat(xs',[1,seqs_n])),seqs,'Color',color,'LineWidth',linewidth);
        if ~isnan(seqs_last_n)
            for i = 1:seqs_n
                l = 1000000;
                index = nan;
                for j = 1:seqs_last_n
                    t = abs(seqs_last(end,j)-seqs(1,i));
                    if t<l
                        l = t;
                        index = j;
                    end
                end
                if ~isnan(index)
                    plot(x([xs(1)-1,xs(1)]),[seqs_last(end,index),seqs(1,i)],'Color',color,'LineWidth',linewidth)
                end
            end
        end
        seqs_last_n = seqs_n;
        seqs_last = seqs;
    end
    x
    xlim([x(1),x(end)])
    xlabel('time (min)')
    ylabel('length (\mum)')
    hold off
end