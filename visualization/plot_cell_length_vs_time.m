
function plot_cell_length_vs_time(cycle_list,cycle_ids,ptmr)
    time_max = 0;
    %cycle_ids = 1:numel(cycle_list);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        time = cycle.times(end);
        if time>time_max,time_max=time;end
    end
    time_count = zeros(1,time_max);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        time_count(cycle.times(1):cycle.times(end)) = time_count(cycle.times(1):cycle.times(end))+1;
    end
    time_vs_length = cell(1,time_max);
    for i = 1:time_max
        time_vs_length{i} = zeros(1,time_count(i));
    end
    time_count = ones(1,time_max);
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        for j = 1:numel(cycle.times)
            t = cycle.times(j);
            l = cycle.lengths(j)*ptmr;
            time_vs_length{t}(time_count(t)) = l;
            time_count(t) = time_count(t) + 1;
        end
    end
    x = 1:10:time_max;
    means = cellfun(@mean,time_vs_length);
    std = cellfun(@std,time_vs_length);
    counts = cellfun(@numel,time_vs_length);
    ste = std./sqrt(counts);
    %figure(1)
    plot_with_err(x,means(x),ste(x),[0,0,1],0.2);
    xlabel('sampling time')
    ylabel('length (\mum)');
    legend('mean length','ste')
    title('cell length vs time')
end