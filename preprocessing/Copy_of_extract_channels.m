function [fov] = extract_channels(fov,parameters)
    n_channels = fov.channel_dimensions.n_channels;
    side_padding = parameters.side_padding;
    adjust_phase = parameters.adjust_phase;
    remove_artifacts = parameters.remove_artifacts;
    remove_artifacts_threshold = parameters.remove_artifacts_threshold;
    remove_constriction = parameters.remove_constriction;
    debug = parameters.debug(8);
    %--Align Y--
    %find rough estimate where the constriction starts both in
    %empty and non empty channels
    bb = fov.mask.bb_channels(:,:,1);
    l = bb(2)-bb(1);%length of the channels
    constriction_height = round(fov.channel_dimensions.height_constriction/fov.pixel_to_micrometer_ratio*1/2); %1/2 of the height of the constriction
    padding_constriction = [-l+constriction_height,constriction_height,5,5];%take the lowest 50 pixel of the channel where the constriction is
    [fov] = bb_to_channel(fov,padding_constriction);%extract the part of the channel containing the constriction
    
    
    constriction = find_constriction(fov.channels(1:end-1));%constriction
    constriction_empty = find_constriction(fov.channels(end));%empty constriction

    %find the exact offset between the constictions
    padding_tmp = 5;%determines what the max offset can be
    constriction_img = mean(cat(3,fov.channels(1:end-1).phase),3);
    constriction_diff_img = diff(constriction_img(constriction-padding_tmp:constriction+padding_tmp,:,:));
    constriction_diff_img(constriction_diff_img>0)=0;%diff of constriction (only reducing not increasing)
    
    constriction_empty_img = mean(fov.channels(end).phase(constriction_empty-padding_tmp*2:constriction_empty+padding_tmp*2,:,:),3);
    constriction_empty_diff_img = diff(constriction_empty_img);
    constriction_empty_diff_img(constriction_empty_diff_img>0)=0;%diff of empty constriction (only reducing not increasing), has double the padding to allow aligment
    bb = align_images(constriction_empty_diff_img,constriction_diff_img,false);
    
    
    
    
    offset_through_diff = bb(1)-1;
    offset_through_constriction = constriction_empty-constriction;
    constriction_position_relative = constriction_empty-constriction_height;
    if debug
        disp(strcat("offset found from constriction: ",string(offset_through_constriction)))
        disp(strcat("offset found from diff: ",string(offset_through_diff)))
        disp(strcat("relative constriction position: ",string(constriction_position_relative)))
    end
    %constriction_empty
    %constriction_empty = constriction_empty+offset_through_diff;
    %figure(100)
    %bb = fov.mask.bb_channels(:,:,n_channels);
    %bb_m = fov.mask.bb;
    %img = flip(fov.phase((bb(1)-50:bb(2))+bb_m(1)-1,(bb(3):bb(4))+bb_m(3)-1,100),1);
    %img(constriction_pos_rel:constriction_pos_rel+2,:) = 65000;
    %imagesc(img)
    
    
    if debug
        figure
        subplot(2,2,1)
        imagesc(constriction_empty_img)
        title('empty const')
        subplot(2,2,2)
        imagesc(constriction_empty_diff_img)
        title('diff empty const')
        subplot(2,2,3)
        imagesc(constriction_img)
        title('const')
        subplot(2,2,4)
        imagesc(constriction_diff_img)
        title('diff const')
        sgtitle(strcat('find offset by aligning diff:',string((constriction-constriction_empty)*-1)))
        
        
        figure
        channels = fov.channels;
        for it = 1:n_channels
            subplot(1,n_channels,it)
            imagesc(mean(channels(it).phase,3))
            hold on
            xlimits = xlim;
            if it == n_channels
                plot([xlimits(1),xlimits(2)],[constriction_empty,constriction_empty],'--r')
            else
                plot([xlimits(1),xlimits(2)],[constriction,constriction],'--r')
            end
            hold off
        end
        sgtitle('channels with hightlighted constriction')
    end
    
    %change bb for the empty channel so it aligns the bottom of the bb with the constriction
    bb = fov.mask.bb_channels(:,:,n_channels);
    bb(:,1) = bb(:,1)-constriction_position_relative;%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
    fov.mask.bb_channels(:,:,n_channels) = bb;
    
    %change bb for the non-empty channels so it aligns with the empty channel
    bbs = fov.mask.bb_channels(:,:,1:(n_channels-1));
    bbs(:,1,:) = bbs(:,1,:)-offset_through_diff-offset_through_constriction+constriction_position_relative;%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
    fov.mask.bb_channels(:,:,1:(n_channels-1)) = bbs;
    
    padding = [-80,max(5,remove_constriction),side_padding,side_padding];%padding of the final extraction
    [fov] = bb_to_channel(fov,padding);%final extraction

    %{
    %align X
    figure
    channels = fov.channels;
    for channel_it = 1:n_channels
        phase = max(channels(channel_it).phase,[],3);
        len = size(phase,1);
        phase = phase(round(len*1/4):round(len*3/4),:);
        subplot(1,n_channels,channel_it)
        imagesc(phase)
    end
    sgtitle('channels')
    %}
    
    channel_empty = fov.channels(end);
    if remove_constriction>=0
        channel_empty.phase(end-padding(2)+remove_constriction+1:end,:,:) = 0;%remove constriction from empty channel
    end
    
    %subtract channels 
    channels = fov.channels(1:end-1);
    for channel_it = 1:n_channels-1%for each non empty channel
        save_these_fields = {'phase'};%these fields are saved as .tif at the end of part I
        for field_name = channels(channel_it).image_data_names
            if strcmp(field_name,'phase')%remove the empty channel pxiel by pixel
                phase = double(channel_empty.(field_name))*adjust_phase-double(channels(channel_it).(field_name));
                phase(phase<0)=0;
                %phase = double(channel_empty.(field_name)-channels(channel_it).(field_name));
                phase = phase-min(phase,[],[1,2]);%normalize the output to min 0 and max 2^16-1 
                phase = (2^16-1)-uint16(double(phase)./max(phase,[],[1,2])*(2^16-1));%and invert the image again so it resembles phase contrast
                phase = remove_artificats(phase,remove_artifacts,remove_artifacts_threshold); %remove artifcats if kernel_size >= 0
                channels(channel_it).phase_subtracted = phase;
            else
                add_this_field = strcat(field_name,'_subtracted');%remove background from the from fluor by the mean measured in the empty channel
                channels(channel_it).(add_this_field) = channels(channel_it).(field_name)-uint16(mean(channel_empty.(field_name),[1,2]));
            end
            save_these_fields{numel(save_these_fields)+1} = strcat(field_name,'_subtracted');
        end
        channels(channel_it).save_these_fields = string(save_these_fields);
    end
    
    channel_empty.save_these_fields = channel_empty.image_data_names;
    fov.channel_empty = channel_empty;%save empty channel 
    fov.channels = channels;%save updated channels (added subtraction);

    if debug
        figure
        subplot(2,n_channels,n_channels)
        imagesc(mean(channel_empty.phase,3))
        for it = 1:n_channels-1
            subplot(2,n_channels,it)
            imagesc(mean(channels(it).phase,3))
        end
        for it = 1:n_channels-1
            subplot(2,n_channels,it+n_channels)
            imagesc(mean(channels(it).phase_subtracted,3))
        end
        sgtitle('channels and subtracted channels')
        pause(1)
    end
end

function [phase] = remove_artificats(phase,kernel_size,threshold)
    if kernel_size >= 0
        mask = phase<threshold; %approximate mask of the cells
        mask = ~logical(convn(mask,ones(kernel_size,kernel_size,1),'same'));%dilation+inversion 
        phase(mask) = 65535;%set background to max uint16
    end
end

function [constriction] = find_constriction(channels)
    phase = mean(cat(3,channels.phase),3);
    lp = sum(phase,2);
    change = diff(lp)';
    %figure
    %plot(change)
    %figure
    %imagesc(phase)
    [~,constriction] = min(change);%find the greatest drop in intensity (this is where the constriction starts
end

function [fov] = bb_to_channel(fov,padding)%remove the channels from the fov by using their bounding boxes
    n_channels = fov.channel_dimensions.n_channels;
    bbs = fov.mask.bb_channels;
    bb_mask = fov.mask.bb;
    for it = 1:n_channels
        if it == n_channels %flip padding in y for the empty channel
            padding = padding([2,1,3,4]);
        end
        
        %new bb for the current channel
        fov.channels(it).image_data_names = fov.image_data_names;
        bb = [bb_mask(1)+bbs(1,1,it)-1-padding(1),bb_mask(3)+bbs(1,2,it)-1-padding(3); ... 
            bb_mask(1)+bbs(2,1,it)-1+padding(2),bb_mask(3)+bbs(2,2,it)-1+padding(4)];
        fov.channels(it).bb = bb;
        
        %crop the channels
        for field_name = fov.image_data_names
            fov.channels(it).(field_name) = fov.(field_name)(bb(1):bb(2),...
                bb(3):bb(4),:);
        end
    end
    
    %flip empty channel
    for field_name = fov.image_data_names
        fov.channels(n_channels).(field_name) = flip(fov.channels(n_channels).(field_name),1);
    end
    
end

















%%