function [fov] = extract_channels(fov,parameters)
    n_channels = fov.channel_dimensions.n_channels;
    side_padding = parameters.side_padding;
    adjust_empty = parameters.adjust_empty;
    remove_artifacts = parameters.remove_artifacts;
    remove_artifacts_threshold = parameters.remove_artifacts_threshold;
    average_over = parameters.average_over;
    remove_constriction = parameters.remove_constriction;
    reconstruct_empty = parameters.reconstruct_empty;
    debug = parameters.debug(8);
    
    if parameters.new_wafer==0
        %--Align Y--
        %find rough estimate where the constriction starts both in
        %empty and non empty channels
        bb = fov.mask.bb_channels(:,:,1);
        l = bb(2)-bb(1);%length of the channels
        constriction_height = round(fov.channel_dimensions.height_constriction/fov.pixel_to_micrometer_ratio*1/2); %1/2 of the height of the constriction
        padding_constriction = [-l+constriction_height,constriction_height,5,5];%take the lowest pixels of the channel where the constriction is
        [fov] = bb_to_channel(fov,padding_constriction);%extract the part of the channel containing the constriction

        %first align in y using a rough prediction of where the constriction is in
        %the empty and non-empty channels
        constriction = find_constriction(fov.channels(1:end-1));%constriction
        constriction_empty = find_constriction(fov.channels(end));%empty constriction

        %change bb for the empty channel so it aligns the bottom of the bb with the constriction
        bb = fov.mask.bb_channels(:,:,n_channels);
        bb(:,1) = bb(:,1)+(constriction-constriction_empty);%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
        fov.mask.bb_channels(:,:,n_channels) = bb;
        [fov] = bb_to_channel(fov,padding_constriction);%extract the part of the channel containing the constriction

        %instead of a rough estimate where the constriction is use the corr
        %between both empty and non-empty channels to align the channels
        constriction_empty = find_constriction(fov.channels(end));%empty constriction
        constriction_img = mean(cat(3,fov.channels(1:end-1).phase),3);
        padding_max = min([constriction_empty-1,size(constriction_img,1)-constriction_empty]);
        offset_range = padding_max-8;

        if offset_range>0
            constriction_diff_img = diff(constriction_img(constriction_empty-padding_max:constriction_empty+padding_max,:,:));
            constriction_diff_img(constriction_diff_img>0)=0;%diff of constriction (only reducing not increasing)
            constriction_empty_img = mean(fov.channels(end).phase(constriction_empty-padding_max:constriction_empty+padding_max,:,:),3);
            constriction_empty_diff_img = diff(constriction_empty_img);
            constriction_empty_diff_img(constriction_empty_diff_img>0)=0;%diff of empty constriction (only reducing not increasing), has double the padding to allow aligment
            bb = align_images(constriction_diff_img,constriction_empty_diff_img(1+offset_range:end-offset_range,:),false);
            offset = bb(1)-offset_range+1;

            %change bb for the empty channel so it aligns the bottom of the bb with the constriction
            bb = fov.mask.bb_channels(:,:,n_channels);
            bb(:,1) = bb(:,1)+offset;%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
            fov.mask.bb_channels(:,:,n_channels) = bb;
        end

        %find location of contriction in the channel and shift the bb to end
        %directly on the start of the constriction
        [fov] = bb_to_channel(fov,padding_constriction);%extract the part of the channel containing the constriction
        offset_to_constr = constriction_height-find_constriction(fov.channels(end));%empty constriction
        bb = fov.mask.bb_channels;
        bb(:,1,1:end-1) = bb(:,1,1:end-1)-offset_to_constr;%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
        bb(:,1,end) = bb(:,1,end)+offset_to_constr;%offset the bb of the empty channel by the difference between the determined starts of the constrictions 
        fov.mask.bb_channels = bb;
    end
    
    padding = [-80,max(5,remove_constriction),side_padding,side_padding];%padding of the final extraction
    [fov] = bb_to_channel(fov,padding);%final extraction

    %remove constriction from empty channel if specified
    %channel_empty = fov.channels(end);
    if remove_constriction>=0
        %fov.channels(end).phase(end-padding(2)+remove_constriction+1:end,:,:) = 0;
        fov.channels(end).phase(end-remove_constriction+1:end,:,:) = 0;
    end
    
    
    
    if debug
        try
            figure
            for i = 1:n_channels
                subplot(1,n_channels,i)
                imagesc(mean(fov.channels(i).phase,3));
            end
            sgtitle('aligned channels')
            figure
            for i = 1:n_channels
                subplot(1,n_channels,i)
                img = mean(fov.channels(i).phase,3);
                imagesc(img(end-padding(2)-10:end,:));
            end
            sgtitle('aligned consttictions')
        catch ME
            disp("failed to align constrictions")
        end
    end
    
    %remove constriction from empty channel if specified
    %channel_empty = fov.channels(end);
    channel_empty = fov.channels(end);
    if remove_constriction>=0
        channel_empty.phase(end-padding(2)+remove_constriction+1:end,:,:) = 0;
    end
    
    
    % reconstrct the empty channel by doing linear fits along why and using
    % the resulting line to build a new empty channel
    if reconstruct_empty
        disp('reconstructing empty channel')
        phase_empty = double(channel_empty.phase(:,:,:));
        phase_empty_reconstructed = zeros(size(phase_empty));
        range = 1:size(phase_empty,1);
        steps = 2;%take every second point for the linear fit
        start = 16;%start at pixel 16 (a few pixels above the constriction)
        for frame_it = 1:size(phase_empty,3)%for each frame in the empty channel
            frame = phase_empty(start:steps:end-50,:,frame_it);
            parfor i = 1:size(phase_empty,2)%for each column in each frame of the empty channel
                column = frame(:,i);
                x = (1:numel(column))*steps+start/steps;
                mdl = fitlm(x,column);%do the linear fit 
                interception = mdl.Coefficients{1,1};
                slope = mdl.Coefficients{2,1};
                phase_empty_reconstructed(:,i,frame_it) = range*slope+interception;
            end
        end
        %smoothen out the transition between the old channel and the
        %reconstructed one and combine them
        transition = (0:1/start:2)';
        transition(transition>1)=1;
        phase_empty_reconstructed(end-numel(transition)+1:end,:,:) = phase_empty(end-numel(transition)+1:end,:,:).*transition+phase_empty_reconstructed(end-numel(transition)+1:end,:,:).*(1-transition);
        channel_empty.phase = uint16(phase_empty_reconstructed);
    end
    
    %subtract channels 
    channels = fov.channels(1:end-1);
    for channel_it = 1:n_channels-1%for each non empty channel
        save_these_fields = {'phase'};%these fields are saved as .tif at the end of part I
        for field_name = channels(channel_it).image_data_names
            if strcmp(field_name,'phase')%remove the empty channel pxiel by pixel
                phase = double(channel_empty.(field_name))*adjust_empty-double(channels(channel_it).(field_name));
                phase(phase<0)=0;
                %phase = double(channel_empty.(field_name)-channels(channel_it).(field_name));
                phase = phase-min(phase,[],[1,2]);%normalize the output to min 0 and max 2^16-1 
                phase = (2^16-1)-uint16(double(phase)./max(phase,[],[1,2])*(2^16-1));%and invert the image again so it resembles phase contrast
                phase = remove_artificats(phase,remove_artifacts,remove_artifacts_threshold,average_over); %remove artifcats if kernel_size >= 0
                channels(channel_it).phase_subtracted = phase;
            else
                add_this_field = strcat(field_name,'_subtracted');%remove background from the from fluor by the mean measured in the empty channel
                channels(channel_it).(add_this_field) = channels(channel_it).(field_name)-uint16(mean(channel_empty.(field_name),[1,2]));
            end
            save_these_fields{numel(save_these_fields)+1} = strcat(field_name,'_subtracted');
        end
        channels(channel_it).save_these_fields = string(save_these_fields);
    end
    
    channel_empty.save_these_fields = channel_empty.image_data_names;
    fov.channel_empty = channel_empty;%save empty channel 
    fov.channels = channels;%save updated channels (added subtraction);

    if debug
        figure
        subplot(2,n_channels,n_channels)
        imagesc(mean(channel_empty.phase,3))
        for it = 1:n_channels-1
            subplot(2,n_channels,it)
            imagesc(mean(channels(it).phase,3))
        end
        for it = 1:n_channels-1
            subplot(2,n_channels,it+n_channels)
            imagesc(mean(channels(it).phase_subtracted,3))
        end
        sgtitle('channels and subtracted channels')
        pause(1)
    end
end

%% functions
function visualize_constriction(fov,constriction_height,headline)
    constriction_img = mean(cat(3,fov.channels(1:end-1).phase),3);
    constriction_img = constriction_img(end-2*constriction_height:end,:,:);
    constriction_empty_img = mean(fov.channels(end).phase(end-2*constriction_height:end,:,:),3);
    figure
    subplot(1,2,1)
    imagesc(constriction_empty_img)
    title('empty const')
    subplot(1,2,2)
    imagesc(constriction_img)
    title('const')
    sgtitle(headline)
end

function [mask] = get_cell_mask(phase,kernel_size,threshold)
    mask = phase<threshold; %approximate mask of the cells
    mask = ~logical(convn(mask,ones(kernel_size,kernel_size,1),'same'));%dilation+inversion     
end

function [phase_adjusted] = local_normalization(phase,mask,average_over,p)
    phase = double(phase);
    width = size(phase,2);
    height = size(phase,1);
    phase_offset = ones(height,width*(average_over*2+1),size(phase,3))*65535;
    mask_offset = true(height,width*(average_over*2+1),size(phase,3));
    for i = -average_over:average_over
        a1 = max([i,1]);
        a2 = min([height+i-1,height]);
        b2 = height-a1+1;
        b1 = height-a2+1;
        c1 = 1+(width*(i+average_over));
        c2 = width*(i+average_over+1);
        phase_offset(a1:a2,c1:c2,:) = phase(b1:b2,:,:);
        mask_offset(a1:a2,c1:c2,:) = mask(b1:b2,:,:);
    end
    phase_offset(mask_offset) = nan;
    phase_offset(phase_offset>64000) = nan;%just to get rid of numbers which where set to 65535
    min_i = quantile(phase_offset,p(1),2);
    max_i = quantile(phase_offset,1-p(2),2);
    phase_adjusted = (phase-min_i)./(max_i-min_i)*65535;
end

function [phase_adjusted] = remove_artificats(phase,kernel_size,threshold,average_over)
    if kernel_size >= 0
        [mask] = get_cell_mask(phase,kernel_size,threshold);
        phase(mask) = 65535;%set background to max uint16
    end
    if average_over > 0
        [mask] = get_cell_mask(phase,kernel_size,2^16);
        p = [0.1,0.01];
        phase_adjusted = local_normalization(phase,mask,average_over,p);
        mask = get_cell_mask(phase_adjusted,5,20000)|mask;%update mask and make sure that it does not extend further than the previous one
        %figure(2)
        %imagesc(mask(:,:,289))
        phase_adjusted = local_normalization(phase,mask,average_over,p);
        phase_adjusted(mask) = 65535;%set background to max uint16
        phase_adjusted = uint16(phase_adjusted);%set background to max uint16
    else
        phase_adjusted = phase;
    end
end

function [constriction] = find_constriction(channels)
    phase = mean(cat(3,channels.phase),3);
    lp = sum(phase(:,1:round(end/2)),2); %only take the half without the constriction (sounds counterintuitive but it works)
    change = diff(lp)';
    [maxi,peak] = max(change);% find the greatest positive change (transition from constriction to halo)
    [mini,constriction] = min(change(1:peak));%find the greatest drop in intensity (this is where the constriction starts) before the halo starts (ensures that the constriction is picked)
    %{
    figure
    subplot(1,2,1)
    imagesc(phase)
    subplot(1,2,2)
    plot(change)
    hold on
    scatter([constriction,peak],[mini,maxi])
    hold off
    %}
end

function [fov] = bb_to_channel(fov,padding)%remove the channels from the fov by using their bounding boxes
    n_channels = fov.channel_dimensions.n_channels;
    bbs = fov.mask.bb_channels;
    bb_mask = fov.mask.bb;
    for it = 1:n_channels
        if it == n_channels %flip padding in y for the empty channel
            padding = padding([2,1,3,4]);
        end
        
        %new bb for the current channel
        fov.channels(it).image_data_names = fov.image_data_names;
        bb = [bb_mask(1)+bbs(1,1,it)-1-padding(1),bb_mask(3)+bbs(1,2,it)-1-padding(3); ... 
            bb_mask(1)+bbs(2,1,it)-1+padding(2),bb_mask(3)+bbs(2,2,it)-1+padding(4)];
        fov.channels(it).bb = bb;
        
        %crop the channels
        for field_name = fov.image_data_names
            fov.channels(it).(field_name) = fov.(field_name)(bb(1):bb(2),...
                bb(3):bb(4),:);
        end
    end
    
    %flip empty channel
    for field_name = fov.image_data_names
        fov.channels(n_channels).(field_name) = flip(fov.channels(n_channels).(field_name),1);
    end
    
end
