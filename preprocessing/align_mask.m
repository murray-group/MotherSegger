function fov = align_mask(fov,debug)    

    max_phase = max(fov.phase,[],3);
    mask = fov.mask.image;
    %bb = align_images(max_phase,mask,true);%align mask to max projection
    bb = align_images_dft(max_phase,mask,1);
    fov.mask.bb=bb;
    %mean_phase(bb(1):bb(2),bb(3):bb(4)) = mean_phase(bb(1):bb(2),bb(3):bb(4))+mask*50000;
    %figure
    %imagesc(mean_phase)
    
    if debug
        max_phase(bb(1):bb(2),bb(3):bb(4)) = double(max_phase(bb(1):bb(2),bb(3):bb(4)))+double(mask)*double(max(max_phase,[],'all'));
        figure
        subplot(2,1,1)
        imagesc(max_phase)
        title('aligned mask')
    end