function [fov] = stabilize_precise_fov(fov,debug)

    mask = fov.mask.image;
    phase = fov.phase;
    n_frames = fov.n_frames;
    
    offsets = zeros(n_frames,2);  

    parfor it = 1:n_frames
        bb = align_images_dft(phase(:,:,it),mask,1); %align mask against region of interest 
        offsets(it,:) = [bb(1);bb(3)];
    end
    
    offsets = offsets-offsets(round(end/2),:);
    
    for it = 1:n_frames   
        for field_name = fov.image_data_names
            fov.(field_name)(:,:,it)=imtranslate(fov.(field_name)(:,:,it),-offsets(it,[2 1]),'linear','FillValues',0);
        end
    end
    
    for field_name = fov.image_data_names
        fov.(field_name) = uint16(fov.(field_name));
    end
    
    if debug
        figure
        subplot(2,1,1)
        imagesc(mean(phase,3))
        title('before2')
        subplot(2,1,2)
        imagesc(mean(fov.phase,3))
        title('after2')
        sgtitle('stabilizing precies')
        pause(1)
    end
end
