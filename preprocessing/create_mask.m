%create_mask creates a mask with the given sizes of the current field of
%view and with the propper pixel to micrometer ratio
%input:
%-pixel_to_micrometer: ratio of pixel to micrometer
%-channel_dimensions: 
%output:
%-mask: logical matrix (2D)
%-polygons: list of points describing the corners of the individual
%channels
function [mask,bbs] = create_mask(pixel_to_micrometer_ratio,channel_dimensions,debug)
    
    width = channel_dimensions.width;
    height = channel_dimensions.height;
    height_constriction = channel_dimensions.height_constriction;
    pitch = channel_dimensions.pitch;
    n_channels = channel_dimensions.n_channels;
    halo = channel_dimensions.halo;
    
    width = width/pixel_to_micrometer_ratio;%convert micrometer into pixel
    height = height/pixel_to_micrometer_ratio;
    pitch = pitch/pixel_to_micrometer_ratio;
    height_constriction = height_constriction/pixel_to_micrometer_ratio;%constrictions dimensions
    width_constriction = 0.35/pixel_to_micrometer_ratio;
    width_halo = round(halo/pixel_to_micrometer_ratio);
    
    
    y_poly = [0,height+height_constriction,height+height_constriction,height,height,0];%create a polygon of the channel
    x_poly = [width,width,width-width_constriction,width-width_constriction,0,0];%by defining x and y positions of the individual points
    
    channel = poly2mask(x_poly,y_poly,ceil(height+height_constriction),ceil(width));%convert the polygon to a mask
    channel = channel(:,any(channel,1));%crop of empty rows
    channel = channel(any(channel,2),:);%and columns (if there are any)
    cs = find(diff(sum(channel,2))~=0);%find end of channel and start of constriction
    bb = [1,1;cs,size(channel,2)];%defining the bounding box of the channel where the cells reside
    
    top_left_corners = round(((1:n_channels)-1)*(width+pitch)+1);%determine the top left corner of all channels inside of the fov
    bbs = repmat(bb,[1,1,n_channels]);%determine the bounding boxes off all channels
    bbs(:,2,:) = bbs(:,2,:)+permute(top_left_corners,[1,3,2])-1;
    
    mask = false(size(channel,1),top_left_corners(end)+size(channel,2)-1);%create an empty mask
    for it_channel = 1:n_channels-1%add all channels to the mask except the empty one
        mask(1:size(channel,1),top_left_corners(it_channel):top_left_corners(it_channel)+size(channel,2)-1) = channel;
    end
    mask(1:size(channel,1),top_left_corners(end):top_left_corners(end)+size(channel,2)-1) = flip(channel,1);%add the empty channel to the mask by mirroring it along the vertical (y) axis
    bbs(:,1,end) = bbs(:,1,end)+size(channel,1)-cs;%update the bounding box of the empty channel
    
    mask(1:1+width_halo,:) = 1;%add halo to the mask
    mask(end-width_halo:end,:) = 1;
    
    if debug
        figure
        %subplot(2,1,1)
        imagesc(mask)
        title('mask')
        %{
        mask_with_bb_highlighted = mask;
        for it = 1:n_channels
            bb = bbs(:,:,it);
            mask_with_bb_highlighted(bb(1):bb(2),bb(3):bb(4)) = mask_with_bb_highlighted(bb(1):bb(2),bb(3):bb(4))+1;
        end
        subplot(2,1,2)
        imagesc(mask_with_bb_highlighted)
        title('mask withh bbs')
        %}
        pause(1)
    end
end