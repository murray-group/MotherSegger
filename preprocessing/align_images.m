function [bb] = align_images(img,template,complete_overlap)
    c = normxcorr2(template,img);%get correlation of each positon
    %figure(3)
    %imagesc(c)
    if complete_overlap
        c = c(size(template,1):end-size(template,1),size(template,2):end-size(template,2));%only take correlations where images are fully overlapping
        [offset_y,offset_x] = find(c==max(c(:)));%find the greatest correaltion
        %[a,b] = find(c==max(c(:)));%find the greatest correaltion
        %offset_y=a-size(template,1)+1;
        %offset_x=b-size(template,2)+1;
    else
        [offset_y,offset_x] = find(c==max(c(:)));
        offset_y = offset_y-size(template,1)+1;
        offset_x = offset_x-size(template,2)+1;
    end
    bb = [offset_y,offset_x;offset_y+size(template,1)-1,offset_x+size(template,2)-1];%determine the bounding box of the template residing in the image
end
