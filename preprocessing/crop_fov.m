% In this script the fov is cropped. This way only the channels are left and the surrounding removed 
function fov = crop_fov(fov,padding,offsets_fluorescent,debug)

    bb=fov.mask.bb;
    
    for field_name = fov.image_data_names%crop fov so it contains only the channels
        fov.(field_name) = fov.(field_name)(bb(1)-padding:bb(2)+padding,bb(3)-padding:bb(4)+padding,:);
    end
    fov.height = bb(2)-bb(1)+1+2*padding;%new height and width of  the fov
    fov.width = bb(4)-bb(3)+1+2*padding;
    
    bb = [padding+1,padding+1;padding+1+size(fov.mask.image,1),padding+1+size(fov.mask.image,2)];%to use bb: image(bb(1):bb(2),bb(3):bb(4))
    fov.mask.bb = bb;
    
    if debug
        subplot(2,1,2)
        imagesc(max(fov.phase,[],3))
        title('cropped fov')
        pause(1)
    end
    
    stop = min([size(offsets_fluorescent,1),numel(fov.image_data_names)-1])+1;%if specified offset the fluorescent
    for channel = 2:stop
        if any(offsets_fluorescent(channel-1,:))
            
%             y = (1:fov.height)-offsets_fluorescent(channel-1,1);
%             y = y>0 & y<=fov.height;
%             x = (1:fov.width)-offsets_fluorescent(channel-1,2);
%             x = x>0 & x<=fov.width;
% 
%             fov.(fov.image_data_names(channel))(y,x,:) = fov.(fov.image_data_names(channel))(flip(y),flip(x),:);
%             fov.(fov.image_data_names(channel))(~y,:,:) = 0;
%             fov.(fov.image_data_names(channel))(:,~x,:) = 0;

            %allow subpixel translations
            fov.(fov.image_data_names(channel))=imtranslate(fov.(fov.image_data_names(channel)),flip(offsets_fluorescent(channel-1,:)));
        end
    end
end