%this function calculates the best fitting ptmr for the given mask
function [fov,ptmr_new] = accurate_ptmr(fov,debug)
    
    pitch_channel = fov.channel_dimensions.pitch;%get information about fov
    width_channel = fov.channel_dimensions.width;
    height = fov.height;
    width = fov.width;
    n_frames = fov.n_frames;
    middle = round(n_frames/2);

    phase=double(fov.phase(:,:,middle));%take middle frame
    s=mean(phase);%calculate mean intensity along the shot axis

    %[~,locs]=findpeaks(s,'MinPeakDistance',40);

    s = msbackadj([1:width]',s');
    middle=[1:round(width/4)]+3*round(width/8);
    Min=min(s(middle));
    Max=max(s(middle));
    P=mspeaks([1:width]',s,'PeakLocation',0.5,'OverSegmentationFilter',pitch_channel/0.069/2,'HeightFilter',Min+0.2*(Max-Min));
    locs=P(:,1);

    locs=locs(locs>50 & locs<width-50);
    [indices] = rec(locs);
    locs = locs(indices);
    
    test = round(numel(locs)/8);
    locs = locs(1+test:end-test);
    [fo,~]=fit(locs,(pitch_channel+width_channel)*[1:length(locs)]','poly1');
    ptmr_new=fo.p1;
    ci = confint(fo);
    CI=ci(:,1);

    if debug
        disp(['Pixel size is ',num2str(ptmr_new),' (',num2str(CI(1)),', ',num2str(CI(2)),')'])
        figure
        plot(1:width,s)
        hold on 
        l = round(locs);
        scatter(l,s(l))
        hold off
        plot(locs,(pitch_channel+width_channel)*[1:length(locs)]','-o')
        P=mspeaks([1:width]',s,'PeakLocation',0.5,'OverSegmentationFilter',pitch_channel/0.069/2,'HeightFilter',Min+0.2*(Max-Min),'ShowPlot',1,'Style','extline');
        pause(1)
    end
    
    fov.pixel_to_micrometer_ratio = ptmr_new;%update fov
end


function [indices] = rec(locs)%recursion to find the indices of the elements whose derivative deviade from the median of all derivatives
    indices = true(1,numel(locs));
    diffs = diff(locs);
    if(min(diffs)<median(diffs)-3)
        diff_locs = sort_diffs(diffs);
        
        score_current = get_score(locs);%get own score
        
        indices1 = get_indices(diff_locs(1,2),numel(locs));%get score left
        n_locs1 = locs(indices1,:);
        score1 = get_score(n_locs1);
        
        indices2 = get_indices(diff_locs(1,2)+1,numel(locs));%get score right
        n_locs2 = locs(indices2,:);
        score2 = get_score(n_locs2);
        
        if(score1<score_current||score2<score_current)%start recursion if one score if better than current score
            if(score1<score2)%start recursion based on which score is better
                indices1(indices1) = rec(n_locs1);
                indices = indices1;
            else
                indices2(indices2) = rec(n_locs2);
                indices = indices2;
            end
        end
    end
end

function [diff_locs] = sort_diffs(diffs)%sorts the diffs and an attached index list so the index of the smallest element can be found
    diff_locs = zeros(numel(diffs),2);
    diff_locs(:,2) = 1:(numel(diffs));
    diff_locs(:,1) = diffs;
    diff_locs = sortrows(diff_locs,1);
end

function [indices] = get_indices(index,n)%return an logical array containing a vector where only the given index is false
    indices = true(1,n);
    indices(index) = false;
end

function [score] = get_score(locs)%returns a score which is based on the squared distance from the median
    tmp = diff(locs);
    m = median(tmp);
    score = mean((tmp-m).^2);
end