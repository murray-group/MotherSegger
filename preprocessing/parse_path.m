%This script extracts the information given in the fov name (path)
%It extracts the width, height, pitch, number of channels and fov index from it
function [name_info,name] = parse_path(path) 

    if ismac
        slash = '\';%Mac
    elseif isunix
        slash = '/';%linux
    elseif ispc
        slash = '\';%windows
    else
        slash = '/';%other
    end

    splitters = split(path,slash);
    splitters = split(splitters{end},'.');
    name = splitters{1};
    splitters = split(splitters{end-1},'_');
    fov_index = str2num(splitters{2});
    if length(splitters)==6
        row=str2num(splitters{3});%should be 1 or 2
    else
        row=0;%no row information
    end
    width = str2num(splitters{end-2})/10;
    height = str2num(splitters{end-1});
    pitch = str2num(splitters{end});

    number_of_channels = get_number_of_channels(width,pitch);
    name_info = [width,height,pitch,number_of_channels,fov_index,row];
end

function [number_of_channels] = get_number_of_channels(width,pitch) %returns the number of channels depending on channel width and pitch
    number_of_channels = 19;
    if pitch == 2
        if width == 0.7
            number_of_channels = 33;
        end
        if width == 0.8
            number_of_channels = 32;
        end
        if width == 0.9
            number_of_channels = 31;
        end
        if width == 1.0
            number_of_channels = 30;
        end
        if width == 1.1
            number_of_channels = 29;
        end
        if width == 1.2
            number_of_channels = 28;
        end
    end
    if pitch == 4 && width == 1.0
        number_of_channels = 18;
    end


end






