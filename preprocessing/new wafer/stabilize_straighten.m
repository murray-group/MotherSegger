function [fov]=stabilize_straighten(fov,which_circles,debug)
%use circles in the chip design to stabilize and straighten the stacks
tic
independent_circle_finding=0;

%which_circles='top';


r=21;%radius of circles in pixels, found to be the best value

n_frames = fov.n_frames;

ref=round(n_frames/2);%reference frame to use

fov=stabilize_circle(fov,'which_circles',r);% first stabilise the image by finding and aligning one circle


pts=find_circles(fov.phase(:,:,ref),r,debug);%find all circles in ref frame


if debug
    figure
    subplot(2,1,1)
    imagesc(max(fov.phase,[],3));
    hold on;
    x=-r:1:r;
    y=sqrt(r^2-x.^2);
    x=[x'; flipud(x')];
    y=[y'; -y'];
    for i=1:4
        plot(x+pts(i,1),y+pts(i,2),'-r');
    end
    hold off;
    title('before')
end






%find the circles on the other frame ***only works if the drift is less
%than 4 pixels
points_ad=cell(n_frames,1);
distsx=zeros(n_frames,1);


if strcmp(which_circles,'both')
    points_ad{ref}=pts;
end
if strcmp(which_circles,'top')
    points_ad{ref}=pts([1 2],:);
end
if strcmp(which_circles,'bottom')
    points_ad{ref}=pts([3 4],:);
end


distsx(ref)=get_dist(pts,which_circles);

if independent_circle_finding==0
    for i=(ref-1):-1:1
        points_ad{i}=cpcorr_custom(points_ad{i+1},points_ad{ref},fov.phase(:,:,i),fov.phase(:,:,ref));
        distsx(i)=get_dist(points_ad{i},which_circles);
    end
    for i=(ref+1):n_frames
    points_ad{i}=cpcorr_custom(points_ad{i-1},points_ad{ref},fov.phase(:,:,i),fov.phase(:,:,ref));
    distsx(i)=get_dist(points_ad{i},which_circles); 
    end
else
    for i=1:n_frames
    points_ad{i}=find_circles(fov.phase(:,:,i),r,0);
    distsx(i)=get_dist(points_ad{i},which_circles);
    end 
end



p=(2*fov.channel_dimensions.offset+(fov.channel_dimensions.n_channels-1)*(fov.channel_dimensions.width+fov.channel_dimensions.pitch)+fov.channel_dimensions.width)/distsx(ref);
%fov.pixel_to_micrometer_ratio=(fov.channel_dimensions.height-fov.channel_dimensions.height_constriction)./distsy(ref);
s=imref2d(size(fov.phase(:,:,ref)));

%determine transformation (translation,rotation,dilation) required and
%apply to all channels
for i=1:n_frames
    tform = fitgeotrans(points_ad{i},points_ad{ref},'NonreflectiveSimilarity');
    for field_name = fov.image_data_names %apply to all channels
        fov.(field_name)(:,:,i) = imwarp(fov.(field_name)(:,:,i),tform,'OutputView',s);
    end
end

% %rotate by angle
% for field_name = fov.image_data_names %stablize all different channels
%     fov.(field_name) = imrotate3(fov.(field_name),angle,[0 0 1],'linear','crop','FillValues',0);%rotate anti-clockwise
% end
if strcmp(which_circles,'both')
    pts_rect=[pts(1,:);
    pts(1,:)+distsx(ref)*[1,0]; 
    pts(1,:)+(fov.channel_dimensions.height-fov.channel_dimensions.height_constriction)/p*[0,1];
    pts(1,:)+distsx(ref)*[1,0]+(fov.channel_dimensions.height-fov.channel_dimensions.height_constriction)/p*[0,1]];
end
if strcmp(which_circles,'top')
    pts_rect=[pts(1,:);
    pts(1,:)+distsx(ref)*[1,0]]; 
end
if strcmp(which_circles,'bottom')
    pts_rect=[pts(3,:);
        pts(3,:)+distsx(ref)*[1,0]];
end

if strcmp(which_circles,'both')
    tform = fitgeotrans(points_ad{ref},pts_rect,'affine');%transformation to make block rectangular
end
if strcmp(which_circles,'bottom') || strcmp(which_circles,'top')
    tform = fitgeotrans(points_ad{ref},pts_rect,'nonreflectivesimilarity'); %rotations, translations scaling
end

s=imref2d(size(fov.phase(:,:,ref)));
for i=1:n_frames
    for field_name = fov.image_data_names %apply to all channels
        fov.(field_name)(:,:,i) = imwarp(fov.(field_name)(:,:,i),tform,'OutputView',s);
    end
end



if debug
    subplot(2,1,2)
    imagesc(max(fov.phase,[],3))
    title('after')
    sgtitle('stabilizing')
    pause(1)
end
toc
end







% function angle=get_angle(pts)
% a1=atan2d(pts(2,2)-pts(1,2),pts(2,1)-pts(1,1));
% a2=atan2d(pts(4,2)-pts(3,2),pts(4,1)-pts(3,1));
% a3=atan2d(pts(3,1)-pts(1,1),pts(3,2)-pts(1,2));
% a4=atan2d(pts(4,1)-pts(2,1),pts(4,2)-pts(2,2));
% 
% angle=( (a1+ a2)+(-a3-a4) )/4;
% %angle=( 0*(a1+ a2)+(-a3-a4) )/2;%use only vertical angles
% %angle=( (a1+ a2)+0*(-a3-a4) )/2;%use only horizontal amgles
% 
% end

function distx=get_dist(pts,which_circles)
    if strcmp(which_circles,'both') %use top and bottom
        distx=mean([pdist(pts([1 2],:)), pdist(pts([3 4],:))]);
    else
        distx=pdist(pts([1 2],:));
   end


end

function S=is_parallelogram(pts)
S=1;
 if(abs(pdist(pts([1 2],:))-pdist(pts([3 4],:)))>5 || abs(pdist(pts([1 3],:))-pdist(pts([2 4],:)))>5)
     S=0;
 end
end


