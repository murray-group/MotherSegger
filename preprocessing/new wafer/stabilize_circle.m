function fov=stabilize_circle(fov,which_circles,r)

H=size(fov.phase,1);
W=size(fov.phase,2);
n_frames=fov.n_frames;

w=70;%half-width of windows around circle on ref frame, set to max offset w.r.t ref frame
ref=round(n_frames/2);%reference frame to use



if strcmp(which_circles,'both') || strcmp(which_circles,'top')
img_ul=imcrop(fov.phase(:,:,ref),[1,1,round(W/4)-1,round(H/2)-1]);%upper left
[row,col]=find_circ_top(img_ul,r);
else
img_ll=imcrop(fov.phase(:,:,ref),[1,round(H/2)+1,round(W/4)-1,H-round(H/2)-1]);%lower left
[row,col]=find_circ_bottom(img_ll,r);
row=row+round(H/2);
end

img1=fov.phase(max(row-w,1):min(row+r+20+w,H) ,max(col-w,1):min(col+2*r+w,W)  , :);

pts=zeros(n_frames,2);

    if strcmp(which_circles,'both') || strcmp(which_circles,'top')
        for i=1:n_frames
            [pts(i,1), pts(i,2)]=find_circ_top(img1(:,:,i),r);
        end
    else
        for i=1:n_frames
            [pts(i,1), pts(i,2)]=find_circ_bottom(img1(:,:,i),r);
        end
    end


for i=1:n_frames
    for field_name = fov.image_data_names %apply to all channels
        fov.(field_name)(:,:,i) = imtranslate(fov.(field_name)(:,:,i),[pts(ref,2)-pts(i,2),pts(ref,1)-pts(i,1)]);
    end
end


end



function [c] = getCircle(r)
c=zeros(2*r+1,2*r+1);
for i=1:size(c,1)
    for j=1:size(c,2)
        if (i-r-1)^2+(j-r-1)^2<=r^2
            c(i,j)=1;
        end
    end
end

end


function [row,col]=find_circ_top(img,r)

circ=getCircle(r);
circ((2*r+1)+[1:20],:)=0;%dark strip on bottom

r0=r;
half_circ=circ(r0:end,:);%half a circle with dark strip, works better than a full circle but can be slightly off

bb=align_images(img,half_circ,1);%align_images_dft does not work here likely due to the small size of the template compared to the image

row=-(r0-1)+bb(1,1)-1+r;%position of centre of circle
col=bb(1,2)-1+r;
end

function [row,col]=find_circ_bottom(img,r)

circ=getCircle(r);
circ=[zeros(20,2*r+1);circ];%dark strip on top

r0=r;
half_circ=circ(1:end-r0+1,:);

bb=align_images(img,half_circ,1);%align_images_dft does not work here likely due to the small size of the template compared to the image

row=bb(1,1)-1+r+20;%position of centre of circle
col=bb(1,2)-1+r;
end