function fov = mask_offset(fov,which_circles,debug_mask,debug)    

    max_phase = max(fov.phase,[],3);
    
    
    pts=find_circles(max_phase,21,debug);%find circles
    
    dist=get_dist(pts,which_circles);
    
    if fov.pixel_to_micrometer_ratio==0
        fov.pixel_to_micrometer_ratio=(2*fov.channel_dimensions.offset+(fov.channel_dimensions.n_channels-1)*(fov.channel_dimensions.width+fov.channel_dimensions.pitch)+fov.channel_dimensions.width)/dist;
    end
    

    
   
    
    [fov.mask.image,fov.mask.bb_channels] = create_mask_circle(fov.pixel_to_micrometer_ratio,fov.channel_dimensions,debug_mask);
    mask = fov.mask.image;
    
    if strcmp(which_circles,'both') || strcmp(which_circles,'top') 
        col=pts(1,1);%top left circle (reference circle for mask)
        row=pts(1,2);
        y_offset=round(row-fov.channel_dimensions.height_constriction/fov.pixel_to_micrometer_ratio)+1;%+1?
        x_offset=round(col-fov.channel_dimensions.radius/fov.pixel_to_micrometer_ratio)+1;%+1?    
    else
        col=pts(3,1);%bottom left circle (reference circle for mask)
        row=pts(3,2);
        y_offset=round(row-fov.channel_dimensions.height/fov.pixel_to_micrometer_ratio)+1;%+1?
        x_offset=round(col-fov.channel_dimensions.radius/fov.pixel_to_micrometer_ratio)+1;%+1?  
    end
    
    
    
    
    bb=[y_offset, x_offset; y_offset+size(mask,1)-1,x_offset+size(mask,2)-1];
      
    fov.mask.bb=bb;
    

    
    
    
    %mean_phase(bb(1):bb(2),bb(3):bb(4)) = mean_phase(bb(1):bb(2),bb(3):bb(4))+mask*50000;
    %figure
    %imagesc(mean_phase)
    
    if debug
        max_phase(bb(1):bb(2),bb(3):bb(4)) = double(max_phase(bb(1):bb(2),bb(3):bb(4)))+double(mask)*double(max(max_phase,[],'all'));
        figure
        subplot(2,1,1)
        imagesc(max_phase)
        
            hold on;
    r=21;        
    x=-r:1:r;
    y=sqrt(r^2-x.^2);
    x=[x'; flipud(x')];
    y=[y'; -y'];
    for i=1:4
        plot(x+pts(i,1),y+pts(i,2),'-r');
    end
    hold off;
       
   title('aligned mask')
    end
    
end

function distx=get_dist(pts,which_circles)
    if strcmp(which_circles,'both') %use top and bottom
        distx=mean([pdist(pts([1 2],:)), pdist(pts([3 4],:))]);
    end
   if strcmp(which_circles,'top') %use top only
        distx=pdist(pts([1 2],:));
   end
    if strcmp(which_circles,'bottom') %use bottom only
        distx=pdist(pts([3 4],:));
    end

end