function [pts]=find_circles(img1,r,debug)
%figure(1)
%imagesc(img1)
H=size(img1,1);
W=size(img1,2);

img_ul=imcrop(img1,[1,1,round(W/4)-1,round(H/2)-1]);%upper left
[row_ul,col_ul]=find_circ_top(img_ul,r);

img_ur=imcrop(img1,[round(3*W/4)+1,1,W-round(3*W/4)-1,round(H/2)-1]);%upper right
[row_ur,col_ur]=find_circ_top(img_ur,r);
col_ur=col_ur+round(3*W/4);

img_ll=imcrop(img1,[1,round(H/2)+1,round(W/4)-1,H-round(H/2)-1]);%lower left
[row_ll,col_ll]=find_circ_bottom(img_ll,r);
row_ll=row_ll+round(H/2);

img_lr=imcrop(img1,[round(3*W/4)+1,round(H/2)+1,W-round(3*W/4)-1,H-round(H/2)-1]);%lower right
[row_lr,col_lr]=find_circ_bottom(img_lr,r);
row_lr=row_lr+round(H/2);
col_lr=col_lr+round(3*W/4);

pts=[col_ul,row_ul;col_ur,row_ur;col_ll,row_ll;col_lr,row_lr];


if debug 
    figure
    x=-r:1:r;
    y=sqrt(r^2-x.^2);
    poly=[x',y';flipud(x'),flipud(-y')];

    subplot(2,2,1)
    imagesc(img1);
    %colormap gray
    hold on;
    plot(poly(:,1)+col_ul,poly(:,2)+row_ul,'-r')
    plot(col_ul*ones(91),(row_ul-55):(row_ul+35),'-r')
    hold off;
    xlim([col_ul-35,col_ul+35]);
    ylim([row_ul-55,row_ul+35]);

    subplot(2,2,2)
    imagesc(img1);
    %colormap gray
    hold on;
    plot(poly(:,1)+col_ur,poly(:,2)+row_ur,'-r')
    plot(col_ur*ones(91),(row_ur-55):(row_ur+35),'-r')
    hold off;
    xlim([col_ur-35,col_ur+35]);
    ylim([row_ur-55,row_ur+35]);

    subplot(2,2,3)
    imagesc(img1);
    %colormap gray
    hold on;
    plot(poly(:,1)+col_ll,poly(:,2)+row_ll,'-r')
    plot(col_ll*ones(91),(row_ll-35):(row_ll+55),'-r')
    hold off;
    xlim([col_ll-35,col_ll+35]);
    ylim([row_ll-35,row_ll+55]);

    subplot(2,2,4)
    imagesc(img1);
    %colormap gray
    hold on;
    plot(poly(:,1)+col_lr,poly(:,2)+row_lr,'-r')
    plot(col_lr*ones(91),(row_lr-35):(row_lr+55),'-r')
    hold off;
    xlim([col_lr-35,col_lr+35]);
    ylim([row_lr-35,row_lr+55]);
end




end


function [c] = getCircle(d)
r=(d-1)/2;%r=21 corresponds to d=43
c=zeros(2*r+1,2*r+1);
for i=1:size(c,1)
    for j=1:size(c,2)
        if (i-r-1)^2+(j-r-1)^2<=r^2
            c(i,j)=1;
        end
    end
end

end




function [row,col]=find_circ_top(img,r)
pad=5;

circ=getCircle(2*r+1);

% c=normxcorr2(circ,img);
% [~,I]=max(c(:));
% [i,j]=ind2sub(size(c),I);

circ((2*r+1)+[1:50],:)=0;%dark strip on bottom

r0=r;
half_circ=circ(r0:end,:);%half a circle with dark strip, works better than a full circle but can be slightly off

%img_altered = img;
%img_altered(img_altered>quantile(img_altered(:),0.5)) = quantile(img_altered(:),0.5);%fixes issue with cricle finding
bb=align_images(img,half_circ,1);%align_images_dft does not work here likely due to the small size of the template compared to the image

%small image around first result
img2=img((bb(1)-pad-r0+1):(bb(2)+pad),(bb(3)-pad):(bb(4)+pad));

%bb2=align_images_dft(img2,circ,10);%supixel alignment, sometimes generates
%errors. Could be fixed by somehow forcing it not to have overlap.

%bb3=align_images(img2,circ,1);

%instead rescale the image and align full circle
f=4;%resize factor
circ2=getCircle(f*(2*r+1));
img2rs=imresize(img2,f);
bb2=align_images(img2rs,circ2,1);
bb2=(bb2-1)/f+1;

%  x=-r:1:r;
% y=sqrt(r^2-x.^2);
% poly=[x',y';flipud(x'),flipud(-y')];

% figure(2)
% subplot(2,1,1)
%     imagesc(img);
%     %colormap gray
%     hold on;
%     plot(poly(:,1)+bb(1,2)-1+r,poly(:,2)+bb(1,1)-1+r-r0+1,'-r')
%     hold off;
%     
%    subplot(2,1,2)
%     imagesc(img);
%     %colormap gray
%     hold on;
%     plot(poly(:,1)+bb2(1,2)-pad+bb(1,2)-1+r,poly(:,2)-(r0-1)+bb2(1,1)-pad+bb(1,1)-1+r,'-r')
%     hold off;



row=-(r0-1)+bb2(1,1)-pad+bb(1,1)-1+r;%position of centre of circle
col=bb2(1,2)-pad+bb(1,2)-1+r;

%row=bb(1,1)+r;%position of centre of circle
%col=bb(1,2)+r;
end

function [row,col]=find_circ_bottom(img,r)

pad=5;

circ=getCircle(2*r+1);

% c=normxcorr2(circ,img);
% [~,I]=max(c(:));
% [i,j]=ind2sub(size(c),I);

circ=[zeros(50,2*r+1);circ];%dark strip on top

r0=r;
half_circ=circ(1:end-r0+1,:);
%img_altered = img;
%img_altered(img_altered>40000) =40000;%fixes issue with cricle finding
%img=img_altered;

bb=align_images(img,half_circ,1);%align_images_dft does not work here likely due to the small size of the template compared to the image

img2=img((bb(1)-pad):(bb(2)+pad+r0-1),(bb(3)-pad):(bb(4)+pad));

%bb2=align_images_dft(img2,circ,10);%supixel alignment
%bb2=align_images(img2,circ,1);

%instead rescale the image and align full circle
f=4;%resize factor
circ2=getCircle(f*(2*r+1));
%circ2=[zeros(50*f,2*f*r+1);circ2];%dark strip on top
img2rs=imresize(img2,f);
bb2=align_images(img2rs,circ2,1);
bb2=(bb2-1)/f+1;

row=1*(bb2(1,1)-pad)+bb(1,1)-1+r;%position of centre of circle
col=1*(bb2(1,2)-pad)+bb(1,2)-1+r;

%row=bb(1,1)+r;%position of centre of circle
%col=bb(1,2)+r;
end

