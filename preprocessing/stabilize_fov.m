%stablize fov using the lineplrofiles of the middle frame
function [fov] = stabilize_fov(fov,debug)
    %get some infromation about the fov
    n_frames = fov.n_frames;
    middle = round(n_frames/2);
    phase = fov.phase;
    
    lps_x = squeeze(mean(phase,1));%all line profiles
    lps_y = squeeze(mean(phase,2));
    lpm_y = lps_x(:,middle);%line profile middle
    lpm_x = lps_y(:,middle);
    
    offsets = zeros(n_frames,2);
    parfor frame_it = 1:n_frames
        lp_y = lps_x(:,frame_it);
        [c,lags] = xcorr(lp_y,lpm_y);
        [~,I] = max(c);
        o_x = lags(I);%offest y
        
        lp_x = lps_y(:,frame_it);
        [c,lags] = xcorr(lp_x,lpm_x);
        [~,I] = max(c);
        o_y = lags(I);%offset x
        offsets(frame_it,:) = [o_y,o_x]

    end
    
    for frame_it = 1:n_frames        
        for field_name = fov.image_data_names %stablize all different channels
            fov.(field_name)(:,:,frame_it)=imtranslate(fov.(field_name)(:,:,frame_it),-offsets(frame_it,[2 1]),'linear','FillValues',0);
        end
    end
    
    if debug
        figure
        subplot(2,1,1)
        imagesc(max(phase,[],3))
        title('before')
        subplot(2,1,2)
        imagesc(max(fov.phase,[],3))
        title('after')
        sgtitle('stabilizing')
        pause(1)
    end
end
%{
function [fov] = stabilize_fov(fov)
    [fov] = stabilize_stack_with_a_given_order(fov,1:fov.n_frames);
    

function [fov] = stabilize_stack_with_a_given_order(fov,order)
    n_frames = fov.n_frames;
    height = fov.height;
    width = fov.width;
    
    image_data_names = fov.image_data_names;
    
    lps_y = squeeze(mean(fov.phase,1)); %line profiles x
    lps_x = squeeze(mean(fov.phase,2)); %line profiles y
    
    phase_cumulative = double(fov.phase(:,:,order(1)));
    for index = 2:n_frames
        frame_index = order(index);
        lpc_y = mean(phase_cumulative,1)/(index-1);
        lpc_x = mean(phase_cumulative,2)'/(index-1);
        lp_x = lps_x(:,frame_index);
        [c,lags] = xcorr(lp_x,lpc_x);
        [~,I] = max(c);
        offset_y = lags(I);

        lp_y = lps_y(:,frame_index);
        [c,lags] = xcorr(lp_y,lpc_y);
        [~,I] = max(c);
        offset_x = lags(I);
        y = -offset_y;
        x = -offset_x;

        a1 = max(1,1-y);
        a2 = min(height,height-y);
        a3 = max(1,1-x);
        a4 = min(width,width-x);

        b1 = max(1,1+y);
        b2 = min(height,height+y);
        b3 = max(1,1+x);
        b4 = min(width,width+x);
        
        for field_name = image_data_names
            fov.(field_name)(b1:b2,b3:b4,frame_index) = fov.(field_name)(a1:a2,a3:a4,frame_index);
        end
        
        phase_cumulative = phase_cumulative + double(fov.phase(:,:,frame_index));
    end
    
    %for field_name = image_data_names
    %    fov.phase = uint16(fov.phase);
    %end
end

end
%}