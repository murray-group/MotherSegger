%%% finds the best match of the template within the image around a given position (top left corner at (row,col)=(bb_y,bb_x)) 
%%% and with a max shift of maxshift. Unfortunately it is still slower than
%%% using normxcorr2 as in align_images


function [bb] = align_images_maxshift(img,template,maxshift,bb_y,bb_x)
    [h, w]=size(template);
    
    c=zeros(2*maxshift+1,2*maxshift+1);
    for i=1:(2*maxshift+1)
        for j=1:(2*maxshift+1)
            c(i,j)=corr2(img([(bb_y):(bb_y+h-1)]+i-maxshift-1,[(bb_x):(bb_x+w-1)]+j-maxshift-1),template);
        end
    end
    [i,j] = find(c==max(c(:)));%find the greatest correaltion
    
    offset_y=bb_y+i-maxshift-1;
    offset_x=bb_x+j-maxshift-1;
    
    bb = [offset_y,offset_x;offset_y+size(template,1)-1,offset_x+size(template,2)-1];%determine the bounding box of the template residing in the image
end
