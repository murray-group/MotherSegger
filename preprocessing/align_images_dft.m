function bb=align_images_dft(img,template,res)

    %resolution is 1/res e.g. res=100 is accuate to 0.01 pixels
    
    t=zeros(size(img));
    rd=round((size(img,1)-size(template,1))/2);
    cd=round((size(img,2)-size(template,2))/2);
    t((1+rd):(size(template,1)+rd), (1+cd):(size(template,2)+cd))=template;
    output = dftregistration(fft2(img),fft2(t),res);
    
    row=output(3)+rd+1;
    col=output(4)+cd+1;
    
    bb=[row, col ; row+size(template,1)-1, col+size(template,2)-1];

end