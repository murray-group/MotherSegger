

function fov = straighten_fov(fov,debug)
    
    mean_phase = mean(fov.phase,3);
    if(debug),max_phase = max(fov.phase,[],3);end%only for debug
    angle1 = angle_first(mean_phase);%identify the tilt in the fov using the bright wide bars on top and bottom of the channels
    mean_phase_rotated = imrotate(mean_phase,angle1,'bilinear');
    [mask,bbs] = create_mask(fov.pixel_to_micrometer_ratio,fov.channel_dimensions,false);%create a mask with the unoptimized ptmr
    channel_empty = get_empty_channel(mean_phase_rotated,mask,bbs);%using the mask get the empty channel
    angle2 = angle_second(channel_empty);%using the empty channel find a more accurate tilt of the fov
    %angle2 = 0;
    %using both angles roatate the fov
    for field_name = fov.image_data_names
        fov.(field_name) = imrotate3(fov.(field_name),angle1+angle2,[0 0 1],'linear','crop','FillValues',0);
    end
    
    if debug
        disp(strcat("rotated by this angle: ",string(angle1+angle2)))
        disp(strcat("angle1: ",string(angle1)))
        disp(strcat("angle2: ",string(angle2)))
        figure
        subplot(2,1,1)
        imagesc(max_phase)
        title('before')
        subplot(2,1,2)
        imagesc(max(fov.phase,[],3))
        title('after')
        sgtitle('straightening')
        pause(1)
    end
end

function channel_empty = get_empty_channel(mpi,mask,bbs)%get the empty channel of the fov
    bb_template = align_images(mpi,mask,true);
    bb_empty = bbs(:,:,end);
    padding_y = -round((bb_empty(2)-bb_empty(1))/3.5);
    padding_x = 10;
    bb = [bb_template(1)+bb_empty(1)-padding_y-1, bb_template(3)+bb_empty(3)-padding_x-1;...
        bb_template(1)+bb_empty(2)+padding_y-1, bb_template(3)+bb_empty(4)+padding_x-1];
    channel_empty = mpi(bb(1):bb(2),bb(3):bb(4));
end

function angle = angle_second(ch)
    %ch = ch'; instead of transpose 
    ch = movsum(ch,5,2);%makes it more consistent
    [~,p] = max(ch,[],2);%p contains a the max line along the empty channel
    x = 1:numel(p);
    mdl = fitlm(x,p);
    c = mdl.Coefficients{2,1};
    angle = -atan(c)/(2*pi)*360;%angle after which p would be a perfect vertical line
end

function angle = angle_first(mean_phase)
    half_max = round(max(mean_phase,[],'all')/2);%half maxi of the mean phase image
    padding = 50;%ignore these many pixel in one x-axis (left and right)
    n_points = 75;%take n_points many points
    x_indices = round(linspace(1+padding,size(mean_phase,2)-padding,n_points));%at these x positions find first and last index which is above half maxi
    %to find the the angle by which the fov is roated one can use the
    %bright bars on top and on bottom of the channels. just find at every
    first_index_above_half_max = nan(1,n_points);
    last_index_above_half_max = nan(1,n_points);
    slices = mean_phase(:,x_indices);
    for it = 1:n_points
        slice = slices(:,it)>half_max;
        tmp = find(slice,1,'first');
        if tmp
            first_index_above_half_max(it) = tmp;
            last_index_above_half_max(it) = find(slice,1,'last');
        end
    end
    %do a linear regression throught the points which where found to figure
    %out by how much the fov is tilted 
    mdl = fitglm(x_indices,first_index_above_half_max);
    c1 = mdl.Coefficients{2,1};
    mdl = fitlm(x_indices,last_index_above_half_max);
    c2 = mdl.Coefficients{2,1};
    c = (c1+c2)/2;
    %take the mean of both tilts and convert it to an angle between 0 and
    %360
    angle = atan(c)/(2*pi)*360;
end