function fov=MSpreprocess(parametersPartI)
paths = get_fov_paths(parametersPartI.base);
disp(strcat("preprocessing: number of FOVs ",string(numel(paths))))
for i = 1:numel(paths)
    try
        tic
        path = paths{i};
        disp(path)
        disp('loading')
        if numel(parametersPartI.segments)
            fov = load_segmented_fov(path,parametersPartI.segments,parametersPartI.starting_frame,parametersPartI.offsets_fluorescent,parametersPartI.debug(1));
        else
            fov = load_fov(path,parametersPartI.starting_frame,parametersPartI.offsets_fluorescent,parametersPartI.debug(1));
        end
        toc
        fov.channel_dimensions.halo = parametersPartI.halo;
        fov.channel_dimensions.height_constriction = parametersPartI.height_constriction;
        fov.pixel_to_micrometer_ratio = parametersPartI.pixel_to_micrometer_ratio;
        
        if parametersPartI.new_wafer==0
            
            disp('stabilizing')
            fov = stabilize_fov(fov,parametersPartI.debug(2));
            toc
            
            disp('straightening')
            fov = straighten_fov(fov,parametersPartI.debug(3));
            toc
            
            disp('perfect pixel to micrometer ratio')
            [fov] = accurate_ptmr(fov,parametersPartI.debug(4));
            toc
            
            [fov.mask.image,fov.mask.bb_channels] = create_mask(fov.pixel_to_micrometer_ratio,fov.channel_dimensions,parametersPartI.debug(5));
            disp('stabilizing precise')            
            fov = stabilize_precise_fov(fov,parametersPartI.debug(6));
            toc
            
            disp('aligning mask');
            fov = align_mask(fov,parametersPartI.debug(7));
            toc

        else
            disp('stabilizing and straightening')
            fov= stabilize_straighten(fov,parametersPartI.which_circles,parametersPartI.debug(2));
            toc
            
            disp('getting mask offset');
            fov = mask_offset(fov,parametersPartI.which_circles,parametersPartI.debug(5),parametersPartI.debug(7));
            toc
        end
        

        
        disp('cropping')
        fov = crop_fov(fov,parametersPartI.crop_padding,parametersPartI.offsets_fluorescent2,parametersPartI.debug(7));
        toc
        
        if parametersPartI.save_preprocessed_fov_as_tif
            disp('saving fov')
            save_cropped_fov(parametersPartI.base,fov)
            toc
        end
        disp('extracting')
        fov = extract_channels(fov,parametersPartI);
        toc
        
        disp('saving')
        path_fov = save_channels(fov,path);
        toc
        
        %create a struct which contains the information of the current
        %fov and save it at the same location as the channels
        fov_data = struct();
        fov_data.sample_time = parametersPartI.sample_time;%time between frames in seconds
        fov_data.channel_dimensions = fov.channel_dimensions;
        fov_data.path = path_fov;
        fov_data.fov_id = 1;%the id that the fov will have in a list of fovs describing its index
        fov_data.name_id = fov.fov_id;%the id given in the name of the fov
        fov_data.pixel_to_micrometer_ratio = parametersPartI.pixel_to_micrometer_ratio;
        fov_data.pixel_to_micrometer_ratio_adjusted = fov.pixel_to_micrometer_ratio;
        fov_data.n_fluors = fov.number_of_fluors;
        fov_data.n_frames = fov.n_frames;
        fov_data.mask = fov.mask;
        fov_data.row = fov.row;
        %fov_data_list(i) = fov_data;
        save(strcat(path_fov,'fov_data.mat'),'fov_data')
        toc
    catch ME
        disp('-----------------------------------Error-------------------------------------------------')
        disp(strcat("failed: ",path))
        disp(getReport(ME,'extended'));
        disp('-----------------------------------------------------------------------------------------')
        %rethrow(ME)
    end
end

%save experiment information
experiment_info.PartI = parametersPartI;
experiment_info.PartI.paths = paths;
save(strcat(parametersPartI.base,'experiment_info.mat'),'experiment_info')
end

