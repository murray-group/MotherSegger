function MSsegment_test(parameters)
    channel_paths = get_channel_paths(parameters.base,false);
    disp('segmentation test')
    CONST = load_const(parameters.supersegger_const);
    CONST.trackLoci.numSpots = [0];
    CUT_INTs = parameters.supersegger_cut_test;
    MAGIC_RADIUSs = parameters.supersegger_radius_test;
    MAGIC_THRESHOLDs = parameters.supersegger_threshold_test;
    %test = parameters.test;
    n_tests = numel(CUT_INTs)*numel(MAGIC_THRESHOLDs)*numel(MAGIC_RADIUSs);%*numel(test);

    channel_id = parameters.channel_id;
    channel_path = channel_paths{channel_id};
    channel = load_channel(channel_path);
    c = parallel.pool.Constant(channel);
    segmentations = cell(n_tests,1);

    tic
    tested = zeros(n_tests,4);
    parfor it = 1:n_tests
        tests_it = it-1;
        ci_index = mod(tests_it,numel(CUT_INTs))+1;
        tests_it = floor(tests_it/numel(CUT_INTs));
        mr_index = mod(tests_it,numel(MAGIC_RADIUSs))+1;
        tests_it = floor(tests_it/numel(MAGIC_RADIUSs));
        mt_index = mod(tests_it,numel(MAGIC_THRESHOLDs))+1;
        tests_it = floor(tests_it/numel(MAGIC_THRESHOLDs));
        %t =  mod(tests_it,numel(test))+1;
        tested(it,:) = [it,CUT_INTs(ci_index),MAGIC_RADIUSs(mr_index),MAGIC_THRESHOLDs(mt_index)];%,test(t)];
        CONST_tmp = CONST;
        CONST_tmp.superSeggerOpti.CUT_INT = CUT_INTs(ci_index);
        CONST_tmp.superSeggerOpti.MAGIC_RADIUS = MAGIC_RADIUSs(mr_index);
        CONST_tmp.superSeggerOpti.MAGIC_THRESHOLD = MAGIC_THRESHOLDs(mt_index);
        frames_data = super_segger(c.Value,CONST_tmp);
        [segmentation,~] = get_supersegger_data(frames_data,channel_path,true);
        segmentations{it} = segmentation;
    end
    toc
    
    %creates a kymograph for each growth channel of their segmentation
    visualize_segmentations_naive(segmentations)

    segmetnation_overview = cat(2,segmentations{:});
    background_overview = repmat(uint16(channel.(parameters.background)),[1,n_tests,1]);
    segmetnation_overview = permute(segmetnation_overview,[3,1,2]);
    background_overview = permute(background_overview,[3,1,2]);
    segmentation_preview(background_overview,segmetnation_overview);

    tested_table = array2table(tested);
    tested_table.Properties.VariableNames = {'Position' 'cut' 'radius' 'threshold'};
    disp(tested_table)
end