function [frames_data] = intProcessXY( dirname_xy, channel, skip, CONST)
% intProcessXY : the details of running the code in parallel.
% Essentially for parallel processing to work, you have to hand each
% processor all the information it needs to process the images.

% get header to show xy position
%namePhase = [dirname_xy,'phase.tif'];
%tiff_info = imfinfo(namePhase);
%num_t = size(tiff_info, 1);


% does the segmentations for all the frames in parallel
%test = []

%out = [];
%parfor(i=1:num_t,workers) % through all frames
shape = size(channel.phase_subtracted);
n_frames = shape(3);
for i=fliplr(1:n_frames) % through all frames
    crop_box_tmp = [];
    phase = channel.phase_subtracted(:,:,i);
    tmp = doSeg(i, phase, skip, CONST, crop_box_tmp);
    frames_data(i) = tmp;
end

end
