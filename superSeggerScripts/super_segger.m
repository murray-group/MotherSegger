function [frames_data] = super_segger(channel,CONST)

    %model = '100XPa';
    %CONST = loadConstants(model,0);
    %CONST.seg.OPTI_FLAG = 0;
    %CONST.parallel.PARALLEL_FLAG = false;
    %CONST.trackOpti.MIN_AREA_NO_NEIGH = 50;
    %CONST.trackLoci.numSpots = [5,5,5,5,5,5]; % Max number of foci to fit in each fluorescence channel (default = [0 0]) % fit up to 5 foci in each cell
    %CONST.parallel.verbose = 0;% not verbose state
    %close all; % close all figures

    frames_data = intProcessXY( '~/', channel, 1, CONST);
     frames_data = rmfield(frames_data,{'phaseNorm','C2phaseThresh','mask_colonies'});
     frames_data = trackOptiStripSmall(frames_data, CONST);
     frames_data = trackOptiLinkCellMulti(frames_data, 1, CONST);
     frames_data = rmfield(frames_data,{'segs'});
     [frames_data,~,~] = trackOptiCellMarker(frames_data, CONST);
     frames_data = trackOptiFluor(frames_data, CONST);
    %frames_data = rmfield(frames_data,{'mask_cell'});
     frames_data = trackOptiMakeCell(frames_data, CONST);
     %frames_data = trackOptiFindFoci(frames_data, CONST);
    %clist = trackOptiClist(frames_data, CONST);
end