function MSsegment(parametersPartII)
    channel_paths = get_channel_paths(parametersPartII.base,false);
    [channel_paths] = filter_paths(channel_paths,parametersPartII.ignore_these_channels);
    disp(strcat("segmentation: number of channels ",string(numel(channel_paths))))
    CONST = load_const(parametersPartII.supersegger_const);
    CONST.trackLoci.numSpots = [];%parametersPartII.supersegger_number_of_foci;
    CONST.superSeggerOpti.CUT_INT = parametersPartII.supersegger_cut;
    CONST.superSeggerOpti.MAGIC_RADIUS = parametersPartII.supersegger_radius;
    CONST.superSeggerOpti.MAGIC_THRESHOLD = parametersPartII.supersegger_threshold;
    n_fragments = parametersPartII.n_fragments;
    n_channels = parametersPartII.n_channels;
    n_paths = numel(channel_paths);

    tic
    for batch_it = 1:n_channels:n_paths%do the analysis in batches so it runs on the cluster
        
        n_current = n_channels-max([batch_it+n_channels-1-n_paths,0]);
        channel_fragments = cell(n_fragments*n_current,1);
        channel_fragments_count = 1;
        res = cell(n_fragments*n_current,1);
        
        if(parametersPartII.reset_pool),reset_parpool();end %reset pool if specified
        if(batch_it==1||parametersPartII.reset_pool),attachFiles();end %add files/methods for supersegger
        disp(strcat("Batch: ",string(batch_it),"-",string(batch_it+n_current-1)," channel"))
        for channel_it = batch_it:batch_it+n_current-1%load data
            channel_path = channel_paths{channel_it};
            channel = load_channel(channel_path);

            fragments_range = round(linspace(0,channel.n_frames,n_fragments+1));
            for frag_it = 1:n_fragments
                range = fragments_range(frag_it)+1:fragments_range(frag_it+1);
                channel_fragment = struct();
                for i = channel.image_data_names_processed
                    channel_fragment.(i) = channel.(i)(:,:,range);
                end
                channel_fragment.number_of_fluors = channel.number_of_fluors;
                channel_fragment.image_data_names_processed = channel.image_data_names_processed;
                channel_fragments{channel_fragments_count} = channel_fragment;
                channel_fragments_count = channel_fragments_count + 1;
            end
            clear channel
        end
        
        parfor frag_it = 1:n_current*n_fragments%run supersegger
            channel_fragment = channel_fragments{frag_it};
            try
                fragment_data = super_segger(channel_fragment,CONST);
                res{frag_it} = fragment_data;
            catch ME
                disp(strcat("fragment ",string(frag_it)," failed"))
                disp(ME.message)
            end
        end
        
        path_index = batch_it:batch_it+n_current-1;
        for channel_it = 1:n_current%save data
            channel_path = channel_paths{path_index(channel_it)};
            fragments = res((channel_it-1)*n_fragments+1:channel_it*n_fragments);
            if any(cellfun(@ numel,fragments)==0)
                disp(strcat("channel ",string(channel_it)," failed"))
                disp(channel_path)
            else
                frames_data = cat(2,fragments{:});
                [segmentation,foci_data] = get_supersegger_data(frames_data,channel_path,false);
                %save_foci_data(foci_data);
                writeTiff(strcat(channel_path,'segmentation.tif'),segmentation);
            end
        end
    end
    
    %load (if exist) experiment data and update it
    exp_info_path = strcat(parametersPartII.base,'experiment_info.mat');
    if isfile(exp_info_path)
        experiment_info = load(exp_info_path);
        experiment_info = experiment_info.experiment_info;
    else
        experiment_info = struct();
    end
    experiment_info.PartII = parametersPartII;
    save(strcat(parametersPartII.base,'experiment_info.mat'),'experiment_info')
    
    toc
    disp('Done! (Part II)')
end

function [paths] = filter_paths(paths,ignore_these_channels)
    if ismac
        slash = '\';%Mac
    elseif isunix
        slash = '/';%linux
    elseif ispc
        slash = '\';%windows
    else
        slash = '/';%other
    end

    filter = true(numel(paths),1);
    if numel(ignore_these_channels) > 0
        for path_it = 1:numel(paths)
            path = paths(path_it);
            path_shredded = strsplit(path,slash);
            fov_info = strsplit(path_shredded(end-2),'_');
            channel_info = strsplit(path_shredded(end-1),'_');
            channel_id = str2num(channel_info(2));
            fov_id = str2num(fov_info(2));
            filter(path_it) = ~any(all(ignore_these_channels == [fov_id,channel_id],2));
        end
    end
    paths = paths(filter);
end

function save_foci_data(foci_data)
    foci_list = foci_data.foci_list;
    save(foci_data.path,'foci_list');
end

function reset_parpool()
    p = gcp('nocreate');
    if ~isempty(p)    
        delete(p)
    end
    myCluster = parcluster;
    p = parpool(myCluster);
    addAttachedFiles(p,{'ssoSegFunPerReg.m','scoreNeuralNet.m','segInfoCurv.m',...
        'cellprops3.m','fast_rotate_loose_double.mexw64','fast_rotate_loose_double.mexw32',...
        'fast_rotate_loose_double.cpp','fast_rotate_loose_double.mexa64','fast_rotate_loose_double.mexglx',...
        'fast_rotate_loose_double.mexmaci','fast_rotate_loose_double.mexmaci64'});

end

function attachFiles()
    p = gcp();
    %p = parpool(myCluster);
    addAttachedFiles(p,{'ssoSegFunPerReg.m','scoreNeuralNet.m','segInfoCurv.m',...
        'cellprops3.m','fast_rotate_loose_double.mexw64','fast_rotate_loose_double.mexw32',...
        'fast_rotate_loose_double.cpp','fast_rotate_loose_double.mexa64','fast_rotate_loose_double.mexglx',...
        'fast_rotate_loose_double.mexmaci','fast_rotate_loose_double.mexmaci64'});
end
