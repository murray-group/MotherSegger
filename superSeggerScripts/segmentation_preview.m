
function segmentation_preview(phase_overview,segmetnation_overview)
    slidervalue(phase_overview,segmetnation_overview)
end


function slidervalue(background_overview,segmentation_overview)
    % Create figure window and components
    background_overview = permute(repmat(background_overview,[1,1,1,3]),[2,3,4,1]);
    segmentation_overview = permute(repmat(segmentation_overview,[1,1,1,3]),[2,3,4,1])>0;
    phase_overview_tmp = background_overview;
    phase_overview_tmp(segmentation_overview) = uint16(phase_overview_tmp(segmentation_overview)*2/3+power(2,16)*1/3);
    merged = phase_overview_tmp;
    segmentation_overview(:,:,1,:) = 0;
    merged(segmentation_overview) = 0;
    
    padding = 100;
    slider_height = 100;
    slider_width = 150;
    height = size(background_overview,1)+slider_height+padding*2;
    width = max([size(background_overview,2),slider_width])+padding*2;
    %width = 100;

    sld = 'filler';
    ucb = 'filler';
    im = 'filler';
    fig = uifigure('Position',[0 0 width height],'Name','Test');
    ucb = uicheckbox(fig,'Text','Overlay Segmentation',...
        'Position',[100 125 165, 15],...
        'ValueChangedFcn',@(cbx,event) updateGauge(fig,sld,ucb,im,background_overview,background_overview,true));

    x = round(width/2-size(background_overview,2)/2);
    im = uiimage(fig,'Position',[x padding+slider_height size(background_overview,2) size(background_overview,1)]);

    sld = uislider(fig,...
        'Position',[padding padding width-padding*2 3],...
        'ValueChangedFcn',@(sld,event) updateGauge(fig,sld,ucb,im,merged,background_overview,true),...
        'Limits', [1,size(background_overview,4)]);
    %im.ImageSource = merged(:,:,:,sld.Value);

    ucb.ValueChangedFcn = @(cbx,event) updateGauge(fig,sld,ucb,im,merged,background_overview,true);
    ucb.Value = 1;
    updateGauge(fig,sld,ucb,im,merged,background_overview,true)
end

function UIFigureKeyPress(app, event)
	value = app.Slider.Value; % get the slider value
	key = event.Key; % get the pressed key value
    if strcmp(key,'leftarrow')
        value = value-1; % left value
    elseif strcmp(key,'rightarrow')
            value = value+1; % right value
    end
    app.Slider.Value = value; % set the slider value
	SliderValueChanging(app, event); % execute the slider callback
end

% Create ValueChangedFcn callback
function updateGauge(fig,sld,ucb,im,phase_overview,segmentation_overview,test)
    %cg.Value = sld.Value;
    if test
        sld.Value = round(sld.Value);
        updateGauge(fig,sld,ucb,im,phase_overview,segmentation_overview,false);
    else
        fig.Name = string(sld.Value);
        if ucb.Value
            im.ImageSource = phase_overview(:,:,:,sld.Value);
        else
            im.ImageSource = segmentation_overview(:,:,:,sld.Value);
        end
    end
end

%%
