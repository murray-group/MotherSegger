function  [data] = doSeg(i, phase,  skip, CONST, crop_box)
% doSeg : Segments and saves data in the seg.mat files in the seg/ directory.
% If the seg files are already found it does not repeat the segmentation.
% It calls the segmentation function found in CONST.seg.segFun to achieve
% this. The images are not ideally segmented at this stage because they do
% not use temporal information, i.e. what came before or after, to optimize 
% the segment choices; this comes at the linking stage.
% After it segments the data it copies the fluor fields into the data 
% structure and saves this structure in the seg directory.
%
% INPUT :
%         i : frame number
%         nameInfo :
%         nc : array of channel numbers
%         nz : array of z values
%         nt : array of time frames 
%         num_z : number of z's
%         num_c : number of channels
%         dirname_xy : xy directory
%         clean_flag : redo segmentation if set to true
%         skip : how many frames to skip
%         CONST : segmentation constants
%         header : information string
%         crop_box : alignment information
%
% Copyright (C) 2016 Wiggins Lab 
% Written by Paul Wiggins & Stella Stylianidou.
% University of Washington, 2016
% This file is part of SuperSegger.
%
% Modified by Robin Koehler (Date:Jul 26, 2021 Hash:54fc3e7a)
% 
% SuperSegger is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% SuperSegger is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with SuperSegger.  If not, see <http://www.gnu.org/licenses/>.


% Init
data = [];
%namePhase = [dirname_xy,'phase.tif'];
%phase = imread(namePhase, i);


if ~mod(i-1,skip)


    % do the segmentation here
    [data, ~] = CONST.seg.segFun( phase, CONST, [], 'filler, rob', crop_box);
    if ~isempty( crop_box )
        data.crop_box = crop_box;
    end

    clear phase
end
end

