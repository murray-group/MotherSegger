%load combined file with line profiles and fov_list (if not included in combined_data.mat)
%or load 
base = "/mnt/thebigone/Data/Microfluid/20230718_LB/";
channel_paths = get_channel_paths(base,true);
[channel_list,cycle_list,fov_list] = load_mothersegger_data(channel_paths,true);
%% Amount of cell cycles per fov
tmp = [cycle_list.channel_id];
fov_id = [channel_list.fov_id];
tmp = fov_id(tmp);
count = histcounts(tmp,0.5:max(tmp)+0.5)
%% Add line profiles y
cycle_list = add_lineprofiles(channel_list,cycle_list,1,[1 1 1]);
%% Find offsets y
fluor_index=3;
p=1;%plot
offsets=find_offset(cycle_list,channel_list,fov_list,1,fluor_index,p);% y direction
%% Add line profiles x
cycle_list = add_lineprofiles(channel_list,cycle_list,2,[1 1 1]);
%% Find offsets x
fluor_index=3;
p=1;%plot
offsets=find_offset(cycle_list,channel_list,fov_list,2,fluor_index,p);%x direction
%%
%caution: changes images, Part I needs to be re-ran to undo (or use -offsets)
%correct fov_list.path if Part I was previously ran from a different
%location
r1 = 0.4;
r2 = 0.4;
offsets = [r1,r1,r1,r2,r2,r2];
fluor_index = 3;
dim = 2;
apply_offsets_to_channel_images(fov_list,fluor_index,offsets,dim);

%% change fov_list path
for i = 1:numel(fov_list)
    fov = fov_list(i);
    p = fov.path;
    p = strrep(p,'/media/koehlerr/970EVOPlus/','/mnt/thebigone/');
    fov.path = p;
    fov_list(i) = fov; 
end

%% add row field to fov list if missing
%rows = [2,1,2,2,1,1,2,1]*0;%no rows
%rows = [2,1,2,2,1,1,2,1];%normal
%rows = [1,2,1,1,2,2,1,2];%reverse
rows = [1,1,1,2,2,2];
for i = 1:6
    fov = fov_list(i);
    fov.row = rows(i);
    fov_list_new(i)=fov;
end
fov_list = fov_list_new;
clear fov_list_new