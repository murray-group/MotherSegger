%displays four histograms of the given cell_cycle_ids (duration, mean area
%starting length, dividing length)
function overview(cell_cycle_ids,cycle_list)
    %overview2(cell_cycle_ids,cycle_list)
    %return
    durations = zeros(1,numel(cell_cycle_ids));
    mean_areas = zeros(1,numel(cell_cycle_ids));
    min_area = zeros(1,numel(cell_cycle_ids));
    max_area = zeros(1,numel(cell_cycle_ids));
    birth_lengths = zeros(1,numel(cell_cycle_ids));
    division_lengths = zeros(1,numel(cell_cycle_ids));
    for i = 1:numel(cell_cycle_ids)
        cycle_id = cell_cycle_ids(i);
        cycle = cycle_list(cycle_id);
        durations(i) = cycle.duration;
        mean_areas(i) = mean(cycle.areas);
        min_area(i) = min(cycle.areas);
        max_area(i) = max(cycle.areas);
        birth_lengths(i) = cycle.lengths(1);
        division_lengths(i) = cycle.lengths(end);
    end
    dim = [3,2];
    color = [0,0,1];
    subplot(dim(1),dim(2),1)
    s=durations;
    edges=(min(s)-0.5):5:(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('duration')
    xlabel('frames')
    
    subplot(dim(1),dim(2),2)
    s=mean_areas;
    edges=(min(s)-0.5):20:(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('mean area')
    xlabel('pixel')
    
    subplot(dim(1),dim(2),3)
    s=min_area;
    edges=(min(s)-0.5):10:(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('min area')
    xlabel('pixel')
    
    subplot(dim(1),dim(2),4)
    s=max_area;
    edges=(min(s)-0.5):20:(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('max area')
    xlabel('pixel')
    
    subplot(dim(1),dim(2),5)
    s=birth_lengths;
    edges=(min(s)-0.5):(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('starting length')
    xlabel('pixel')
    
    subplot(dim(1),dim(2),6)
    s=division_lengths;
    edges=(min(s)-0.5):2:(max(s)+0.5);
    %hist_transparent(s,edges,color,0.5,false)
    histogram(s,edges)
    title('dividing length')
    xlabel('pixel')
end

%for SI figure
function overview2(cell_cycle_ids,cycle_list)
    durations = zeros(1,numel(cell_cycle_ids));
    mean_areas = zeros(1,numel(cell_cycle_ids));
    min_area = zeros(1,numel(cell_cycle_ids));
    max_area = zeros(1,numel(cell_cycle_ids));
    birth_lengths = zeros(1,numel(cell_cycle_ids));
    division_lengths = zeros(1,numel(cell_cycle_ids));
    for i = 1:numel(cell_cycle_ids)
        cycle_id = cell_cycle_ids(i);
        cycle = cycle_list(cycle_id);
        durations(i) = cycle.duration;
        mean_areas(i) = mean(cycle.areas);
        min_area(i) = min(cycle.areas);
        max_area(i) = max(cycle.areas);
        birth_lengths(i) = cycle.lengths(1);
        division_lengths(i) = cycle.lengths(end);
    end
    dim = [1,3];
    color = [0,0,1];
    color2 = [1,0,0];
    subplot(dim(1),dim(2),1)
    s=durations;
    edges=(50-0.5):5:(300+0.5);
    %hist_transparent(s,edges,color,0.2,false)
    h = histogram(s,edges);
    h.FaceColor = color;
    ylabel('absolute abundance')
    xlabel('cell cycle duration (min)')
    
    ptmr = 0.0668;
    subplot(dim(1),dim(2),3)
    s=birth_lengths*ptmr;
    edges=(20-0.5):2:(100+0.5);
    edges = edges * ptmr;
    h(1) = histogram(s,edges);
    h(1).FaceColor = color;
    %hist_transparent(s,edges,[1,0,0],0.2,false)
    hold on
    s=division_lengths*ptmr;
    h(2) = histogram(s,edges);
    h(2).FaceColor = color2;
    ylabel('absolute abundance')
    xlabel('length (\mum)')
    %hist_transparent(s,edges,color,0.2,false)
    hold off
    legend('birth length','divison length')
end
