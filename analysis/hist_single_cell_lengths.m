function lengths=hist_single_cell_lengths(cycle_list,cycle_ids)

    count = 0;
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        count = count + cycle.duration;
    end
    lengths = zeros(count,1);
    pointer = 1;
    for i = 1:numel(cycle_ids)
        cycle = cycle_list(cycle_ids(i));
        lengths(pointer:pointer+cycle.duration-1) = cycle.lengths;
        pointer = pointer + cycle.duration;
    end
    ptmr = 0.0688;
    edges = (min(lengths)-0.5):(max(lengths)+0.5);
    edges = edges*ptmr;
    lengths = lengths * ptmr;
    histogram(lengths,edges);
    title("Single Cell Lengths")
    xlabel("cell length (\mum)")
    ylabel("absolute count")
end