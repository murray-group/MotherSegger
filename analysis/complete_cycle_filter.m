%extacts the ids of complete cell cycles from cycle_list
function [filtered_cycle_ids] = complete_cycle_filter(complete_cycle_ids,cycle_list,cut_off)%sorts all complete cell cycles and cuts off cut_off amount on both sides 

    elements = numel(complete_cycle_ids);
    sort_this = zeros(elements,5);
    cut_off = ceil(elements*cut_off);
    cycle_counter = 1;
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        cycle_id = cycle.cycle_id;
        if ismember(cycle.cycle_id, complete_cycle_ids)
            sort_this(cycle_counter,1) = cycle_id;
            sort_this(cycle_counter,2) = numel(cycle.times); %duration
            sort_this(cycle_counter,3) = mean(cycle.areas); %mean area
            sort_this(cycle_counter,4) = cycle.lengths(1); %starting length
            sort_this(cycle_counter,5) = cycle.lengths(end); %ending length
            cycle_counter = cycle_counter + 1;
        end
    end
    filtered_cycle_ids = sort_this(:,1);
    for i = 2:size(sort_this,2)
        sort_this = sortrows(sort_this,i);
        filtered_cycle_ids = intersect(filtered_cycle_ids,sort_this(cut_off:end-cut_off,1));
    end
end