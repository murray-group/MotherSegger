function growth_rates=hist_growth_rates(cycle_list,cycle_ids)

    growth_rates = zeros(numel(cycle_ids),1);
    for i = 1:numel(cycle_ids)
        if ~mod(i,1000)
            disp(strcat("First ",num2str(i)," are done"))
        end
        cycle_id = cycle_ids(i);
        cycle = cycle_list(cycle_id);
        if cycle.times(1) > -1
            areas = log(cycle.areas);
            mdl = fitlm(1:numel(areas),areas);
            growth_rate = mdl.Coefficients{2,1};
            growth_rates(i) = growth_rate;
        end
    end
    histogram(growth_rates,0:0.0002:0.02)
    title("Growth Rates")
    xlabel("growth rate (per sample time)")
    ylabel("absolute count")
end