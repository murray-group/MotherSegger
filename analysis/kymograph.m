function [canvas] = kymograph(cycle_list,complete_cycle_ids,n_buckets_y,n_buckets_x,fluor_index,flip_pole)
    slices = cell(n_buckets_x,1); %contains the lineprofiles
    counter = zeros(n_buckets_x,1);
    
    fn = strcat("fluor",string(fluor_index),"_lp");
    
    growth_rate = 1.01;
    d_p = round(power(growth_rate,linspace(0,log(2)/log(growth_rate),n_buckets_x))*n_buckets_y/2); %exponential
    %d_p = round(linspace(0.5,1,n_buckets_x)*n_buckets_y); %data points for each slice (linerar)
    %d_p = round(linspace(1,1,n_buckets_x)*n_buckets_y); %data points for each slice (rectangle)
    for j= 1:n_buckets_x
        slices{j} = zeros(d_p(j),1);
    end

    for i = 1:numel(complete_cycle_ids)
        cycle = cycle_list(complete_cycle_ids(i));
        t_n = ((1:cycle.duration)-1)/(cycle.duration-1);
        bx = round(t_n*(n_buckets_x-1)+1);
        for j = 1:cycle.duration
            x = bx(j);
            if ~isnan(x)
                slice = slices{x};
                lp = cycle.(fn){j};
                lp(isnan(lp))=0;
                if flip_pole
                    if cycle.pole == -1
                        lp = flipud(lp);
                    end
                end
                lp = lp(round(linspace(1,numel(lp),numel(slice))));
                slice = slice + lp;
                slices{x} = slice;
                counter(x) = counter(x) + 1;
            end
        end
    end

    canvas = nan(n_buckets_y,n_buckets_x);
    for j = 1:n_buckets_x
        slice = slices{j};
        a = numel(slice);
        offset = round((n_buckets_y-a)/2);
        %offset
        canvas(1+offset:a+offset,j) = slice/counter(j);
    end

    
    imAlpha=ones(size(canvas));
    imAlpha(isnan(canvas))=0;
    imagesc(canvas,'AlphaData',imAlpha);
    set(gca,'color',[1 1 1]);
    
    x = linspace(0,1,5);
    xticks = x*(n_buckets_x-1)+1;
    xlabel("relative cell age")
    set(gca,'XTick',xticks, 'XTickLabel', x);
    
    y = linspace(0,2,5);
    yticks = x*(n_buckets_y-1)+1;
    set(gca,'YTick',yticks, 'YTickLabel', -1*y+1);
    ylabel("relative cell length")
    %imagesc(canvas)
    colorbar
    
end