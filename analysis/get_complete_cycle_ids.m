function [complete_cycle_ids] = get_complete_cycle_ids(cycle_list)
    complete_count = 0;
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        if cycle.flags.complete
            complete_count = complete_count + 1;
        end
    end
    complete_cycle_ids = zeros(1,complete_count);
    complete_count = 1;
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        if cycle.flags.complete
            complete_cycle_ids(complete_count) = cycle.cycle_id;
            complete_count = complete_count + 1;
        end
    end
end