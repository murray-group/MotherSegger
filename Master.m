%=====================================Mother Segger=====================================
%This is the master script which is used for the analysis of Field of Views (fov) 
%The analysis is broken down into three Parts:
% + PartI  : preprocessing (aligining, rotating, cropping, subtraction)
% + PartII : segmentation
% + PartIII: tracking
% + PartIV (optional) : foci detection and simple foci tracking
% + PartV  : line profiles and combining files

%Multiple fovs can be analyzed simultaneously, simply by placing them in
%the same directory (base). The fov must be flipped to follow the following
%rules:
% + The open end of channels in which cells grow must be located topside.
% + The inverted/empty channel must be located on the right-hand side.

%Furthermore, the fov files need to follower a specific naming convention 
%"experimentName_fovIndex_width_height_pitch"
% + fovIndex: index of the fov
% + [optional] row: 1 or 2 corresponding to the row in the device, useful
% for fluorescent shift correction
% + width: width of one channel. If the channel is 0.7 micro meter wide 'width' 
%   should be '07'.
% + height: heighy of one channel. If the channel is 25 micro meter long
%   'height' should be '25'.
% + pitch: the distance between two channels. If the distance between two
%   channels is 2 micro meter, 'pitch' should be '2'.
%% Part I : preprocessing (aligining, rotating, cropping, subtraction)
parametersPartI.base = "Path/To/Folder/";
parametersPartI.new_wafer=1;% when using the marker of the new wafer put this parameter to 1 otherwise 0 to analyze the data without any markers
parametersPartI.which_circles='both';%which circles to use for alignment, 'both','top' or 'bottom'. If both is used the alignment will correct for shear i.e. if the block is parallelogram
parametersPartI.segments = [];% if the fov is splitted in mutiple days insert all folder which contain one  segment else leave empty
parametersPartI.pixel_to_micrometer_ratio = 0;% pixel to micrometer ratio (pixel edge length in micrometer). When using the new wafer it can be set to 0 to automatically find the pixel to micrometer ratio otherwise set it 0.06667 
parametersPartI.sample_time = 60; % time between frames (seconds)
parametersPartI.halo = 3; %halo width in micro meter (the white horizontal lines at the beginning and the end of the channels)
parametersPartI.height_constriction = 5; %height of constriction in micro meter
parametersPartI.starting_frame = 0; % start the analysis on this frame
parametersPartI.crop_padding = 5; % when cropping out the roi in the fov add this many pixels to all sides (Top,Left,Bottom,Right)
parametersPartI.side_padding = 5; % pads the sides (Left,Right) of each individual channel with that many pixels
parametersPartI.reconstruct_empty = false; % reconstructs the empty channel to get rid of artifacts inside of the empty channel
parametersPartI.adjust_empty = 1; % adjust intensity in the empty channel by this factor
parametersPartI.remove_artifacts = -6; % remove artifacts in the sepcified radius around cells, if < 0 do nothing
parametersPartI.remove_artifacts_threshold = 50000; % everything below this is considered to be a cell
parametersPartI.average_over = -10; %sets min to 0 and max to 2^16 (only works if number is greater 0)
parametersPartI.remove_constriction = 2; % remove constriction if >= 0, positive interger add padding to the removed constriction
parametersPartI.offsets_fluorescent = [0,0;0,0]; %offsets fluorescent channels before rotation (eg. [-2.1,0.6;0,0] -> first fluorescent channel: 2.1 pixels up, 0.6 pixels right, second fluorescent channel: no offset)
parametersPartI.offsets_fluorescent2 = [0,0;0,0]; %offsets fluorescent channels after rotation (eg. [-2.1,0.6;0,0] -> first fluorescent channel: 2.1 pixels up, 0.6 pixels right, second fluorescent channel: no offset); row information is used of present i.e. - vertical offset is used for row 2 fovs
parametersPartI.save_preprocessed_fov_as_tif = false;% save preprocessed fov
parametersPartI.debug = [0,0,0,0,0,0,0,0];% (1)loading, (2)stabilizing, (3)straightening, (4)ptmr, (5)mask, (6)stabilizing precis, (7) mask alignment and cropping, (8)extracting channel

MSpreprocess(parametersPartI);

%% pre Part II : testing segmentation, finding segmentation parameters
% This step can be run to figure out with which parameters supersegger should run
parametersPrePartII.base = "Path/To/Folder/";
parametersPrePartII.supersegger_const = '100XPa_4'; % load the parameters which supersegger needs for the segmentaion
parametersPrePartII.supersegger_cut_test = 210;%round(linspace(80,230,10)); % these parameters will be tested for for CUT_INT (influences the size of the cells)
parametersPrePartII.supersegger_radius_test = 30;%round(linspace(1,45,15)); % 
parametersPrePartII.supersegger_threshold_test = round(linspace(4,35,30)); % these parameters will be tested for for MAGIC_THRESH (influences when a cell divdes)
parametersPrePartII.channel_id = 1; % this channel will be tested
parametersPrePartII.background = 'phase_subtracted'; % will be used as background (phase/phase_subtracted/fluor1/...)
    
MSsegment_test(parametersPrePartII)

%% Part II : segmentation; generates a segmented stack of each channel (segmentation.tif)
parametersPartII.base = "Path/To/Folder/";
parametersPartII.ignore_these_channels = []; %ignore these channels (eg. [1,12;4,7] ignore channel 12 from fov 1 and ignore channel 7 from fov 4)
parametersPartII.n_fragments = 4; % splits each channel into n fragments which are processed individually (reduces ram usage)
parametersPartII.n_channels = 5; % analyzes n channels simultaneously (a max of n_fragments*n_channels workers can be used in parallel)
parametersPartII.reset_pool = true; % resets the parpool after n channels
parametersPartII.supersegger_const = '100XPa_4'; % load the parameters which supersegger needs for the segmentaion
parametersPartII.supersegger_cut = 210; % influences the size of the cells
parametersPartII.supersegger_radius = 30; % the shape of the mask (higher -> smoother outline)
parametersPartII.supersegger_threshold = 12; % influences when a cell divdes

MSsegment(parametersPartII)

%% Part III : tracking; identifies cell cycles and creates mothersegger_data.mat
parametersPartIII.base = "Path/To/Folder/";
parametersPartIII.runLocal = true;
parametersPartIII.n_channels = 200; % analyzes n channels simultaneously, should be a multiple of the number of workers in the pool, choose according to RAM availability
parametersPartIII.growth_rate = 0.006; % approximate growth rate
parametersPartIII.error_range = 20; % when tracking cells within this error range are still considered to be blanaced (in the same cycle)
parametersPartIII.cell_area_threshold = 300;%200; %cells smaller than this are not considered as cells (pixel)
parametersPartIII.min_duration_threshold = 20; %cell cycles with a duration of less than duration_threshold are flagged as bad (pixel)
parametersPartIII.max_duration_threshold = 1200; %cell cycles with a duration of less than duration_threshold are flagged as bad (pixel)
parametersPartIII.min_area_threshold = 350;%250; %cell cycles where the area of the smallest cell is less than min_area_threshold are flagged as bad (pixel)
parametersPartIII.max_area_threshold = 2250; %cell cycles where the area of the biggest cell is greater than max_area_threshold are flagged as bad (pixel)

MStracking(parametersPartIII)

%% Part IV : foci detection & tracking
%trainspotdetection;%GUI to generate parameter files
parametersPartIV.base = "Path/To/Folder/";
p1=load("Path/To/Parameters/parm.mat");%load parameters
parametersPartIV.params={p1.params,'',p1.params};
parametersPartIV.channels=[1 0];
parametersPartIV.ignore_bad_cells=1;
parametersPartIV.ignore_border_cells=1;
parametersPartIV.n_channels=110; %should be a multiple of the number of workers in the pool, choose according to RAM availability

MSspot_detection(parametersPartIV)%adds foci data to cycle_list

%% pre Part V : fix offest
%Offests in the fluorescent channels can be removed with the MSfixOffset script
%edit MSfixOffset.m
%% Part V : add line profiles and save a combined file
parametersPartV.base = "Path/To/Folder/";
parametersPartV.add_lineprofiles = [1,2];%add fluorescense line profile field to the mothersegger data,
parametersPartV.add_realtive_foci = false;%adds a field to the data which contains the position of the foci relative to the longaxis (longaxis is determined by fitting an elipse around the cell).
parametersPartV.save_only_complete = false;%save only the infromation about complete cycles
parametersPartV.do_not_save_border = true;
parametersPartV.do_not_save_bad = true;
parametersPartV.save_combined_file = true;%save all information from each channel into a combined file
%Important: save_only_complete in combination with overwrite_channels removes all cycles which are not labbled as complete in mothersegger_data.mat files! To get them back PartIII needs to be re run 
parametersPartV.overwrite_channels = false;%go into each channel folder and update/overwrite mothersegger_data.mat

MSsupplement(parametersPartV);
%% fix offset
% uses line profiles to detect vertical offset in the fluorescent channels
% Part IV (foci detection) is not required for this can be ran after the
% offset correction is applied. If foci detection has already been done,
% foci can be shifted to correct but is only trustworthy if foci are far
% enough within cell boundaries.
%edit MSfixOffset.m


















