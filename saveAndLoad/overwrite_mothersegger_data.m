function overwrite_mothersegger_data(cycle_list,channel_list)
    for channel_id = 1:numel(channel_list)
        channel = channel_list(channel_id);
        cycle_ids = channel.cycle_ids(:);
        cycle_ids = unique(cycle_ids(~isnan(cycle_ids)));
        cycle_list_tmp = struct();
        if numel(cycle_ids) > 0
            mini = min(cycle_ids);
            maxi = max(cycle_ids);
            cycle_list_tmp = cycle_list(mini:maxi);
            for cycle_id = 1:(maxi-mini+1)
                cycle = cycle_list_tmp(cycle_id);
                cycle.channel_id = 1;
                cycle.cycle_id = cycle.cycle_id-mini+1;
                cycle.relation_graph.parent = cycle.relation_graph.parent-mini+1;
                cycle.relation_graph.children = cycle.relation_graph.children-mini+1;
                cycle_list_tmp(cycle_id) = cycle;
            end
            channel.cycle_ids = channel.cycle_ids-mini+1;
        end
        channel.channel_id = 1;
        save_mothersegger_data(channel.path,channel,cycle_list_tmp);
    end
end