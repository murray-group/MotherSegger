function [cycle_list,channel_list,fov_list] = load_combined_data(path)
    data = load(path);
    channel_list = data.channel_list;
    cycle_list = data.cycle_list;
    if ismember('fov_list',fieldnames(data))
        fov_list = data.fov_list;
    else
        fov_list = {};
    end
end