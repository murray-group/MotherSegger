function [combined_fov] = load_segmented_fov(path,segments,starting_frame,offsets_fluorescent,debug)
    fovs = cell(1,numel(segments));
    max_height = 0;
    max_width = 0;
    n_frames = 0;
    for s_it = 1:numel(segments)
        p = strrep(path,segments(1),segments(s_it));
        if s_it > 1,starting_frame = 0;end
        fov = load_fov(p,starting_frame,offsets_fluorescent,debug);
        fovs{s_it} = fov;
        if fov.width > max_width,max_width=fov.width;end
        if fov.height > max_height,max_height=fov.height;end
        n_frames = n_frames+fov.n_frames;
    end
    
    combined_fov = struct();
    combined_fov.name = fov.name;
    combined_fov.fov_id = fov.fov_id;
    combined_fov.channel_dimensions = fov.channel_dimensions;
    combined_fov.n_frames = n_frames;
    combined_fov.height = max_height;
    combined_fov.width = max_width;
    combined_fov.number_of_fluors = fov.number_of_fluors;
    combined_fov.image_data_names = fov.image_data_names;
    for name = fov.image_data_names
        canvas = uint16(zeros(max_height,max_width,n_frames));
        index = 0;
        for s_it = 1:numel(segments)
            fov = fovs{s_it};
            canvas(end-fov.height+1:end,1:fov.width,index+1:index+fov.n_frames) = fov.(name);
            index = index+fov.n_frames;
        end
        combined_fov.(name) = canvas;
    end
end
