function [channel_list,cycle_list,fov_list] = load_mothersegger_data(result_paths,only_complete_cycles,no_border,no_bad)
    
    %initalize stuff
    n_channels = numel(result_paths);
    channel_list = cell(1,n_channels);
    cycle_lists = cell(1,n_channels);
    fov_list = {};
    fov_paths = "DoesNotExist";
    
    disp(strcat("loading ",string(n_channels)," channels "))
    ms_list = cell(1,n_channels);
    parfor channel_it = 1:n_channels
        channel_path = string(result_paths{channel_it});
        ms_path = strcat(channel_path,'mothersegger_data.mat');%create path pointing to mothersegger_data
        ms_data = load(ms_path);
        ms_list{channel_it}=ms_data;
    end

    for channel_it = 1:n_channels
        channel_path = string(result_paths{channel_it});
        ms_data = ms_list{channel_it};
        channel_data = ms_data.channel_list;%there should only be one element in channel list
        channel_data.path = channel_path;%update path if the folder was moved
        cycle_list = ms_data.cycle_list;
        if only_complete_cycles%remove all cycles which are not flagged complete
            [cycle_list,channel_data] = filter_complete_cycles(cycle_list,channel_data);
        elseif no_bad || no_border
            [cycle_list,channel_data] = filter_bad_border_cycles(cycle_list,channel_data,no_bad,no_border);
        end
        
        %load fov data and set the right fov id in channel_data
        fov_path = strsplit(channel_path,'channel_');
        fov_path = strcat(fov_path{1},'fov_data.mat');%create path pointing to fov_data
        if exist(fov_path,'file')%if fov data exist (in older version it does not)
            fov_id = find(strcmp(fov_paths,fov_path));%check if we encountered current fov_data already and get its id
            if numel(fov_id) ~= 1%if we encountered current fov_data already we are done otherwise we need to add it to the list of fov_data and update its fov id
                fov_data = load(fov_path);
                fov_data = fov_data.fov_data;
                fov_id = numel(fov_list)+1;
                fov_data.fov_id = fov_id;
                fov_list{fov_id} = fov_data;
                fov_paths{fov_id} = fov_path;
            end
        else 
            fov_id = 1;
        end
        
        channel_data.fov_id = fov_id;%update fov_id in channel_data
        channel_list{channel_it} = channel_data;
        cycle_lists{channel_it} = cycle_list;
        
        
    end
    
    %make lists into struct arrays
    [channel_list,cycle_list] = combine_mothersegger_data(channel_list,cycle_lists);
    fov_list = cat(2,fov_list{:});
end



function [new_cycle_list,channel]  = filter_bad_border_cycles(cycle_list,channel,bad,border)

    cycle_mapper = nan(numel(cycle_list),1);
    cycle_count = 1;
    for cycle_it = 1:numel(cycle_list)%iterate over each cycle and update its id if it is a complete cycle and add it to new_cycle_list
        cycle = cycle_list(cycle_it);
        if (bad==0 || (bad && cycle.flags.bad==0)) && (border==0 || border && cycle.flags.border_cell==0)
            cycle_mapper(cycle.cycle_id) = cycle_count;%also rember which old cycle_id maps to the new cycle_id (nan for not complete cycles)
            cycle.cycle_id = cycle_count;
            new_cycle_list(cycle_count) = cycle;
            cycle_count = cycle_count + 1;
        end
    end
    
    if ~exist('new_cycle_list','var'), new_cycle_list = []; end
    
    for cycle_it = 1:numel(new_cycle_list)%update the relation_graph of the new_cycle_list with the help of cycle_mapper
        cycle = new_cycle_list(cycle_it);
        if ~isnan(cycle.relation_graph.parent)
            cycle.relation_graph.parent = cycle_mapper(cycle.relation_graph.parent);
        end
        if ~isnan(cycle.relation_graph.children(1))
            cycle.relation_graph.children(1) = cycle_mapper(cycle.relation_graph.children(1));
        end
        if ~isnan(cycle.relation_graph.children(2))
            cycle.relation_graph.children(2) = cycle_mapper(cycle.relation_graph.children(2));
        end
        new_cycle_list(cycle_it) = cycle;
        %}
    end
    
    tmp = ~isnan(channel.cycle_ids);
    channel.cycle_ids(tmp) = cycle_mapper(channel.cycle_ids(tmp));%update the cycle_ids in channel_data
    channel.cycle_indices(isnan(channel.cycle_ids)) = nan;%and also remove the indices which are not longer needed
    
end





function [new_cycle_list,channel]  = filter_complete_cycles(cycle_list,channel)

    cycle_mapper = nan(numel(cycle_list),1);
    cycle_count = 1;
    for cycle_it = 1:numel(cycle_list)%iterate over each cycle and update its id if it is a complete cycle and add it to new_cycle_list
        cycle = cycle_list(cycle_it);
        if cycle.flags.complete
            cycle_mapper(cycle.cycle_id) = cycle_count;%also rember which old cycle_id maps to the new cycle_id (nan for not complete cycles)
            cycle.cycle_id = cycle_count;
            new_cycle_list(cycle_count) = cycle;
            cycle_count = cycle_count + 1;
        end
    end
    
    if ~exist('new_cycle_list','var'), new_cycle_list = []; end
    
    for cycle_it = 1:numel(new_cycle_list)%update the relation_graph of the new_cycle_list with the help of cycle_mapper
        cycle = new_cycle_list(cycle_it);
        if ~isnan(cycle.relation_graph.parent)
            cycle.relation_graph.parent = cycle_mapper(cycle.relation_graph.parent);
        end
        if ~isnan(cycle.relation_graph.children(1))
            cycle.relation_graph.children(1) = cycle_mapper(cycle.relation_graph.children(1));
        end
        if ~isnan(cycle.relation_graph.children(2))
            cycle.relation_graph.children(2) = cycle_mapper(cycle.relation_graph.children(2));
        end
        new_cycle_list(cycle_it) = cycle;
        %}
    end
    
    tmp = ~isnan(channel.cycle_ids);
    channel.cycle_ids(tmp) = cycle_mapper(channel.cycle_ids(tmp));%update the cycle_ids in channel_data
    channel.cycle_indices(isnan(channel.cycle_ids)) = nan;%and also remove the indices which are not longer needed
    
end

function fov_data = empty_fov_data()%create and empty fov_data (this function might be moved to an own script together with other init functions)
    fov_data = struct();
    fov_data.sample_time = 60;
    channel_dimensions = struct();
    channel_dimensions.width = -1;
    channel_dimensions.height = -1;
    channel_dimensions.pitch = -1;
    channel_dimensions.n_channels = -1;
    channel_dimensions.halo = -1;
    channel_dimensions.height_constriction = -1;
    fov_data.channel_dimensions = channel_dimensions;
    fov_data.path = 'DoesNotExists';
    fov_data.fov_id = -1;
    fov_data.name_id = -1;
    fov_data.pixel_to_micrometer_ratio = 0.0668;
    fov_data.pixel_to_micrometer_ratio_adjusted = 0.0668;
    fov_data.n_fluors = -1;
    fov_data.n_frames = -1;
    fov_data.row = 0;
    mask = struct();
    mask.image = false(2,2);
    mask.bb_channels = ones(2,2,7)*-1;
    mask.bb=ones(2,2)*-1;
    fov_data.mask = mask;
end




