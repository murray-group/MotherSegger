
function channel = load_channel(path)

    if ismac
        slash = '\';%Mac
    elseif isunix
        slash = '/';%linux
    elseif ispc
        slash = '\';%windows
    else
        slash = '/';%other
    end

    %{
    path_original = strsplit(path,slash);
    path_original = strcat(strjoin(path_original(1:end-2),slash),'.tif');
    
    %figure out frames_total
    tiff_info = imfinfo(path_original);
    tiff_info
    description = tiff_info(1);
    tmp = description.ImageDescription;%parses the shape of the tif stack. IMPORTANT this only works for tif files created with fiji!!!
    tmp = splitlines(tmp);
    frames_total = split(tmp{find(cellfun(@(x) startsWith(x,'frames'),tmp),1,'first')},'='); %extract total number of frames from info
    frames_total = str2num(frames_total{2});
    %}

    %set up paths
    names = dir(path);
    tmp = names(1);
    names(1) = names(end);
    names(end) = tmp;
    tmp = {names.name};
    names = tmp(startsWith(tmp, 'phase') & (endsWith(tmp, 'adjusted.tif') | endsWith(tmp, 'subtracted.tif')));
    names = names(~matches(names,'segmentation.tif'));
    paths = strcat(path,names);
    
    %load tifs

    channel = struct();
    for i = 1:numel(paths)
        path_to_image = paths{i};
        name = strsplit(names{i},'.');
        name = name{1};
        channel.(name)=loadTiff(path_to_image);
    end
    
    n_frames=size(channel.(name),3);
    
    field_names = fieldnames(channel);
    image_data_names = cell(0,0);
    image_data_names_processed = cell(0,0);
    for i = 1:numel(field_names)
        fn = field_names{i};
        if endsWith(fn,'adjusted') || endsWith(fn,'subtracted')
            image_data_names_processed{numel(image_data_names_processed)+1} = fn;
        else
            image_data_names{numel(image_data_names)+1} = fn;
        end
    end
    channel.image_data_names = string(image_data_names);
    channel.image_data_names_processed = string(image_data_names_processed);
    channel.n_frames = n_frames;
    channel.number_of_fluors = numel(image_data_names_processed)-1;
    channel.path = path;
    
end