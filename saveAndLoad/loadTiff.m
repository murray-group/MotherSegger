function [tif] = loadTiff(path)
    %path = strrep(path,"970EVOPlus1","970EVOPlus"); %only get rid of the 1. I hope noone ever uses the pattern 907EvoPlus1 :S
	%path = '/media/koehlerr/970EVOPlus1/Data/Microfluid/20210521/matP dataset/Test/matp-ypet_02_09_25_2/channel_0004/segmentation.tif'
    %     tiff_info = imfinfo(path);
    %     frames_total = length(tiff_info);
    %     description = tiff_info(1);
    %     dimensions = [description.Height,description.Width];
    %     tif = uint16(zeros(dimensions(1),dimensions(2),frames_total,'uint16'));
    import ScanImageTiffReader.ScanImageTiffReader;
    path = char(path);%if path is of type string matlab crashes on me :(
    reader=ScanImageTiffReader(path);
    tif=reader.data();
    tif=permute(tif,[2,1,3]);%ScanImages tranposes each image, fixing
    %     for frame_it = 1:frames_total%reads tif file frame by frame
    %         tif(:,:,frame_it) = imread(path,'Index', frame_it,'Info',tiff_info);
    %     end
end
%%