
function [path_channels] = get_channel_paths(base,is_analyzed)
    if ismac
        slash = '\';%Mac
    elseif isunix
        slash = '/';%linux
    elseif ispc
        slash = '\';%windows
    else
        slash = '/';%other
    end

    names = dir(base);
    names = {names.name};
    path_channels = cell(0,0);
    for i = 1:numel(names)
        name = names{i};
        base_folders_path = strcat(base,name);
        if ~startsWith(name,'.') && isfolder(base_folders_path)
            base_folders_path = strcat(base_folders_path,slash);
            channel_names = dir(base_folders_path);
            channel_names = {channel_names.name};
            for j = 1:numel(channel_names)
                channel_name = channel_names{j};
                if startsWith(channel_name,'channel_')
                    channel_path = strcat(base_folders_path,channel_name,slash);
                    if isfolder(channel_path)
                        b = false;
                        if is_analyzed
                            analyzed_names = dir(channel_path);
                            analyzed_names = {analyzed_names.name};
                            b = any(ismember(analyzed_names,'mothersegger_data.mat'));
                        end
                        if ~is_analyzed || b
                            path_channels{numel(path_channels)+1} = channel_path;
                        end
                    end
                end
            end
        end
    end
    path_channels = string(path_channels);
end