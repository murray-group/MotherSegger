%process_path parse the information of a tif-file and parse the whole file as a stack and
%places this stack into a matrix. Further it parses the name of the tif-
%file and collects the information describing the layout of the tif-file
function [fov] = load_fov(path,starting_frame,offsets_fluorescent,debug)
    path = convertStringsToChars(path); %sometimes matlab chrashes if not converted to char

    
    import ScanImageTiffReader.ScanImageTiffReader;
    reader=ScanImageTiffReader(path);
    img=reader.data();
    img=permute(img,[2,1,3]);%ScanImages tranposes each image, fixing, channel and time are mixed up in the third dimension
    
    height=size(img,1);
    width=size(img,2);
    
    
    %description_img = description_tiff.ImageDescription;%parses the shape of the tif stack. IMPORTANT this only works for tif files created with fiji!!!
    description_img = reader.descriptions{1};
    description_img = splitlines(description_img);
    n_frames = split(description_img{find(cellfun(@(x) startsWith(x,'frames'),description_img),1,'first')},'='); %extract total number of frames from info
    n_frames = str2num(n_frames{2});
    %n_frames = 60
    n_channels = split(description_img{find(cellfun(@(x) startsWith(x,'channels'),description_img),1,'first')},'='); %extract total number of channels from info
    if(numel(n_channels) < 1 || strcmp(n_channels{1},'='))
        n_channels = 1;
    else
        n_channels = str2num(n_channels{2});
    end


    fov = struct();%create an empty field of view stuct

    [name_info,name] = parse_path(path);%get information from tif name
    fov.name = name;
    fov.fov_id = name_info(5);
    fov.row = name_info(6);
    fov.channel_dimensions.width = name_info(1);
    fov.channel_dimensions.height = name_info(2);
    fov.channel_dimensions.pitch = name_info(3);
    fov.channel_dimensions.n_channels = name_info(4);
    fov.n_frames = n_frames-starting_frame;
    fov.height = height;
    fov.width = width;

    fov.channel_dimensions.radius=1.5;%radius of circles
    fov.channel_dimensions.offset=4.5;%distance from channel to centre of circle


    ffns = cell(1,0); %fluorescent field names
    for i = 2:n_channels
         fluor_field_name = strcat("fluor",string(i-1));
         ffns(i-1) = {fluor_field_name};
    end
    fov.number_of_fluors = n_channels-1;
    fov.image_data_names = ["phase",ffns];


    for channel = 1:n_channels %dissect stack
        fov.(fov.image_data_names(channel))  = zeros(height,width,n_frames-starting_frame,'uint16');
    end


    for channel = 1:n_channels
        for frame = (1+starting_frame):n_frames%reads tif file frame by frame
            %fov.(fov.image_data_names(channel))(frame-starting_frame,:,:)  = imread(path,'Index', (frame-1)*n_channels+channel,'Info',tiff_info);
            fov.(fov.image_data_names(channel))(:,:,frame-starting_frame)  = img(:,:,(frame-1)*n_channels+channel);
        end
    end

    % apply offset to fluorescent channel if it is specified in the parameters
    stop = min([size(offsets_fluorescent,1),n_channels-1])+1;
    
    if isfield(fov, 'row') && fov.row~=0 % if row information is present, apply -vertical offset to row 2 fovs
        offsets_fluorescent(:,1)=-(2*fov.row-3)*offsets_fluorescent(:,1);
    end
    for channel = 2:stop
        if any(offsets_fluorescent(channel-1,:))
            fov.(fov.image_data_names(channel))=imtranslate(fov.(fov.image_data_names(channel)),flip(offsets_fluorescent(channel-1,:)));
        end
    end

    % show information about the loaded tif (quality check)
    if debug
        nof = fov.number_of_fluors;
        figure
        subplot(1,nof+1,1)
        imagesc(max(fov.phase,[],3))
        title('phase (max)')
        colorbar()
        for i=1:nof
            name = strcat('fluor',string(i));
            subplot(1,nof+1,i+1)
            imagesc(mean(fov.(name),3))
            title(name)
            colorbar()
        end
        sgtitle('loading')
        pause(1)
    end
end




