%This function save the whole fov as individual 
function save_cropped_fov(path,fov)
    path = strcat(path,fov.name,'/fov/');%folder where the files will be saved
    if ~exist(path, 'dir')%check if the folder already exists
       mkdir(path)
    end
    
    for name = fov.image_data_names%for each channel of the fov
        path_fov = strcat(path,name,'.tif');
        writeTiff(path_fov,fov.(name));%save one .tif file
    end
end