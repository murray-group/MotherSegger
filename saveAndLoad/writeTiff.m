
function writeTiff(path,tiff_stack)
%     imwrite(squeeze(tiff_stack(:,:,1)),path);
%     for ii = 2 : size(tiff_stack, 3)
%         imwrite(tiff_stack(:,:,ii),path,'WriteMode','append');
%         %imwrite(tiff_stack(:,:,ii),path,'WriteMode','append','Compression','lzw');
%     end
    compression=0;
    pixels_per_cm=1/0.065e-4;
    
    %import ScanImageTiffReader.ScanImageTiffReader;
    
    w=whos('tiff_stack');
    if w.bytes<4e9
        fTIF = Fast_Tiff_Write(path,pixels_per_cm,compression);
    else
        fTIF = Fast_BigTiff_Write(path,pixels_per_cm,compression);
    end
    
    for i=1:size(tiff_stack,3)
       fTIF.WriteIMG(permute(tiff_stack(:,:,i),[2,1]));
    end
       fTIF.close();
    
    %if numel(size(tiff_stack))<4
    %else
    %    %import ScanImageTiffReader.ScanImageTiffReader;
    %    fTIF = Fast_Tiff_Write(path,pixels_per_cm,compression);
    %    for i=1:size(tiff_stack,3)
    %        for j=1:size(tiff_stack,4)
    %            fTIF.WriteIMG(permute(tiff_stack(:,:,i,j),[2,1]));
    %        end
    %    end
    %    fTIF.close();
    %    
    %end
end

