function [seg_info] = load_segmentation(channel_path)
    foci_list_path = strcat(channel_path,'supersegger_foci');
    if isfile(foci_list_path)
        foci_list = load(strcat(channel_path,foci_list_path));
        foci_list = foci_list.foci_list;
    else
        foci_list = zeros(0,10);
    end
    number_of_fluors = how_many_fluors(channel_path);
    %channel_path
    segmentation = loadTiff(strcat(channel_path,'segmentation.tif'));
    %segmentation = permute(segmentation,[3,1,2]);
    seg_info = struct();
    seg_info.segmentation = segmentation;
    seg_info.foci_list = foci_list;
    seg_info.number_of_fluors = number_of_fluors;
    seg_info.channel_path = channel_path;
end


function [number_of_fluors] = how_many_fluors(channel_path)
    prefix = 'fluor';
    paths = dir(channel_path);
    names = {paths.name};
    names = names(startsWith(names,prefix));
    number_of_fluors = 0;
    for name = names
        name = name{:};
        f = str2num(name(length(prefix)+1));
        if number_of_fluors < f
            number_of_fluors = f;
        end
    end
end
