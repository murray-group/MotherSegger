%get all files which end with .tif in a give dir
function [paths] = get_fov_paths(base)
    names = dir(base);
    names = {names.name};
    names = names(endsWith(names, '.tif') | endsWith(names, '.btf'));
    paths = strcat(base,names);
end