

function [path_fov] = save_channels(fov,path)
    if ismac
        slash = '\';%Mac
    elseif isunix
        slash = '/';%linux
    elseif ispc
        slash = '\';%windows
    else
        slash = '/';%other
    end
    path_fov = strsplit(path,slash);
    path_fov = strcat(strjoin(path_fov(1:end-1),slash),slash,fov.name,slash);
    for i = 1:fov.channel_dimensions.n_channels-1
        channel = fov.channels(i);
        if ~exist(path_fov, 'dir')
           mkdir(path_fov)
        end
        path_channel = strcat(path_fov,'channel_',num2str(i,'%04.f'),slash);
        if ~exist(path_channel, 'dir')
           mkdir(path_channel)
        end
        
        for name = channel.save_these_fields%['phase',channel.image_data_names_processed]
            path_image = strcat(path_channel,name,'.tif');
            writeTiff(path_image,channel.(name));
        end
    end
    
    %save empty channel
    channel_empty = fov.channel_empty;
    if ~exist(path_fov, 'dir')
       mkdir(path_fov)
    end
    path_channel = strcat(path_fov,'empty',slash);
    if ~exist(path_channel, 'dir')
       mkdir(path_channel)
    end

    for name = ['phase',channel_empty.save_these_fields]
        path_image = strcat(path_channel,name,'.tif');
        writeTiff(path_image,channel_empty.(name));
    end
end