%% load Experiment
base = "/media/koehlerr/970EVOPlus/Data/Microfluid/20211008_ParB/combined/";
base = "/media/koehlerr/970EVOPlus/Data/Microfluid/Short/";
%base = "/media/koehlerr/970EVOPlus/Data/Microfluid/20210701/Test/";
result_paths = get_channel_paths(base,true);
[channel_list,cycle_list] = load_mothersegger_data(result_paths,false);%load cycles from a specified path (either all (false) or only complete (true))
complete_cycle_ids = get_complete_cycle_ids(cycle_list);
disp('done')

%% show foci all vs max
fluor_selector = 1;
[x_all,y_all,x_min,y_min,x_max,y_max] = plot_foci_intensity_vs_sigma(cycle_list,complete_cycle_ids,fluor_selector);
xlim([0,50])
%% foci heatmap
[canvas] = scatter_to_heatmap(x_max,y_max,[20,20]);
figure(5)
imagesc(flipud(canvas))
colorbar()
%% test foci seperator line (a:slope, b:intersection)
%filter conditions
%sigma > intensity*a+b;
%intensity > c
%sigma > d
%sigma < intensity*e+f;
a = -0.01;
b = 0.7;
c = 3;
d = 0.6;
e = 0.02;
f = 1.2;

test_foci_seperator_line(x_all,y_all,x_min,y_min,x_max,y_max,a,b,c,d,e,f);
xlim([0,50])
%% filter foci
cycle_list_new = filter_foci(cycle_list,1:numel(cycle_list),1,a,b,c,d,e,f);
[x_all,y_all,x_min,y_min,x_max,y_max] = plot_foci_intensity_vs_sigma(cycle_list_new,complete_cycle_ids,fluor_selector);
test_foci_seperator_line(x_all,y_all,x_min,y_min,x_max,y_max,a,b,c,d,e,f);
xlim([0,50])
%% kick foci which are too close together
distance_thresh = 3; %if two foci are closer than 7 pixels the les-bright one will be removed
cycle_list_new = kick_foci_which_are_too_close(cycle_list_new,complete_cycle_ids,distance_thresh,1);
[x_all,y_all,x_min,y_min,x_max,y_max] = plot_foci_intensity_vs_sigma(cycle_list_new,complete_cycle_ids,fluor_selector);
test_foci_seperator_line(x_all,y_all,x_min,y_min,x_max,y_max,a,b,c,d,e,f);
xlim([0,50])
%% resort foci
cycle_list_new = resort_foci(cycle_list_new);
%% get trajectories (to check how many good trajectories we get)
fluor_selector = 3;
foci_selector = 1;
trajectory_min_length = 6;
interpolate_points_in_a_row = 1;
set_mean_to_0 = false;
flip_by_pole = true;
seperate = true;
remove_bb = false; %only for old data sets
cycle_ids = complete_cycle_ids;
trajectory_list = get_trajectories(cycle_ids,cycle_list_new,foci_selector,trajectory_min_length,fluor_selector,interpolate_points_in_a_row,set_mean_to_0,flip_by_pole,seperate,remove_bb);

%% overwrite motherseggger data
overwrite_mothersegger_data(cycle_list_new,channel_list);

%% show sorted foci one by one
cycle_ids = complete_cycle_ids;
for cycle_it = 1:numel(cycle_ids)
    cycle = cycle_list_new(cycle_ids(cycle_it));
    for fluor_index = 1:cycle.number_of_fluors
        foci_field_name = strcat('foci',string(fluor_index));
        foci_characteristics_list = cat(3,cycle.(foci_field_name).intensity,cycle.(foci_field_name).sigma);
        foci_list = cycle.(foci_field_name).position;
        plot(foci_list(:,:,1),'-o');
        pause(3)
    end
    cycle_list(cycle_ids(cycle_it)) = cycle;
end
