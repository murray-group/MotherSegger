function MSsupplement(parametersPartV)
    base = parametersPartV.base;
    result_paths = get_channel_paths(base,true);
    [channel_list,cycle_list,fov_list] = load_mothersegger_data(result_paths,parametersPartV.save_only_complete,parametersPartV.do_not_save_border,parametersPartV.do_not_save_bad);%load cycles from a specified path (either all (false) or only complete (true))
    %complete_cycle_ids = get_complete_cycle_ids(cycle_list);
    if nnz(parametersPartV.add_lineprofiles)
        cycle_list = add_lineprofiles(channel_list,cycle_list,1,parametersPartV.add_lineprofiles);
    end
    
    if parametersPartV.add_realtive_foci
        [cycle_list] = add_relative_foci_positions(cycle_list);%add relative foci position (major, minor, angle, centroid of fitted elpise) to all cells
    end
    
    if parametersPartV.overwrite_channels
        overwrite_mothersegger_data(cycle_list,channel_list)
    end
    
    if parametersPartV.save_combined_file
        disp(strcat("saving combined file: ",base,'combined_data.mat'))
        save_combined_data(strcat(base,'combined_data.mat'),cycle_list,channel_list,fov_list)
    end
    
    exp_info_path = strcat(parametersPartV.base,'experiment_info.mat');
    if isfile(exp_info_path)
        experiment_info = load(exp_info_path);
        experiment_info = experiment_info.experiment_info;
    else
        experiment_info = struct();
    end
    experiment_info.PartV = parametersPartV;
    
    save(exp_info_path,'experiment_info')
    
   
    %save(strcat(parametersPartV.base,'fov_list.mat'),'fov_list')
    
    disp('Done! (Part IV)')
end

