function apply_offsets_to_channel_images(fov_list,fluor_index,offsets,dim)

    %apply_offet_to_fluor_channels
    % go back to the original images and re-extract the fluorescence channels
    % with an offset. Can therefore be re-ran to apply a new offset or undo an
    % offset

    if dim==1
        
        for i=1:length(fov_list)%loop over fov
            disp(strcat("Fov ",string(i)," of ",string(numel(fov_list))))
            %if ~isfield(fov_list,'row')
            %    row=0;
            %else
            %    row=fov_list(i).row;
            %end
            
            n_channels=fov_list(i).channel_dimensions.n_channels-1;
            
            for channel_it = 1:n_channels
                channel_path=[fov_list(i).path,'channel_',num2str(channel_it,'%04i'),filesep];
                
                
                name=[channel_path,'fluor',num2str(fluor_index),'_subtracted.tif'];
                fluor = loadTiff(name);
                
                %if size(offsets,1)==1 %one fov or row information provided
                %    if row==0 || row ==1
                %        fluor = imtranslate(fluor,[0,offsets]);
                %    else %row=-1
                %        fluor = imtranslate(fluor,[0,-offsets]);
                %    end
                %else
                %    fluor = imtranslate(fluor,[0,offsets(i)]);
                %end
                fluor = imtranslate(fluor,[0,offsets(i)]);
                writeTiff(name,fluor);
            end
            
        end
        
    else
        for i=1:length(fov_list)%loop over fov
            disp(strcat("Fov ",string(i)," of ",string(numel(fov_list))))
            
            n_channels=fov_list(i).channel_dimensions.n_channels-1;
            
            for channel_it = 1:n_channels
                channel_path=[fov_list(i).path,'channel_',num2str(channel_it,'%04i'),filesep];
                
                
                name=[channel_path,'fluor',num2str(fluor_index),'_subtracted.tif'];
                fluor = loadTiff(name);
                
                fluor = imtranslate(fluor,[offsets(i),0]);
                writeTiff(name,fluor);
            end
            
        end
        
    end

end
