
function [cycle_list] = add_relative_foci_positions(cycle_list)
    deal = cell(numel(cycle_list),1);
    [cycle_list.('centroids')] = deal{:};
    [cycle_list.('angles')] = deal{:};
    %[cycle_list.('minor')] = deal{:};
    %[cycle_list.('major')] = deal{:};

    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        [centroids,angles,minor,major] = fit_elipse(cycle.masks);
        for j = 1:cycle.number_of_fluors
            fn = strcat("foci",string(j));
            if~isempty(cycle.(fn))
                if ~isempty(cycle.(fn).position)
                    foci = cycle.(fn).position;
                    pr = make_foci_relative_to_cell_axes(centroids,angles,foci);
                    cycle.(fn).position_relative = pr;
                else
                   cycle.(fn).position_relative=[];
                end
            end
        end
        cycle.centroids = centroids;
        cycle.angles = angles;
        %cycle.minor = minor;
        %cycle.major = major;
        cycle_list(i) = cycle;
    end
end


function [centroids,angles,minor,major] = fit_elipse(masks)
    n = numel(masks);
    centroids = nan(n,2);
    angles = nan(n,1);
    minor = nan(n,1);
    major = nan(n,1);
    for i = 1:n
        s = regionprops(masks{i}, 'Orientation', 'MajorAxisLength', ...
        'MinorAxisLength', 'Eccentricity', 'Centroid');

        centroids(i,:) = fliplr(s.Centroid);
        angles(i) = -90-s.Orientation;  % roation in degrees
        minor(i) = s.MinorAxisLength;
        major(i) = s.MajorAxisLength;
    end
    angles(angles<-90) = angles(angles<-90) + 180;
    
end

function [pr] = make_foci_relative_to_cell_axes(centroids,angles,foci)
    pr = nan(size(foci,1),size(foci,2),size(foci,3));

    foci(:,:,2) = foci(:,:,2)-centroids(:,2);
    foci(:,:,1) = foci(:,:,1)-centroids(:,1);

    pr(:,:,1) = -(foci(:,:,2)).*sind(angles) + (foci(:,:,1)).*cosd(angles);% + c(1);
    pr(:,:,2) =  (foci(:,:,2)).*cosd(angles) + (foci(:,:,1)).*sind(angles);% + c(2);
end
