function [channel_list_new,cycle_list_new] = combine_mothersegger_data(channel_lists,cycle_lists)
    n_channels = numel(channel_lists);
    channel_id_count_global = 0;
    cycle_id_count_global = 0;

    disp('combining channels')
    for channel_it = 1:n_channels
        if ~mod(channel_it,10)
            disp(strcat(string(channel_it)," out of ",string(n_channels)))
        end
        channel_list = channel_lists{channel_it};
        cycle_list = cycle_lists{channel_it};
        if numel(cycle_list)>0 && (numel(fieldnames(cycle_list))>0) 
            channel_id_count = numel(channel_list);
            cycle_id_count = numel(cycle_list);

            for j = 1:channel_id_count
                channel = channel_list(j);
                channel.channel_id = channel.channel_id + channel_id_count_global;
                channel.cycle_ids = channel.cycle_ids + cycle_id_count_global;
                channel_list(j) = channel;
            end

            for j = 1:cycle_id_count
                cycle = cycle_list(j);
                cycle.channel_id = cycle.channel_id + channel_id_count_global;
                cycle.cycle_id = cycle.cycle_id + cycle_id_count_global;
                cycle.relation_graph.parent = cycle.relation_graph.parent + cycle_id_count_global;
                cycle.relation_graph.children = cycle.relation_graph.children + cycle_id_count_global;
                cycle_list(j) = cycle;
            end


            channel_id_count_global = channel_id_count_global + channel_id_count;
            cycle_id_count_global = cycle_id_count_global + cycle_id_count;

            channel_lists{channel_it} = channel_list;
            cycle_lists{channel_it} = cycle_list;
        else
            channel_lists{channel_it} = struct();
            cycle_lists{channel_it} = struct();
        end
    end
    non_empty_channels = cellfun(@(x) numel(fieldnames(x)),cycle_lists) ~= 0;
    channel_list_new = cat(2,channel_lists{non_empty_channels});
    cycle_list_new = cat(2,cycle_lists{non_empty_channels});

end


function [channel_list_new,cycle_list_new] = combine_mothersegger_data_old(channel_lists,cycle_lists)
    elements = numel(channel_lists);
    channel_id_count_global = 0;
    cycle_id_count_global = 0;

    disp('combining channels')
    for i = 1:elements
        if ~mod(i,10)
            disp(i)
        end
        channel_list = channel_lists{i};
        cycle_list = cycle_lists{i};

        channel_id_count = numel(channel_list);
        cycle_id_count = numel(cycle_list);

        for j = 1:channel_id_count
            channel = channel_list(j);
            channel.channel_id = channel.channel_id + channel_id_count_global;
            channel.cycle_ids = channel.cycle_ids + cycle_id_count_global;
            channel_list(j) = channel;
        end

        for j = 1:cycle_id_count
            cycle = cycle_list(j);
            cycle.channel_id = cycle.channel_id + channel_id_count_global;
            cycle.cycle_id = cycle.cycle_id + cycle_id_count_global;
            cycle.relation_graph.parent = cycle.relation_graph.parent + cycle_id_count_global;
            cycle.relation_graph.children = cycle.relation_graph.children + cycle_id_count_global;
            %cycle.relation_graph.child(1) = cycle.relation_graph.child(1) + cycle_id_count_global;
            %cycle.relation_graph.child(2) = cycle.relation_graph.child(2) + cycle_id_count_global;
            cycle_list(j) = cycle;
        end


        channel_id_count_global = channel_id_count_global + channel_id_count;
        cycle_id_count_global = cycle_id_count_global + cycle_id_count;

        if i == 1
            channel_list_new = channel_list;
            cycle_list_new = cycle_list;
        else
            channel_list_new = cat(2,channel_list_new,channel_list);
            cycle_list_new = cat(2,cycle_list_new,cycle_list);
        end

    end
end