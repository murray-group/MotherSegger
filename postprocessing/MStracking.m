function MStracking(parametersPartIII)
    channel_paths = get_channel_paths(parametersPartIII.base,false);
    [channel_paths] = filter_paths(channel_paths);
    disp(strcat("tracking: number of channels ",string(numel(channel_paths))))
    tic
    
    %
    if parametersPartIII.runLocal
        disp('starting prallel')
        parfor channel_it = 1:numel(channel_paths) 
            try
                [seg_info] = load_segmentation(channel_paths(channel_it));
                [channel_list,cycle_list] = process_segmentation(seg_info,parametersPartIII);
                save_mothersegger_data(channel_paths(channel_it),channel_list,cycle_list);
            catch ME
                disp(strcat(channel_paths(channel_it)," failed"))
                disp(ME.message)
            end
        end
    else
        n_channels = parametersPartIII.n_channels;
        for batch_it = 1:n_channels:numel(channel_paths)
            current_end = min(batch_it+n_channels-1,numel(channel_paths));
            disp(strcat("Batch: ",string(batch_it),"-",string(current_end)," channel"))
            current_range = (batch_it:current_end);
            seg_infos = cell(current_range(end)-current_range(1)+1,1);
            for channel_it = current_range
                [seg_info] = load_segmentation(channel_paths(channel_it));
                seg_infos{channel_it-current_range(1)+1} = seg_info;
            end
            results_channel = cell(current_range(end)-current_range(1)+1,1);
            results_cycle = cell(current_range(end)-current_range(1)+1,1);
            parfor channel_it = 1:numel(current_range) 
                try
                    [channel_list,cycle_list] = process_segmentation(seg_infos{channel_it},parametersPartIII);
                    results_channel{channel_it} = channel_list;
                    results_cycle{channel_it} = cycle_list;
                    %save_mothersegger_data(channel_path,channel_list,cycle_list)
                catch ME
                    disp(strcat(channel_paths(current_range(1)+channel_it-1)," failed"))
                    disp(ME.message)
                end
            end

            for channel_it = 1:numel(current_range)
                try
                    save_mothersegger_data(channel_paths(current_range(1)+channel_it-1),results_channel{channel_it},results_cycle{channel_it});
                catch ME
                    disp(strcat(channel_paths(current_range(1)+channel_it-1)," failed"))
                    disp(ME.message)
                end
            end

        end
        
    end
    
    exp_info_path = strcat(parametersPartIII.base,'experiment_info.mat');
    if isfile(exp_info_path)
        experiment_info = load(exp_info_path);
        experiment_info = experiment_info.experiment_info;
    else
        experiment_info = struct();
    end
    experiment_info.PartIII = parametersPartIII;
    save(strcat(parametersPartIII.base,'experiment_info.mat'),'experiment_info')
    
    toc
    disp('Done! (Part III)')
end

function [paths] = filter_paths(paths)
    filter = true(numel(paths),1);
    for path_it = 1:numel(paths)
        path = paths(path_it);
        filter(path_it) = isfile(strcat(path,'segmentation.tif'));
    end
    paths = paths(filter);
end
