function [offsets]=find_offset(cycle_list,channel_list,fov_list,dim,fluor_index,plots)
    %outputs either an array of offsets, one for each fov or the offset for row 1 (row 2 is the same just in the opposite direction)

    
    cycle_ids=get_complete_cycle_ids(cycle_list);%get all complete cell cycles
    
    mother_cycle_ids = get_mother_cycle_ids(channel_list,cycle_list);%remove mother cell cycles
    cycle_ids = cycle_ids(~ismember(cycle_ids,mother_cycle_ids));

    
    channels=extractfield(cycle_list(cycle_ids),'channel_id'); %channel of each cycle
    tmp=extractfield(channel_list,'fov_id');
    fov_ids=tmp(channels); % fov_id of each cycle
    ufov_ids=unique(fov_ids);
    if exist('fov_list','var')% if combined fov_list is loaded
        if isfield(fov_list,'row')
            tmp=extractfield(fov_list,'row');
            rows=tmp(fov_ids); %row of each cycle
        else
            rows=zeros(size(fov_ids));
        end
        fov_names=extractfield(fov_list,'name_id');
    else
        %get from individual fov_data.mat files
        
    end
    

    if ~any(rows==0)%if all row numbers are known (0 basically means the row is unkown)
        rows_ids = sort(unique(rows));
        for r_it = 1:numel(rows_ids)%for each row number
            row_id = rows_ids(r_it);
            ids = cycle_ids(rows==row_id);%get all ids from one row
            if dim == 1
                offsets(r_it)=get_offset_y(cycle_list,ids,fluor_index,[r_it,numel(rows_ids)]*plots);%and calculate the offset
            elseif dim == 2
                offsets(r_it)=get_offset_x(cycle_list,ids,fluor_index,[r_it,numel(rows_ids)]*plots);
            end
            disp(['Row ',num2str(row_id),' offset of fluorescent channels ', num2str(fluor_index),' is ',num2str(offsets(r_it))]);
        end
    else%find the offset for each row individually


        for f_it=1:numel(ufov_ids)%for each field of view find the offset
            ids=cycle_ids(fov_ids==ufov_ids(f_it));
            if dim == 1
                offsets(f_it)=get_offset_y(cycle_list,ids,fluor_index,[f_it,numel(ufov_ids)]*plots);
            elseif dim == 2
                offsets(f_it)=get_offset_x(cycle_list,ids,fluor_index,[f_it,numel(ufov_ids)]*plots);
            end
        end

        if plots==1%plot the offsets off all field of views
            figure
            bar(fov_names(ufov_ids),offsets)
            xlabel('fov (name)');
            ylabel('offset (pixels)')
            title(['offset for fluorescent channel ', num2str(fluor_index)])
        end
    end
        


end

function [out_y]=get_offset_y(cycle_list,cycle_ids,fluor_index,plots) %find the offset in the given cycles with the ids from cycle_ids
    %get an equal number of complete cycle ids with each orientation
    poles=extractfield(cycle_list,'pole');
    poles=poles(cycle_ids);
    cycle_ids_p=cycle_ids(poles==1);
    cycle_ids_n=cycle_ids(poles==-1);
    Np=length(cycle_ids_p);
    Nn=length(cycle_ids_n);
    N=min(Np,Nn);
    cycle_ids=[cycle_ids_p(randperm(Np,N)), cycle_ids_n(randperm(Nn,N))];
    
    cycles=cycle_list(cycle_ids); % Import details of those cell cycles that are identified as complete cycles
    min_len=30;
    max_len=110;
    
    n_ybins=max_len-min_len+1;
    bin_edges_y=min_len-0.5:1:max_len+0.5;
    
    bin_edges_x=-92.5:1:92.5;
    n_xbins=numel(bin_edges_x)-1;
    
    data=zeros(n_ybins,n_xbins);
    n_data=zeros(1,n_ybins);
    
    %make demograph
    lp=strcat("fluor",num2str(fluor_index),"_lp");
    for i=1:numel(cycles)
        if(cycles(i).lengths>=min_len & cycles(i).lengths<=max_len)%cycles that are always between min_len and max_len
            [~,~,bins]=histcounts(cycles(i).lengths,bin_edges_y);
            for j=1:cycles(i).duration
                lp_length=numel(cycles(i).(lp){j});
                lp_offset=round((n_xbins-lp_length)/2)+1;
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+cycles(i).(lp){j}';
                %if rows(i)<2
                %    data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+cycles(i).(lp){j}';
                %else
                %    data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+flip(cycles(i).(lp){j})';
                %end
                n_data(bins(j))=n_data(bins(j))+1;
            end
        end
    end
    data=data./n_data';
    
    offset_range=-6:0.1:6;% minus so that positive offset corresponds to a shift down being required, negative to shift up
    asym=zeros(size(offset_range));
    j=1;
    for i=offset_range
        asym(j)=get_asym(data,i);
        j=j+1;
    end
    
    [~,I]=min(asym);
    out_y=offset_range(I);
    
    if any(plots>0)
        i1 = plots(1);%index of demograph
        i2 = plots(1)+plots(2);%index of asymmetry plot
        c = plots(2);%number of columns in the plot
        

        y=bin_edges_y;%*0.067;
        
        if(i1==1),figure,end%for the first fov make a new figure to put all the other plots in
        subplot(2,c,i1)
        imagesc(bin_edges_x,y,data)
        ylabel("Cell length (pixels)")
        xlabel("Relative position inside cell")

        subplot(2,c,i2)
        [~,pos_min] = min(asym);
        plot(offset_range,asym,'-ob','MarkerFaceColor','b','MarkerSize',2)
        ylimit = ylim();
        hold on
        plot(offset_range([pos_min,pos_min]),ylimit,'--r')
        plot([0,0],ylimit,'--k')
        hold off
        ylim(ylimit)
        xlabel('Offset (pixel)')
        ylabel('Asymmertry')
        legend("Asymmetry","Minimum")
    end
    
    
    
    function asym=get_asym(data,offset)
        asym=0;
        for i=10:30%1:size(data,1)
            lp_length=min_len+i-1;
            lp_offset=round((size(data,2)-lp_length)/2)+1;
            range=lp_offset:(lp_offset+lp_length-1);
            s=data(i,range);
            range_x=[1:lp_length]-(lp_length+1)/2;
            range2=1:floor(lp_length/2-abs(offset));
            asym=asym+nansum(abs(interp1(range_x,s,-offset+range2)-interp1(range_x,s,-offset-range2)));%difference of points same distance from reference (=0-offset)
        end
    end


end

function [out_x]=get_offset_x(cycle_list,cycle_ids,fluor_index,plots)

    cycles=cycle_list(cycle_ids); % Import details of those cell cycles that are identified as complete cycles
    min_len=5;
    max_len=20;
    
    n_ybins=max_len-min_len+1;
    bin_edges_y=min_len-0.5:1:max_len+0.5;
    
    bin_edges_x=-10.5:1:10.5;
    n_xbins=numel(bin_edges_x)-1;
    
    data=zeros(n_ybins,n_xbins);
    n_data=zeros(1,n_ybins);
    
    %make demograph
    lp=strcat("fluor",num2str(fluor_index),"_lp");
    for i=1:numel(cycles)
        if(cycles(i).widths>=11 & cycles(i).widths<=15)%cycles that are always between 11 and 15 pixels
            [~,~,bins]=histcounts(cycles(i).widths,bin_edges_y);
            for j=1:cycles(i).duration
                lp_length=numel(cycles(i).(lp){j});
                lp_offset=round((n_xbins-lp_length)/2)+1;
                data(bins(j),lp_offset:(lp_offset+lp_length-1))=data(bins(j),lp_offset:(lp_offset+lp_length-1))+cycles(i).(lp){j}';
                n_data(bins(j))=n_data(bins(j))+1;
            end
        end
    end
    data=data./n_data';
    %{
    if plots==1
        y=min_len:max_len;%*0.067;
        %
        figure
        imagesc(bin_edges_x,y,data)
        ylabel("Cell width (pixels)")
        xlabel("Relative position inside cell")
    end
    %}
    %%
    offset_range=-3:0.1:3;% minus so that positive offset corresponds to a shift down being required, negative to shift up
    asym=zeros(size(offset_range));
    j=1;
    for i=offset_range
        asym(j)=get_asym(data,i);
        j=j+1;
    end
    
    [~,I]=min(asym);
    out_x=offset_range(I);
    
    if any(plots>0)
        i1 = plots(1);%index of demograph
        i2 = plots(1)+plots(2);%index of asymmetry plot
        c = plots(2);%number of columns in the plot
        

        y=bin_edges_y;%*0.067;
        
        if(i1==1),figure,end%for the first fov make a new figure to put all the other plots in
        subplot(2,c,i1)
        imagesc(bin_edges_x,y,data)
        ylabel("Cell length (pixels)")
        xlabel("Relative position inside cell")

        subplot(2,c,i2)
        [~,pos_min] = min(asym);
        plot(offset_range,asym,'-ob','MarkerFaceColor','b','MarkerSize',2)
        ylimit = ylim();
        hold on
        plot(offset_range([pos_min,pos_min]),ylimit,'--r')
        plot([0,0],ylimit,'--k')
        hold off
        ylim(ylimit)
        xlabel('Offset (pixel)')
        ylabel('Asymmertry')
        legend("Asymmetry","Minimum")
    end
    
    
        function asym=get_asym(data,offset)
            asym=0;
            for i=8:10%1:size(data,1)
                lp_length=min_len+i-1;
                lp_offset=round((size(data,2)-lp_length)/2)+1;
                range=lp_offset:(lp_offset+lp_length-1);
                s=data(i,range);
                range_x=[1:lp_length]-(lp_length+1)/2;
                range2=1:floor(lp_length/2-abs(offset));
                asym=asym+nansum(abs(interp1(range_x,s,-offset+range2)-interp1(range_x,s,-offset-range2)));%difference of points same distance from reference (=0-offset)
            end
        end


end