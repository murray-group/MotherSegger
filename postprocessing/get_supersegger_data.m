function [segmentation,foci_data] = get_supersegger_data(frames_data,channel_path,check_run)
    cell_mask_top_dilation = 0;
    
    n_frames = numel(frames_data);
    mask_frames = cell(n_frames,1);
    foci_list = zeros(0,8);
    foci_index = 1;
    for frame_it = 1:n_frames
        
        frame_data = frames_data(frame_it);
        mask_frame = zeros(size(frame_data.mask_cell));
        n_cells = numel(frame_data.CellA);
        
        % sort cells by order in channel
        mapper = zeros(n_cells,2); %mapper to go from lowest to highets cell 
        mapper(:,1) = 1:n_cells;
        for cell_it = 1:n_cells
            cel = frame_data.CellA{cell_it};
            bb = cel.BB;
            mapper(cell_it,2) = bb(2);
        end
        mapper = sortrows(mapper,2,'descend');
        
        
        for cell_it = 1:n_cells
            cel = frame_data.CellA{mapper(cell_it,1)};
            bb = cel.BB;
            cell_mask = cel.mask;
            cell_mask(1:end-cell_mask_top_dilation,:) = cell_mask(1:end-cell_mask_top_dilation,:) | cell_mask(1+cell_mask_top_dilation:end,:);
            mask_shred = mask_frame(bb(2):bb(2)+bb(4),bb(1):bb(1)+bb(3));
            mask_shred = mask_shred+cell_mask*cell_it;
            mask_frame(bb(2):bb(2)+bb(4),bb(1):bb(1)+bb(3)) = mask_shred;
            
            locus_it = 1;
            while 1
                locus_name = strcat('locus',string(locus_it));
                if ~isfield(cel, locus_name)
                    break
                end
                locus = cel.(locus_name);
                for loci_it = 1:length(locus)
                    loci = locus(loci_it);
                    foci_list(foci_index,:) = [frame_it,cell_it,locus_it,loci.r(2),loci.r(1),loci.intensity,loci.fitSigma,loci.background];
                    foci_index = foci_index+1;
                end
                locus_it = locus_it+1;
            end
        end
        mask_frames{frame_it} = mask_frame;
    end
    mask_frames = uint8(cat(3,mask_frames{:}));
    foci_data.path = strcat(channel_path,'supersegger_foci.mat');
    foci_data.foci_list = foci_list;
    if check_run
        mask_frames = (mask_frames>0)*65535;
    end
    segmentation = uint16(mask_frames);
    
end





%%