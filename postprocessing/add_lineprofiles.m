
function [cycle_list] = add_lineprofiles(channel_list,cycle_list,dim,line_profiles)
    disp("adding lineprofiles")
    for channel_it = 1:numel(channel_list) % for each channel
        if ~mod(channel_it,10)
            disp(strcat(string(channel_it)," out of ",string(numel(channel_list))))
        end
        channel = channel_list(channel_it);
        names = dir(channel.path);
        names = {names.name};
        prefix = 'fluor';
        names = names(startsWith(names,prefix));
        subtracted =  any(endsWith(names,'subtracted.tif'));
        number_of_fluors = 0;
        for name = names
            name = name{:};
            n = str2num(name(length(prefix)+1));
            if n > number_of_fluors, number_of_fluors = n; end
        end
        for fluor_it = 1:number_of_fluors % for each fluor
            if line_profiles(fluor_it)==0
                continue; 
            end
            if subtracted
                fluor_name = strcat('fluor',string(fluor_it),'_subtracted');
            else
                fluor_name = strcat('fluor',string(fluor_it));
            end

            field_name = strcat('fluor',string(fluor_it),'_lp');
            deal = cell(numel(cycle_list),1);
            if channel_it == 1
                [cycle_list.(field_name)] = deal{:};
            end
            fluor = loadTiff(strcat(channel.path,fluor_name,'.tif'));
            
            for frame_it = 1:channel.n_frames % for each frame
                for cycle_list_it = 1:size(channel.cycle_ids,2)
                    cycle_id = channel.cycle_ids(frame_it,cycle_list_it);
                    cycle_index = channel.cycle_indices(frame_it,cycle_list_it);
                    if ~isnan(cycle_id)
                        
                       
                        
                        cycle = cycle_list(cycle_id);
                        bb = cycle.bbs(cycle_index,:,:);
                        y = (bb(1):bb(2));
                        x = (bb(3):bb(4));
                        
                        a = numel(y);
                        b = numel(x);
                        f = uint16(zeros(a,b));
                        yl = y>0 & y<=size(fluor,1);
                        xl = x>0 & x<=size(fluor,2);
                        f(yl,xl) = fluor(y(yl),x(xl),frame_it);
                        
                        m = uint16(cycle.masks{cycle_index});
                        f = f.*m;
                        lp = sum(f,2-dim+1)./sum(m,2-dim+1);
                        if dim == 2
                            lp = lp';
                        end
                        %end
                        
                        
                        
                        cycle.(field_name){cycle_index} = lp; 
                        cycle_list(cycle_id) = cycle;
                    end
                end
            end
        end
    end
end




