%sort foci
function [sorted,sorted_characteristics] = sort_foci(sort_this,characteristics)


    sorted = nan(size(sort_this));
    sorted_characteristics = nan(size(characteristics));
    sorted(1,:,:) = sort_this(1,:,:);
    sorted_characteristics(1,:,:) = characteristics(1,:,:);

    n = size(sort_this,2);
    for t = 1:size(sort_this,1)-1

        this = sorted(t,:,1);
        next = sort_this(t+1,:,1);
        
        this = cat(2,this',(1:n)');
        next = cat(2,next',(1:n)');

        this = this(~isnan(this(:,1)),:);
        next = next(~isnan(next(:,1)),:);

        this = sortrows(this,1);
        next = sortrows(next,1);

        flip = false;
        if numel(this)<numel(next)
            tmp = this;
            this = next;
            next = tmp;
            flip = true;
        end

        n_true = size(next,1);
        n_false = size(this,1)-n_true;

        permutations = logical_permuations(n_true,n_false);
        distance_squared = zeros(size(permutations,1),1);
        for perm_it = 1:size(permutations,1)
            perm = permutations(perm_it,:);
            this_small = this(perm,:);
            distance_squared(perm_it) = sum(this_small(:,1)-next(:,1).^2);
        end
        [~,index] = min(distance_squared);
        this = this(permutations(index,:),:);

        if flip
            tmp = this;
            this = next;
            next = tmp;
        end
        
        mapper = zeros(n,1);
        mapper(this(:,2)) = next(:,2);
        indices = 1:n;
        indices_unused = ~ismember(indices,mapper);
        mapper(mapper==0)=indices(indices_unused);
        sorted(t+1,:,:) = sort_this(t+1,mapper,:);
        sorted_characteristics(t+1,:,:) = characteristics(t+1,mapper,:);
    end
end



function [out] = logical_permuations(n_true,n_false)
    if n_true == 0
        out = false(1,n_false);
    else
        A = false(n_true+n_false,1);
        A(1:n_true) = 1;
        n = numel(A);
        k = sum(A);
        c = nchoosek(1:n,k);
        m = size(c,1);
        out = false(m,n);
        out(sub2ind([m,n],(1:m)'*ones(1,k),c)) = 1;
    end
end


function bool = is_foci(inten,sigma)
    bool =  sigma > inten*-0.075+1.5;
    %bool = true;
end
