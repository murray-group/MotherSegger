function [channel_list,cycle_list] = process_segmentation(seg_info,parameters)
    %channel_path = '/media/koehlerr/970EVOPlus1/Data/Microfluid/20210521/matP dataset/Test/matp-ypet_02_09_25_2/channel_0001/';
    
    % tracking
    growth_rate = exp(parameters.growth_rate);
    error_range = parameters.error_range;
    % filterin
    cell_area_threshold = parameters.cell_area_threshold;
    min_duration_threshold = parameters.min_duration_threshold;
    max_duration_threshold = parameters.max_duration_threshold;
    min_area_threshold = parameters.min_area_threshold;
    max_area_threshold = parameters.max_area_threshold;
    
    foci_list = seg_info.foci_list;%load(strcat(channel_path,'supersegger_foci'));
    %foci_list = foci_list.foci_list;
    number_of_fluors = seg_info.number_of_fluors;%how_many_fluors(channel_path);
    %channel_path
    segmentation = seg_info.segmentation;%loadTiff(strcat(channel_path,'segmentation.tif'));
    %segmentation = permute(segmentation,[3,1,2]);
    n_frames = size(segmentation,3);
    
    channel_struct = struct();
    channel_struct.channel_id = 1;
    channel_struct.n_frames = n_frames;
    channel_struct.path = seg_info.channel_path;
    channel_struct.shape = [size(segmentation,1),size(segmentation,2)];
    
    %generate cell list from segmentation
    n_cells = 1;
    for frame_it = 1:n_frames
        seg_frame = segmentation(:,:,frame_it);
        cell_ids = zeros(0,1);
        mapper = zeros(0,2);
        cell_it = 1;
        cell_mask_it = 1;
        while true
            mask_frame = seg_frame == cell_mask_it;
            if ~any(mask_frame)
                break
            end
            bb = zeros(2,2);
            bb(1) = find(any(mask_frame,2),1,'first');
            bb(2) = find(any(mask_frame,2),1,'last');
            bb(3) = find(any(mask_frame,1),1,'first');
            bb(4) = find(any(mask_frame,1),1,'last');
            mask_cell = mask_frame(bb(1):bb(2),bb(3):bb(4));
            
            cel = struct();
            cel.cell_id = n_cells;
            cel.cycle_id = nan;
            cel.frame_id = frame_it;
            cel.time = frame_it;
            cel.number_of_fluors = number_of_fluors;
            cel.pole = 0;
            cel.mask = mask_cell;
            cel.bb = bb;
            cel.length = bb(2)-bb(1)+1;
            cel.width = bb(4)-bb(3)+1;
            cel.area = sum(mask_cell,'all');
            %cel.color = round(get_color(cell_count)*(2^16-1));
            cel.relation_graph.next = nan;
            cel.relation_graph.prev = nan;
            cel.relation_graph.is_child = 0;
            cel.inferred_segmentation = false;
            
            loci_data = foci_list(foci_list(:,1)==frame_it&foci_list(:,2)==cell_mask_it,3:end);
            for locus_it = 1:number_of_fluors
                locus_data = loci_data(loci_data(:,1)==locus_it,2:end);
                locus_name = strcat('locus',string(locus_it));
                locus_new = struct('number_of_loci',size(locus_data,1),'position',locus_data(:,1:2)-[bb(1)-1,bb(3)-1],'intensity',locus_data(:,3),'sigma',locus_data(:,4),'background',locus_data(:,5));
                cel.(locus_name) = locus_new;
            end

            if cel.area>cell_area_threshold
                cell_list(n_cells) = cel;
                mapper(cell_it,:) = [cel.bb(1),cell_it];
                cell_ids(cell_it) = n_cells;
                
                n_cells = n_cells+1;
                cell_it = cell_it + 1;
            end

            cell_mask_it = cell_mask_it + 1;
        end
        
        frame = struct();
        frame.number_of_cells = cell_it-1;
        mapper = sortrows(mapper,1,'descend');
        frame.cell_ids = cell_ids(mapper(:,2));
        frame_list(frame_it) = frame;
    end

    %track cells in cell_list
    for frame_it = 1:numel(frame_list)-1
        frame1 = frame_list(frame_it);
        frame2 = frame_list(frame_it+1);
        cell_ids1 = frame1.cell_ids;
        cell_ids2 = frame2.cell_ids;
        n_cells1 = numel(cell_ids1);
        n_cells2 = numel(cell_ids2);
        index1 = 1;
        index2 = 1;
        weight1 = 0;
        weight2 = 0;
        ids_frame1 = zeros(0,1);
        ids_frame2 = zeros(0,1);


        while(true)
            %if p1 > e1 || p2 > e2
            %    break
            %end
            if weight1 < weight2
                if index1 > n_cells1
                    break
                end
                a1 = cell_list(cell_ids1(index1)).length;
                if cell_list(cell_ids1(index1)).area > cell_area_threshold 
                    weight1 = weight1 + a1;
                    ids_frame1(numel(ids_frame1)+1) = cell_ids1(index1);
                end
                index1 = index1 + 1;
            else
                if index2 > n_cells2
                    break
                end
                a2 = cell_list(cell_ids2(index2)).length;
                if cell_list(cell_ids2(index2)).area > cell_area_threshold
                    weight2 = weight2 + a2;
                    ids_frame2(numel(ids_frame2)+1) = cell_ids2(index2);
                end
                index2 = index2 + 1;
            end

            balance = weight1*growth_rate-weight2;
            %if balance is found
            if balance >= -error_range && balance <= error_range && weight1 ~= 0 && weight2 ~= 0
                %test(numel(test)+1) = balance;
                for it_i1 = ids_frame1
                    cell_list(it_i1).relation_graph.next = ids_frame2;
                end
                for it_i2 = ids_frame2
                    cell_list(it_i2).relation_graph.prev = ids_frame1;
                end
                if numel(ids_frame1) == 1 && numel(ids_frame2) == 2
                    cell_list(ids_frame2(1)).pole = 1;
                    cell_list(ids_frame2(2)).pole = -1;
                end
                
                weight1 = 0;
                weight2 = 0;
                ids_frame1 = zeros(0,1);
                ids_frame2 = zeros(0,1);
            end
            %break
        end
    end
    
    if ~exist('cell_list','var'), cell_list = []; end
    
    %{
    % fixes segmentation after tracking (cut cells in two)
    for iterator = 1:numel(cell_list)
        cel = cell_list(iterator);
        pc = numel(cel.relation_graph.prev); % parent count
        if pc == 2 % pc > 1 for generic case, it is coded that way but only tested with pc == 2
            b = true;
            for j = 1:pc
                if numel(cell_list(cel.relation_graph.prev(j)).relation_graph.next) ~= 1
                    b = false;
                end
            end
            if b %at this point we identified all cells with two parents and no siblings (parents only have one child)
                prev_indices = cel.relation_graph.prev;
                len = cel.length;
                spacing = zeros(pc,1);
                for j = 1:pc
                    prev_cell = cell_list(cel.relation_graph.prev(j));
                    spacing(j) = prev_cell.length;
                end
                
                %split cell
                spacing = [0;round(cumsum(flipud(spacing/sum(spacing)))*len)];
                clear new_cells
                for j = 1:pc
                    new_cells(j) = cel;
                    new_cells(j).inferred_segmentation = true;
                    new_cells(j).mask = cel.mask(spacing(j)+1:spacing(j+1),:);
                    new_cells(j).bb(1) = cel.bb(1)-1+spacing(j)+1;
                    new_cells(j).bb(2) = cel.bb(1)-1+spacing(j+1);
                    for k = 1:cel.number_of_fluors
                        locus_name = strcat('locus',string(k));
                        tmp = new_cells(j).(locus_name).position(:,1) >= new_cells(j).bb(1)-1 & new_cells(j).(locus_name).position(:,1) < new_cells(j).bb(2);
                        new_cells(j).(locus_name).intensity = new_cells(j).(locus_name).intensity(tmp);
                        new_cells(j).(locus_name).sigma = new_cells(j).(locus_name).sigma(tmp);
                        new_cells(j).(locus_name).position = new_cells(j).(locus_name).position(tmp,:);
                        new_cells(j).(locus_name).number_of_loci = sum(tmp);
                    end
                    new_cells(j).length = spacing(j+1)-spacing(j);
                end
                cell_list(iterator) = new_cells(1);
                new_indices = zeros(pc,1);
                new_indices(1) = cel.cell_id;
                for j = 2:pc
                    new_indices(j) = numel(cell_list)+1;
                    new_cells(j).cell_id = new_indices(j);
                    %new_cells(j).color = get_color(new_indices(j))*(2^16-5);
                    cell_list(new_indices(j)) = new_cells(j);
                end
                new_indices = flipud(new_indices);
                
                
                % update parental relation ship
                cc = numel(cel.relation_graph.next); % children count
                next_indices = cel.relation_graph.next;
                for j = 1:pc
                    cell_list(prev_indices(j)).relation_graph.next = new_indices(j);
                    cell_list(new_indices(j)).relation_graph.prev = prev_indices(j);
                end
                
                if cc == pc %if there are as many parents as children we got a match
                
                    spacing_next = zeros(pc,1);
                    for j = 1:pc
                        next_cell = cell_list(cel.relation_graph.next(j));
                        spacing_next(j) = next_cell.length;
                    end
                    spacing_next = [0;round(cumsum(flipud(spacing_next/sum(spacing_next)))*len)];
                    %[sum(abs(spacing-spacing_next)), spacing(end)*0.10]
                    if sum(abs(spacing-spacing_next)) < spacing(end)*0.10 %check if newly divided cells are with a 10% error range to detected unbalanced splits
                        for j = 1:cc
                            cell_list(next_indices(j)).relation_graph.prev = new_indices(j);
                            cell_list(new_indices(j)).relation_graph.next = next_indices(j);
                        end
                    else
                        for j = 1:cc
                            cell_list(next_indices(j)).relation_graph.prev = new_indices;
                            cell_list(new_indices(j)).relation_graph.next = next_indices;
                        end
                    end
                else
                    if cc>1 || ~isnan(next_indices(1))
                        for j = 1:pc
                            cell_list(new_indices(j)).relation_graph.next = next_indices;
                        end
                        for j = 1:cc
                            cell_list(next_indices(j)).relation_graph.prev = new_indices;
                        end
                    end
                end
            end
        end
    end
    %}
    cycle_count = 1;
    
    %create cell cycles
    for i = 1:numel(cell_list)
        cel = cell_list(i);
        if isnan(cel.cycle_id)
            current_cell = cel;
            while numel(current_cell.relation_graph.prev) == 1 && ...
                    ~isnan(current_cell.relation_graph.prev) && ...
                    numel(cell_list(current_cell.relation_graph.prev).relation_graph.next) == 1
                current_cell = cell_list(current_cell.relation_graph.prev);
            end
            cell_id_count = 2;
            cell_ids = current_cell.cell_id;
            while numel(current_cell.relation_graph.next) == 1 && ...
                    ~isnan(current_cell.relation_graph.next) && ...
                    numel(cell_list(current_cell.relation_graph.next).relation_graph.prev) == 1
                current_cell = cell_list(current_cell.relation_graph.next);
                cell_ids(cell_id_count) = current_cell.cell_id;
                cell_id_count = cell_id_count + 1;
            end
            [cycle,cell_list] = create_cycle(cell_list,cycle_count,cell_ids);
            cycle_list(cycle_count)  = cycle;
            cycle_count = cycle_count + 1;
        end
        %break
    end

    if ~exist('cycle_list','var'), cycle_list = []; end
    % flag cell cyles
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);

        parent_ids = cycle.relation_graph.parent;
        has_parent = false;
        
        for j = 1:numel(parent_ids)
            if ~isnan(parent_ids(j))
                cycle.relation_graph.parent(j) = cell_list(parent_ids(j)).cycle_id;
            end
        end
        
        if numel(parent_ids) == 1 && ...
                ~isnan(parent_ids) && ...
                numel(cell_list(parent_ids).relation_graph.next) == 2
            has_parent = true;
        end
        
        has_children = false;
        children_ids = cycle.relation_graph.children;
        
        
        for j = 1:numel(children_ids)
            if ~isnan(children_ids(j))
                cycle.relation_graph.children(j) = cell_list(children_ids(j)).cycle_id;
            end
        end
        
        if numel(children_ids) == 2 && ...
                numel(cell_list(children_ids(1)).relation_graph.prev) == 1 && ...
                numel(cell_list(children_ids(2)).relation_graph.prev) == 1
            has_children = true;
        end
        
        cycle.flags.bad = min(cycle.areas) < min_area_threshold || ...
            max(cycle.areas) > max_area_threshold || cycle.duration < min_duration_threshold || ...
            cycle.duration > max_duration_threshold;
        left_border = cycle.times(1) == 1;
        right_border = cycle.times(end) == n_frames;
        top_border = min(cycle.bbs(:,1,1)) <= 5 || (numel(children_ids) == 1 && isnan(children_ids)); %no children therefore (inferred) it must be at the top
        cycle.flags.border_cell = any([left_border,right_border,top_border]);
        cycle.flags.bad = ~(has_parent && has_children) || cycle.flags.bad;

        cycle_list(i) = cycle;
    end

    %flag complete cell cycles
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        if ~cycle.flags.bad && ~cycle.flags.border_cell
            parent = cycle_list(cycle.relation_graph.parent);
            child1 = cycle_list(cycle.relation_graph.children(1));
            child2 = cycle_list(cycle.relation_graph.children(2));
            if (~parent.flags.bad || parent.flags.border_cell) && ...
                    (~child1.flags.bad || child1.flags.border_cell) && ...
                    (~child2.flags.bad || child2.flags.border_cell)
                cycle_list(i).flags.complete = true;

            end
        end
    end

    % getting the max number of cells on one frame
    frame_cell_count = zeros(n_frames,1);
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        frame_cell_count(cycle.times) = frame_cell_count(cycle.times) + 1;
    end

    % creating a map of which cells are on which frame plus their cycle index
    max_cycle_count = max(frame_cell_count);
    cycle_ids = nan(n_frames,max_cycle_count);
    cycle_indices = nan(n_frames,max_cycle_count);
    cycle_count = ones(n_frames,1);
    for i = 1:numel(cycle_list)
        cycle = cycle_list(i);
        for j = 1:numel(cycle.times)
            frame_it = cycle.times(j);
            cycle_ids(frame_it,cycle_count(frame_it)) = cycle.cycle_id;
            cycle_indices(frame_it,cycle_count(frame_it)) = j;
            cycle_count(frame_it) = cycle_count(frame_it) + 1;
        end
    end
    
    channel_struct.cycle_ids = cycle_ids;
    channel_struct.cycle_indices = cycle_indices;
    channel_list(1) = channel_struct;
    
    if ~exist('cycle_list','var'), cycle_list = []; end
end





function [number_of_fluors] = how_many_fluors(channel_path)
    prefix = 'fluor';
    paths = dir(channel_path);
    names = {paths.name};
    names = names(startsWith(names,prefix));
    number_of_fluors = 0;
    for name = names
        name = name{:};
        f = str2num(name(length(prefix)+1));
        if number_of_fluors < f
            number_of_fluors = f;
        end
    end
end




function [cycle,cell_list] = create_cycle(cell_list,cycle_id,cycle_cell_ids)
    duration = numel(cycle_cell_ids);
    cycle = struct();
    cycle.cycle_id = cycle_id;
    cycle.channel_id = 1;
    cycle.duration = duration;
    cycle.color = get_color(cycle_id);
    
    
    cycle.flags.complete = false;
    cycle.flags.bad = false;
    cycle.flags.inferred_segmentation = false;
    
    cycle.relation_graph.parent = cell_list(cycle_cell_ids(1)).relation_graph.prev;
    cycle.relation_graph.children = cell_list(cycle_cell_ids(end)).relation_graph.next; %only cell id because parent does not have an cycle id yet!
    
    %cycle.relation_graph.parent = parent.cycle_id;
    %cycle.relation_graph.child(1) = child1;
    %cycle.relation_graph.child(2) = child2;
    
    %cycle.cell_ids = cycle_cell_ids;
    cycle.pole = cell_list(cycle_cell_ids(1)).pole;
    
    %cycle.frame_ids = zeros(duration,1);
    cycle.times = zeros(duration,1);
    cycle.areas = zeros(duration,1);
    cycle.lengths = zeros(duration,1);
    cycle.widths = zeros(duration,1);
    cycle.bbs = zeros(duration,2,2);
    cycle.masks = cell(duration,1);
    cycle.number_of_fluors = cell_list(cycle_cell_ids(1)).number_of_fluors;
    
    for i = 1:duration
        cell_list(cycle_cell_ids(i)).cycle_id = cycle_id;
        cell_list(cycle_cell_ids(i)).color = cycle.color;
        %cycle.frame_ids(i) = cell_list(cycle_cell_ids(i)).frame_id;
        cycle.times(i) = cell_list(cycle_cell_ids(i)).time;
        cycle.areas(i) = cell_list(cycle_cell_ids(i)).area;
        cycle.lengths(i) = cell_list(cycle_cell_ids(i)).length;
        cycle.widths(i) = cell_list(cycle_cell_ids(i)).width;
        cycle.bbs(i,:,:) = cell_list(cycle_cell_ids(i)).bb;
        cycle.masks{i} = cell_list(cycle_cell_ids(i)).mask;
        
        cycle.flags.inferred_segmentation = cycle.flags.inferred_segmentation || cell_list(cycle_cell_ids(i)).inferred_segmentation;
    end
    
    %locus
    for locus_index = 1:cycle.number_of_fluors
        locus_name = strcat('locus',string(locus_index));
        max_foci = 0;
        for i = 1:duration
            cel = cell_list(cycle_cell_ids(i));
            if cel.(locus_name).number_of_loci > max_foci
                max_foci = cel.(locus_name).number_of_loci;
            end
        end
        
        foci_list = nan(duration,max_foci,2);
        foci_characteristics_list = nan(duration,max_foci,2);
        for i = 1:duration
            cel = cell_list(cycle_cell_ids(i));
            for j = 1:cel.(locus_name).number_of_loci
                %cel.(locus_name).position(j,:)
                foci_list(i,j,:) = cel.(locus_name).position(j,:);
                foci_characteristics_list(i,j,1) = cel.(locus_name).intensity(j);
                foci_characteristics_list(i,j,2) = cel.(locus_name).sigma(j);
            end
        end
        %cycle.foci_positions_test = foci_list;
        foci_field_name = strcat('foci',string(locus_index));
        [sorted_foci_list,sorted_foci_characteristics_list] = sort_foci(foci_list,foci_characteristics_list);
        cycle.(foci_field_name).position = sorted_foci_list;
        cycle.(foci_field_name).intensity = sorted_foci_characteristics_list(:,:,1);
        cycle.(foci_field_name).sigma = sorted_foci_characteristics_list(:,:,2);
        
    end
    
    %cycle.flags.bad = mean(cycle.areas) > 750 || cycle.duration < 25;
    
end


function [out] = logical_permuations(n_true,n_false)
    if n_true == 0
        out = false(1,n_false);
    else
        A = false(n_true+n_false,1);
        A(1:n_true) = 1;
        n = numel(A);
        k = sum(A);
        c = nchoosek(1:n,k);
        m = size(c,1);
        out = false(m,n);
        out(sub2ind([m,n],(1:m)'*ones(1,k),c)) = 1;
    end
end


function [sorted,sorted_characteristics] = sort_foci(sort_this,characteristics)
    %{
    sorted = nan(size(sort_this));
    sorted_characteristics = nan(size(characteristics));
    sorted(1,:,:) = sort_this(1,:,:);
    sorted_characteristics(1,:,:) = characteristics(1,:,:);

    for t = 1:size(sort_this,1)-1
        this = sorted(t,:,1);
        next = sort_this(t+1,:,1);
        permutations = perms(1:size(sort_this,2));
        permutation_count = size(permutations,1);
        scores = nan(permutation_count,3);
        scores(:,3) = 1:permutation_count;
        for i = 1:permutation_count
            permutation = permutations(i,:);
            perm_next = next(permutation);
            scores(i,1) = sum((this-perm_next).^2,'all','omitnan');
            scores(i,2) = sum(or(isnan(this),isnan(perm_next)));
        end
        scores = sortrows(scores,[2 1]);
        sorted(t+1,:,:) = sort_this(t+1,permutations(scores(1,3),:),:);
        sorted_characteristics(t+1,:,:) = characteristics(t+1,permutations(scores(1,3),:),:);
    end
    %}


    sorted = nan(size(sort_this));
    sorted_characteristics = nan(size(characteristics));
    sorted(1,:,:) = sort_this(1,:,:);
    sorted_characteristics(1,:,:) = characteristics(1,:,:);

    n = size(sort_this,2);
    for t = 1:size(sort_this,1)-1

        this = sorted(t,:,1);
        next = sort_this(t+1,:,1);
        
        this = cat(2,this',(1:n)');
        next = cat(2,next',(1:n)');

        this = this(~isnan(this(:,1)),:);
        next = next(~isnan(next(:,1)),:);

        this = sortrows(this,1);
        next = sortrows(next,1);

        flip = false;
        if numel(this)<numel(next)
            tmp = this;
            this = next;
            next = tmp;
            flip = true;
        end

        n_true = size(next,1);
        n_false = size(this,1)-n_true;

        permutations = logical_permuations(n_true,n_false);
        distance_squared = zeros(size(permutations,1),1);
        for perm_it = 1:size(permutations,1)
            perm = permutations(perm_it,:);
            this_small = this(perm,:);
            distance_squared(perm_it) = sum((this_small(:,1)-next(:,1)).^2);
        end
        [~,index] = min(distance_squared);
        this = this(permutations(index,:),:);

        if flip
            tmp = this;
            this = next;
            next = tmp;
        end
        
        mapper = zeros(n,1);
        %this
        %next
        mapper(this(:,2)) = next(:,2);
        indices = 1:n;
        indices_unused = ~ismember(indices,mapper);
        mapper(mapper==0)=indices(indices_unused);

        sorted(t+1,:,:) = sort_this(t+1,mapper,:);
        sorted_characteristics(t+1,:,:) = characteristics(t+1,mapper,:);
    end






    sorted = sort_this;
    sorted_characteristics = characteristics;
end

function [color] = get_color(index)
    rng(index)
    while true
        r = randi([0,2^16-1]);
        g = randi([0,2^16-1]);
        b = randi([0,2^16-1]);
        if (r*g*b)^(1.0/3) > 25700
            break
        end
    end
    color = uint16([r,g,b]);
end

