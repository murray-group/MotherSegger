%This function was written by Sean Murray (last modified on 13/4/2022).
%It is based heavily on the spotFinderZ function of Microbetracker (header
%reprinted below) and is released under the same license (GNU GPL v.3).


% MicrobeTracker 0.937
% by Oleksii Sliusarenko, 06/07/2012
% 
% License (GNU GPL v.3)
% 
% The program suite MicrobeTracker outlines bacterial cells in microscope
% images, analyzes fluorescence data and performs statistical analysis of
% the cells. The suite has been developed by Oleksii Sliusarenko with
% contribution of Therry Emonet, Michael Sneddon, and Whitman Schofield,
% members of the Emonet lab and the Jacobs-Wagner lab, Yale University, New
% Haven, CT.
% 
% Copyright (c) 2007-2012, the Emonet lab and the Jacbos-Wagner lab, Yale
% University.
%
% This program is free software: you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by the
% Free Software Foundation, either version 3 of the License, or (at your
% option) any later version.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with this program. If not, see <http://www.gnu.org/licenses/>.
% 
% Additional information about the program suite can be obtained from its
% website <http://microbetracker.org/>.



function cycle_list=spot_detection(params,images,channel_list,cycle_list,fluor_channel,ignore_bad,ignore_border)

%load('parms.mat','params');
%load('matp-ypet-oriC-parS_07_08_25_4/channel_0001/mothersegger_data.mat','channel_list','cycle_list')
%images=loadTiff('matp-ypet-oriC-parS_07_08_25_4/channel_0001/fluor1_subtracted.tif');

H=size(images,1);
W=size(images,2);
fociname=['foci' num2str(fluor_channel)];

if isfield(cycle_list,fociname), cycle_list=rmfield(cycle_list,fociname); end
tmp.position=[];
tmp.b=[];
tmp.h=[];
tmp.sigma=[];
tmp.intensity=[];
tmp.rsqe=[];%relative square error
tmp.outside_mask=[];
cycle_list(1).(fociname)=tmp;

%%

s2 = strel('disk',params.dilateCount);
%positions = {};
Ncells1 = 0;
Ncells2 = 0;
Nspotstotal1 = 0; % Raw spots count
Ispotstotal1 = 0; % Raw spots total intensity
Nspotstotal2 = 0; % Good spots count
%Ispotstotal2 = 0; % Good spots total intensity
msize = 1.5; % Prefilter range, must be >0
x = repmat(1:ceil(msize)*2+1,ceil(msize)*2+1,1);
y = repmat((1:ceil(msize)*2+1)',1,ceil(msize)*2+1);
prefilter = exp(-((ceil(msize)+1-x).^2 + (ceil(msize)+1-y).^2) / msize^2);
prefilter = prefilter/sum(sum(prefilter));

n_cells=sum(~isnan(channel_list.cycle_ids),2);

for frame=1:size(images,3)
    cimage=[];
    cimage0=[];
    cimageF=[];
    convertimages
    Ispotstotal1 = Ispotstotal1 + sum(sum(cimageF));
    Nspotstotal1 = Nspotstotal1 + sum(sum(cimageF>0));
    cycles=channel_list.cycle_ids(frame,~isnan(channel_list.cycle_ids(frame,:)));
    indices=channel_list.cycle_indices(frame,~isnan(channel_list.cycle_indices(frame,:)));
    if params.getnearest
        %a mask on the whole image got by eroding the inverse
        %mask from all cells
        dmap = getdmap(cycle_list(cycles),indices,[],size(cimage),ceil(params.dilateCount*1.5));
    end
    for cell = 1:n_cells(frame)
        cycle=channel_list.cycle_ids(frame,cell);
        index=channel_list.cycle_indices(frame,cell);
        if(isempty(cycle_list(cycle).(fociname)))
            cycle_list(cycle).(fociname)=tmp;
            %                              cycle_list(cycle).(fociname).position=NaN(cycle_list(cycle).duration,1,2);
            %                             cycle_list(cycle).(fociname).b=NaN(cycle_list(cycle).duration,1);
            %                             cycle_list(cycle).(fociname).h=NaN(cycle_list(cycle).duration,1);
            %                             cycle_list(cycle).(fociname).sigma=NaN(cycle_list(cycle).duration,1);
            %                             cycle_list(cycle).(fociname).intensity=NaN(cycle_list(cycle).duration,1);
            %                             cycle_list(cycle).(fociname).outside_mask=NaN(cycle_list(cycle).duration,1);
        end
        if((ignore_bad==1 && cycle_list(cycle).flags.bad==1) || (ignore_border==1 && cycle_list(cycle).flags.border_cell==1 ))
            continue;
        end
        
        Ncells1 = Ncells1 + 1;
        norm = 1;
        
        
        
        mask=cycle_list(cycle).masks{index};
        box=squeeze(cycle_list(cycle).bbs(index,:,:));
        box=[box(3),box(1),box(4)-box(3),box(2)-box(1)];%[top_x, top_y, width height]
        
        orig_box=box;
        [mask,box]=expand_mask(mask,box,H,W,10,10);
        mask0=mask;%
        imgF = imcrop(cimageF,box)*norm;
        imgR = imcrop(cimage,box)*norm;
        
        if params.getnearest
            %a mask of everywhere that is not another cell
            dmapmsk = imcrop(dmap,box)>=...
                getdmap(cycle_list(cycle),index,mask,size(imgF),ceil(params.dilateCount*1.5));
        else
            dmapmsk = [];
        end
        
        % Parameters
        scalefactor = params.scalefactor;
        dmax = floor(params.dmax*scalefactor);
        
        % Do fitting, keep only good spots
        [spotlist,mask] = getspots(mask,imgF,imgR,s2,dmax,dmapmsk);
        lst = getcorrectspots(spotlist,params);
        spotlist2 = spotlist(lst,:);
        spotlist3 = getspots2(spotlist2,imgR,mask,params.wmax*params.scalefactor,dmax,params.shiftlim);
        
        
        % Saving data
        
        if isempty(cycle_list(cycle).(fociname).b)
            dur=cycle_list(cycle).duration;
            cycle_list(cycle).(fociname).position=NaN(dur,1,2);
            cycle_list(cycle).(fociname).b=NaN(dur,1);
            cycle_list(cycle).(fociname).h=NaN(dur,1);
            cycle_list(cycle).(fociname).sigma=NaN(dur,1);
            cycle_list(cycle).(fociname).intensity=NaN(dur,1);
            cycle_list(cycle).(fociname).rsqe=NaN(dur,1);
            cycle_list(cycle).(fociname).outside_mask=NaN(dur,1);
        end
        
        n_spots = size(spotlist3,1);
        if n_spots>0
            [~,I]=sort(spotlist3(:,8));
            spotlist3=spotlist3(I,:);
            %spint = pi*spotlist3(:,2).*spotlist3(:,3); % spot intensities (pi*height*width^2)
            X = (spotlist3(:,8))';
            Y = (spotlist3(:,9))';
            spb = spotlist3(:,1)';
            spw = sqrt(max(0,spotlist3(:,2)))';
            sph = spotlist3(:,3)';
            
            spotlist2=spotlist2(I,:);
            error=spotlist2(:,4)';
            
            outside_mask=NaN(1,n_spots);
            for i=1:n_spots
                if(round(X(i))>0 && round(X(i))<=size(mask0,1) && round(Y(i))>0 && round(Y(i))<=size(mask0,2))
                    outside_mask(i)=~mask0(round(X(i)),round(Y(i)));
                end
            end
            
            range=1:n_spots;
            cycle_list(cycle).(fociname).position(index,range,1)=X'-(orig_box(2)-box(2));
            cycle_list(cycle).(fociname).position(index,range,2)=Y'-(orig_box(1)-box(1));
            cycle_list(cycle).(fociname).b(index,range)=spb;
            cycle_list(cycle).(fociname).h(index,range)=sph;
            cycle_list(cycle).(fociname).sigma(index,range)=spw;
            cycle_list(cycle).(fociname).intensity(index,range)=pi*spw.^2.*sph;
            cycle_list(cycle).(fociname).rsqe(index,range)=error;
            cycle_list(cycle).(fociname).outside_mask(index,range)=outside_mask;
            
            
            Nspotstotal2 = Nspotstotal2 + n_spots;
            
            
            Ncells2 = Ncells2 + 1;
            
        end
        
        if(index==cycle_list(cycle).duration)%if the last frame of the cell cycle, then 0->NaN
            cycle_list(cycle).(fociname).position(cycle_list(cycle).(fociname).position(:)==0)=NaN;
            cycle_list(cycle).(fociname).b(cycle_list(cycle).(fociname).b(:)==0)=NaN;
            cycle_list(cycle).(fociname).h(cycle_list(cycle).(fociname).h(:)==0)=NaN;
            cycle_list(cycle).(fociname).sigma(cycle_list(cycle).(fociname).sigma(:)==0)=NaN;
            cycle_list(cycle).(fociname).intensity(cycle_list(cycle).(fociname).intensity(:)==0)=NaN;
            cycle_list(cycle).(fociname).outside_mask(isnan(cycle_list(cycle).(fociname).intensity(:)))=NaN;
        end
        %end
    end % for cell=1:length(cellList{frame})
    %[row,col,v] = find(cimageF);
    %positions{frame} = [row,col,v];
    %disp(['Detecting spots, frame ' num2str(frame)]);
end % for frame=framerange
disp(['Identified ' num2str(Nspotstotal1) ' non-normalized non-thresholded spots in ' num2str(Ncells1) ' cells']);
disp(['Identified ' num2str(Nspotstotal2) ' positively identified spots in ' num2str(Ncells2) ' cells']);
%disp(['Mean magnitude of ''good'' spots: ' num2str(Ispotstotal2/Nspotstotal2)]);

    function convertimages
        cimage0 = im2double(images(:,:,frame));
        cimageF = filterimage(cimage0);
        if params.minprefilterh>0
            cimageF = cimageF.*(cimageF>params.minprefilterh);
        end
        cimage  = imfilter(cimage0,prefilter,'replicate');
    end

    function res = filterimage(img)
        
        img2a=imgaussfilt(img,params.sigma);
        
        %img2a(img2a<5)=0;
        
        img2b = img2a;
        
        res =img2b.*(imdilate(img2b,strel('arbitrary',[1 1 1; 1 1 1; 1 1 1]))==img2b);%.*(imerode(img2b,strel('disk',1))<img2b);
        
    end

end

function [spotlist,mask] = getspots(mask,imgF,imgR,s2,dmax,dmapmsk)
mask = imdilate(mask,s2);
if ~isempty(dmapmsk)
    mask = mask.*dmapmsk;
end
[row,col] = find(imgF.*mask);
Y = col;
X = row;
Ly = length(row);
spotlist = zeros(Ly,7);

msk = zeros(dmax*2-1);
msk(dmax,dmax)=1;
msk = imdilate(msk,strel('disk',dmax));
D2 = repmat(reshape((-dmax+1):(dmax-1),1,[]),2*dmax-1,1).^2 +...
    repmat(reshape((-dmax+1):(dmax-1),[],1),1,2*dmax-1).^2;

% 1st fit - fixed positions, individual spots
for sp = 1:Ly
    % Prepare matrices for minimization
    box1(1:2) = max([Y(sp) X(sp)]-dmax+1,1);
    box1(3:4) = min([Y(sp) X(sp)]+dmax-1,[size(imgR,2) size(imgR,1)])-box1(1:2);
    box2(1:2) = max(0,dmax-[Y(sp) X(sp)])+1;
    box2(3:4) = box1(3:4);
    img2 = imcrop(imgR,box1);
    msk2 = imcrop(msk,box2);%.*imcrop(mask,box1);
    mskp = bwperim(imcrop(msk,box2));
    dst2 = imcrop(D2,box2);
    % Assign data
    dat = [mean(mean(img2)) dmax max(0,img2(dmax-max(0,dmax-X(sp)),dmax-max(0,dmax-Y(sp)))-mean(mean(img2)))];
    % Do minimization
    options = optimset('Display','off','MaxIter',300);
    [dat,fval,exitflag] = fminsearch(@gfit,dat,options);
    % Save for later
    spotlist(sp,1) = dat(1); % background
    spotlist(sp,2) = dat(2); % squared width of the spots
    spotlist(sp,3) = dat(3); % hight
    spotlist(sp,4) = fval/(dat(3)^2); % rel. sq. error
    spotlist(sp,5) = var(imgR(mskp))/(dat(3)^2); % perimeter variance
    spotlist(sp,6) = imgF(X(sp),Y(sp))/dat(3); % filtered/fitted ratio
    spotlist(sp,7) = exitflag; % exit -1 / 0 / 1
    spotlist(sp,8) = X(sp);
    spotlist(sp,9) = Y(sp);
    
end
    function res = gfit(in)
        % also uses:
        % ls (list of spots to fit)
        % wf - squared width of the spots, only those for ~ls are used
        % hf - hight of the spots
        % in = [b wv(1:n) hv(1:n)]
        b = in(1);
        wv = in(2);
        hv = in(3);
        M = b + exp(-dst2/wv)*hv;
        R = (msk2.*(M - img2)).^2;
        res = sum(sum(R));
        if b<0, res=res+(b/hv)^2; end
    end
end
function spotlist2 = getspots2(spotlist,imgR,mask,wmax,dmax,shiftlim)
if isempty(spotlist); spotlist2 = []; return; end
X = spotlist(:,8);
Y = spotlist(:,9);
m = size(imgR,1);
n = size(imgR,2);
Ly = size(spotlist,1);
Xm = repmat(reshape(X,[1 1 Ly]),m,n);
Ym = repmat(reshape(Y,[1 1 Ly]),m,n);
Ix2 = repmat((1:m)',[1 n]);
Iy2 = repmat(1:n,[m 1]);
wf = spotlist(:,2);
hf = spotlist(:,3);
bf = spotlist(:,1);
% bf = mean(b);
Ix3 = repmat((1:m)',[1 n Ly-1]);
Iy3 = repmat(1:n,[m 1 Ly-1]);
spotlist2 = zeros(size(spotlist));
msk = zeros(dmax*2-1);
msk(dmax,dmax)=1;
msk = imdilate(msk,strel('disk',dmax));
D2 = repmat(reshape((-dmax+1):(dmax-1),1,[]),2*dmax-1,1).^2 +...
    repmat(reshape((-dmax+1):(dmax-1),[],1),1,2*dmax-1).^2;
% 2nd fit - flexible positions
%for q = 1:2, what it this for?
    for sp = 1:Ly
        % Assign variable data
        box1(1:2) = max([Y(sp) X(sp)]-dmax+1,1);
        box1(3:4) = min([Y(sp) X(sp)]+dmax-1,[size(imgR,2) size(imgR,1)])-box1(1:2);
        box2(1:2) = max(0,dmax-[Y(sp) X(sp)])+1;
        box2(3:4) = box1(3:4);
        img2 = imcrop(imgR,box1);
        msk2 = imcrop(msk,box2);%.*imcrop(mask,box1);
        dst2 = imcrop(D2,box2);
        dat = [bf(sp) wf(sp) hf(sp) X(sp) Y(sp)];
        % Assign fixed data
        ls = true(1,Ly);
        ls(sp) = false;
        W = repmat(reshape(wf(ls),[1 1 Ly-1]),m,n);
        H = repmat(reshape(hf(ls),[1 1 Ly-1]),m,n);
        Xm3 = repmat(reshape(X(ls),[1 1 Ly-1]),m,n);
        Ym3 = repmat(reshape(Y(ls),[1 1 Ly-1]),m,n);
        D3 = (Xm3-Ix3).^2 + (Ym3-Iy3).^2;
        M0 = sum(exp(-D3./W).*H,3);
        % Do minimization
        options = optimset('Display','off','MaxIter',300);
        [dat,fval2,exitflag] = fminsearch(@gfitpos,dat,options);
        bf(sp) = dat(1);
        wf(sp) = dat(2);
        hf(sp) = dat(3);
        X(sp) = dat(4);
        Y(sp) = dat(5);
        
        spotlist2(sp,1) = dat(1); % background
        spotlist2(sp,2) = dat(2); % spot width
        spotlist2(sp,3) = dat(3); % hight
        spotlist2(sp,4) = gfit(dat)/(dat(3)^2); % rel. sq. error
        % fields 5 & 6 are not filled in second pass
        spotlist2(sp,7) = exitflag; % exit -1 / 0 / 1
        spotlist2(sp,8) = dat(4);
        spotlist2(sp,9) = dat(5);
    end %
%end
    function res = gfit(in)
        b = in(1);
        wv = in(2);
        hv = in(3);
        M = b + exp(-dst2/wv)*hv;
        R = (msk2.*(M - img2)).^2;
        res = sum(sum(R));
    end
    function res = gfitpos(in)
        % also uses:
        % ls (indicates the spot to fit)
        % wf - width of the spot
        % hf - hight of the spot
        % in = [b wv(1:n) hv(1:n)]
        b = in(1);
        wv = in(2);
        hv = in(3);
        xv = in(4);
        yv = in(5);
        x0 = spotlist(sp,8);
        y0 = spotlist(sp,9);
        Xm = repmat(xv,m,n);
        Ym = repmat(yv,m,n);
        D2a = (Xm-Ix2).^2 + (Ym-Iy2).^2;
        M = M0 + b + exp(-D2a/wv)*hv;
        R = (mask.*(M - imgR)).^2;
        if isempty(shiftlim)||shiftlim<0, shiftlim = 0.01; end
        res = sum(sum(R))*(1+shiftlim*((x0-xv)^2+(y0-yv)^2))*(1+max(0,wv/wmax-1)^2)*(1+min(0,hv/mean(hf))^2);
        if b<0, res=res+(b/hv)^2; end
    end
end
function lst = getcorrectspots(spotlist,params)
scalefactor=params.scalefactor;
wmax = params.wmax*scalefactor; % max width in pixels
wmin = params.wmin*scalefactor;
hmin = params.hmin; % min height
ef2max = params.ef2max; % max relative squared error
vmax = params.vmax; % max ratio of the variance to squared spot height
fmin = params.fmin; % min ratio of the filtered to fitted spot (takes into account size and shape)

lst = ... % spotlist(:,1)>0 & ...
    spotlist(:,2)<wmax & spotlist(:,2)>wmin & spotlist(:,3)>hmin ...
    & spotlist(:,4)<ef2max ...
    & (spotlist(:,5)<vmax | spotlist(:,5)==0) ... % OK if zero
    & spotlist(:,6)>fmin ...
    & spotlist(:,7)==1;
end

function res = getdmap(clist,indices,mask,sz,n)
if isempty(mask)
    mask = zeros(sz);
    for i=1:length(clist)
        %mesh = clist{cell}.mesh;
        %plgx = ([mesh(1:end-1,1);flipud(mesh(:,3))])*rs;
        %plgy = ([mesh(1:end-1,2);flipud(mesh(:,4))])*rs;
        %mask = mask+poly2mask(plgx,plgy,sz(1),sz(2));
        box=squeeze(clist(i).bbs(indices(i),:,:));
        %box=[box(3),box(1),box(4)-box(3),box(2)-box(1)];%[top_x, top_y, width height]
        mask(box(1):box(2),box(3):box(4))=clist(i).masks{indices(i)};
    end
else
    %mesh = clist.mesh;
    %plgx = ([mesh(1:end-1,1);flipud(mesh(:,3))]-box(1)+1)*rs;
    %plgy = ([mesh(1:end-1,2);flipud(mesh(:,4))]-box(2)+1)*rs;
    %mask = poly2mask(plgx,plgy,sz(1),sz(2));
    %box=squeeze(clist(1).bbs(indices(1),:,:));
    %box=[box(3),box(1),box(4)-box(3),box(2)-box(1)];%[top_x, top_y, width height]
    %mask(box(1):box(2),box(3):box(4))=clist(1).masks{indices(1)};
end
mask = ~mask;
res = mask;
se = strel('disk',1);
for i=1:n
    mask = imerode(mask,se);
    res = res+mask;
end
end


function [mask2,box2]=expand_mask(mask,box,H,W,xpadding,ypadding)
%box is topx,topy,w,h
topx=box(1);
topy=box(2);
w=box(3);
h=box(4);

if topx>xpadding
    xpaddingL=xpadding;
else
    xpaddingL=topx-1;
end
if topy>ypadding
    ypaddingT=ypadding;
else
    ypaddingT=topy-1;
end
if topx+w+xpadding<=W
    xpaddingR=xpadding;
else
    xpaddingR=W-topx-w;
end
if topy+h+ypadding<=H
    ypaddingB=ypadding;
else
    ypaddingB=H-topy-h;
end

mask2=zeros(h+1+ypaddingT+ypaddingB,w+1+xpaddingL+xpaddingR);
mask2(ypaddingT+1:end-ypaddingB,xpaddingL+1:end-xpaddingR)=mask;
box2=[topx-xpaddingL,topy-ypaddingT,size(mask2,2)-1,size(mask2,1)-1];
end

