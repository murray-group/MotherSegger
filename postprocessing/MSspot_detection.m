function MSspot_detection(parametersPartIV)
    channel_paths = get_channel_paths(parametersPartIV.base,false);
    [channel_paths] = filter_paths(channel_paths);
    disp(strcat("tracking: number of channels ",string(numel(channel_paths))))
    tic
    
    ignore_bad_cells=parametersPartIV.ignore_bad_cells;
    ignore_border_cells=parametersPartIV.ignore_border_cells;
    
    fluor_channels=parametersPartIV.channels;
    n_fluor=length(fluor_channels);
    p=cell(n_fluor,1);
    for i=1:n_fluor   
    if fluor_channels(i)==1
        p{i}=parametersPartIV.params{i};
        %p{i}=load([parametersPartIV.base parametersPartIV.params{i}],'params');
    end
    end
   
    %no need to distinguish between local and remote pool?. Same RAM usage from image loading. If there is, the
    %following could be used
    %pool=gcp;%get current or start default pool
   % if(strcmp(pool.Cluster.Profile, 'local'))
       
       
%         parfor i = 1:numel(channel_paths) 
%                 z(i)=load([channel_paths{i} 'mothersegger_data.mat'],'channel_list','cycle_list');
%                 for j=1:length(fluor_channels)
%                     if(fluor_channels(j)==1)
%                         images=loadTiff([channel_paths{i} 'fluor' num2str(j) '_subtracted.tif']);
%                         z(i).cycle_list = spot_detection(p{j}.params,images,z(i).channel_list,z(i).cycle_list,j,parametersPartIV.ignore_bad_cells,parametersPartIV.ignore_border_cells);
%                     end
%                 end 
%         end
%         for i = 1:numel(channel_paths) 
%         save_mothersegger_data(channel_paths(i),z(i).channel_list,z(i).cycle_list);
%         end
        

         n_channels = parametersPartIV.n_channels;
        for batch_it = 1:n_channels:numel(channel_paths)
            current_end = min(batch_it+n_channels-1,numel(channel_paths));
            disp(strcat("Batch: ",string(batch_it),"-",string(current_end)," channel"))
            current_range = (batch_it:current_end);
            images=cell(length(current_range),length(fluor_channels));
            z=cell(length(current_range));
              
            for k = 1:length(current_range)  
                z{k}=load([channel_paths{current_range(k)} 'mothersegger_data.mat'],'channel_list','cycle_list');
            
                for j=1:length(fluor_channels)
                    if(fluor_channels(j)==1)
                        images{k,j}=loadTiff([channel_paths{current_range(k)} 'fluor' num2str(j) '_subtracted.tif']);
                    end
                end           
            end
       
            parfor k = 1:length(current_range)
               for j=1:n_fluor
                    if(fluor_channels(j)==1)
                        try 
                            z{k}.cycle_list = spot_detection(p{j},images{k,j},z{k}.channel_list,z{k}.cycle_list,j,ignore_bad_cells,ignore_border_cells);
                        catch ME
                            disp(k)
                        end
                    end
               end
                z{k}.cycle_list = resort_foci(z{k}.cycle_list);% sort foci
            end

            for k = 1:length(current_range)
                    save_mothersegger_data(channel_paths(current_range(k)),z{k}.channel_list,z{k}.cycle_list);
            end
        end
        
        
        
     exp_info_path = strcat(parametersPartIV.base,'experiment_info.mat');
    if isfile(exp_info_path)
        experiment_info = load(exp_info_path);
        experiment_info = experiment_info.experiment_info;
    else
        experiment_info = struct();
    end
    if isfield(experiment_info,'PartIV') && isfield(experiment_info.PartIV,'params')
         p=experiment_info.PartIV.params;
        for i=1:n_fluor   
            if fluor_channels(i)==1
                p{i}=parametersPartIV.params{i};%update the existing params cell array with params used in this call
            end
        end
        parametersPartIV.params=p;
    end
    experiment_info.PartIV = parametersPartIV;
    save(exp_info_path,'experiment_info')
        
        
        
    toc
    disp('Done! (Part IV)')
end


function [paths] = filter_paths(paths)
    filter = true(numel(paths),1);
    for path_it = 1:numel(paths)
        path = paths(path_it);
        filter(path_it) = isfile(strcat(path,'segmentation.tif'));
    end
    paths = paths(filter);
end