%% load Experiment
base = "Path/To/Folder/";
result_paths = get_channel_paths(base,true);
[channel_list,cycle_list,fov_list] = load_mothersegger_data(result_paths,false,false,false);%load cycles from a specified path (either all (false) or only complete (true))
complete_cycle_ids = get_complete_cycle_ids(cycle_list);
disp('done')
%% load combined file
path = "/Path/to/File/combined_data.mat";
[cycle_list,channel_list,fov_list] = load_combined_data(path);
complete_cycle_ids = get_complete_cycle_ids(cycle_list);
%% channel quality (works only if all cycles are loaded, does not work with only complete cycles)
figure
channel_quality(channel_list,cycle_list);
%% filter complete cell cycle ids
complete_cycle_ids = get_complete_cycle_ids(cycle_list);
complete_cycle_ids = complete_cycle_filter(complete_cycle_ids,cycle_list,0.005);
%% overview
figure
overview(complete_cycle_ids,cycle_list)
%% cell length vs time
figure
plot_cell_length_vs_time(cycle_list,complete_cycle_ids,0.0688);
%% growth rates
figure
growth_rates=hist_growth_rates(cycle_list,complete_cycle_ids);
%% overview of a specific channel (line style)
range = 1:1:5000;
figure
canvas = display_channel_linestyle(channel_list(2),cycle_list,-1,range);
%% overview of a specific channel (detailed)
range = 1:1:10;
canvas = display_channel_with_range(channel_list(3),cycle_list,'phase',1,range);
%% show one cycle
figure
cycle = cycle_list(complete_cycle_ids(552));
fluor_name = 'phase_subtracted';
fluor_name = {'fluor1_subtracted','fluor2_subtracted',''};
display_cycle_with_range(cycle,channel_list,0,fluor_name,1,1:2:cycle.duration);
%% plot cycle with trajectory of the specified foci list
figure
plot_cell_with_trajectory(cycle_list(1),'foci3',[119,221,80]/225,1,0.1,-1)
%% kymograph
figure
[canvas] = kymograph(cycle_list,complete_cycle_ids,40,15,2,true);
%% hist single cell lengths
figure
lengths=hist_single_cell_lengths(cycle_list,complete_cycle_ids);
%% show each complete cell cycle
fluor_name = {'fluor3_subtracted','fluor2_subtracted',''};
for i = 1:numel(complete_cycle_ids)
    disp(string(i))
    cycle = cycle_list(complete_cycle_ids(i));
    display_cycle_with_range(cycle,channel_list,0,fluor_name,-1,1:2:cycle.duration);
    pause(1)
    %saveas(gcf,strcat('~/Tmp/triple_',string(i),'.png'))
end




